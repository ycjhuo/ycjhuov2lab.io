---
home: true
layout: BlogHome
#icon: home
title: YC's Weekly Journal

# 顯示主頁圖片 
hero: false
#heroImage: /logo.svg
#heroText: YC's Weekly Journal
tagline: Code, Invest, Travel
#heroFullScreen: false
projects:
  - icon: arrow-trend-up
    name: Stocks
    #desc: project detailed description
    link: https://ycjhuo.gitlab.io/tag/Stock/

  - icon: location-pin
    name: Travel
    #desc: link detailed description
    link: https://ycjhuo.gitlab.io/categories/Travel/
  
  - icon: hotel
    name: Hotels
    #desc: link detailed description
    link: https://ycjhuo.gitlab.io/tag/Hotel/

  - icon: hand-holding-dollar
    name: Passive Income
    #desc: Detailed description of the book
    link: https://ycjhuo.gitlab.io/tag/Passive%20Income/

#footer: customize your footer text
---
