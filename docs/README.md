---
home: true
layout: BlogHome
#icon: home
title: YC's Weekly Journal

# 顯示主頁圖片 
hero: false
#heroImage: /logo.svg
#heroText: YC's Weekly Journal
tagline: Code, Invest, Travel
#heroFullScreen: false
projects:
  - icon: arrow-trend-up
    name: 美股投資
    #desc: project detailed description
    link: https://ycjhuo.gitlab.io/tag/Stock/

  - icon: location-pin
    name: 行程旅遊
    #desc: link detailed description
    link: https://ycjhuo.gitlab.io/categories/Travel/
  
  - icon: hotel
    name: 飯店開箱
    #desc: link detailed description
    link: https://ycjhuo.gitlab.io/tag/Hotel/

  - icon: hand-holding-dollar
    name: 被動收入
    #desc: Detailed description of the book
    link: https://ycjhuo.gitlab.io/tag/Passive%20Income/

#footer: customize your footer text
---
