---
title: Coinbase（COIN）2023 Q1 季報 | 財報大幅好轉，但與 SEC 的官司影響更甚
description: '美股投資 Coinbase COIN 介紹|Coinbase COIN 2023 Q1 第一季財報分析|第一家上市的加密貨幣 Crypto 交易所|加密貨幣 Crypto 大跌|加密貨幣 Crypto 美股'
date: 2023-07-18
tag: [Stock, COIN, Crypto]
category: Investment
---
最近，幣安美國（Binance.US）遭受美國證券委員會（SEC）起訴後，在上個月（6/13）停止了法幣出入金的業務，僅專注在純加密貨幣的交易/轉換，徹底切割了美元存/提款的業務

去年 FTX 暴雷後，加密貨幣交易所僅剩幣安＆Coinbase 二強競爭，而現在幣安美國又因為 SEC 的壓力，縮減了業務範圍，在美國上市的 Coinbase 是否能在同業都倒下後，獨佔整個市場，享受勝利的果實呢？

下面來看 Coiinbase 在 5/4 所發佈的 2023 Q1 財報

## Coinbase 2023 Q1 財務指標
[資料來源](https://s27.q4cdn.com/397450999/files/doc_financials/2023/q1/Shareholder-Letter-Q1-2023.pdf)

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q3</th>
    <th align="center">2022 Q4</th>
    <th align="center">2023 Q1</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2023 Q1 vs. 2022 Q4</th>
  </tr>
  <tr>
    <td align="center">總營收</td>
    <td align="center">5.9 億</td>
    <td align="center">6.3 億</td>
    <td align="center">7.7 億</td>
    <td align="center">6%</td>
    <td align="center">19%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">83%</td>
    <td align="center">87%</td>
    <td align="center">88%</td>
    <td align="center">5%</td>
    <td align="center">1%</td>
  </tr>
  <tr>
    <td align="center">營業利潤</td>
    <td align="center">-5.6 億</td>
    <td align="center">-5.5 億</td>
    <td align="center">-1.2 億</td>
    <td align="center">--</td>
    <td align="center">--</td>
  </tr>  
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">-94%</td>
    <td align="center">-88%</td>
    <td align="center">-16%</td>
    <td align="center">--</td>
    <td align="center">--</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">-2.43</td>
    <td align="center">-2.46</td>
    <td align="center">-0.34</td>
    <td align="center">--</td>
    <td align="center">--</td>
  </tr>
</table>

- 總營收：本季營收已連續二季成長，達到 7.7 億，季成長高達 19%，但跟 2021 Q2 的高點比，現在營收僅為當時的 36%
- 毛利率：毛利率在連續四季的成長後，目前達到 88%，為史上最高
- 營業利潤：本季虧損了 1.2 億，已連續五季虧損，但相較於前四季，已有著大幅度的進步，今年將有機會轉虧為盈
- 營業利潤率：本季營業利潤率為 -16%，相比上季的 -88%，大幅減少了 70%，主要是裁員導致的開銷減少
- EPS：受惠於營業利潤率的大幅成長，本季 EPS 縮小至 -0.34，是近五季的最佳表現

Coinbase 的營收來源可分為：「淨營收」＆「其它」，而「淨營收」又可細分為「交易」 與 「訂閱＆服務」

能為公司帶來穩定來源的「訂閱＆服務」，一直保持著雙位數的季成長，在本季已貢獻了 49% 的淨營收（一年前僅約 13%），是本季營收成長的關鍵之一

在總營收季成長 19% 的同時，營運費用大幅縮減了 32%，其中開銷最大的研發＆行政費用分別減少了 65% ＆ 52%
讓營利率從上季的 -88% 縮減到 -16%

## Coinbase 2023 Q1 財報重點
財報中揭露：帶動本季營收成長的「訂閱＆服務」，在下個季度的成長性會因為 USDC 市值的縮小而減慢，連帶影響 Q2 營收，導致營收可能比本季還低

雖然在 2022 年底，Coinbase 推出了將 USDT 轉換成 USDC 免手續費的方案，但成效不佳，仍無法阻止 USDC 的市值繼續縮小

::: tip
- USDT 一年前的市值約 659 億，現在為 838 億，成長 27% 
- USDC 一年前的市值約 547 億，現在為 270 億，衰退 51% 
:::

下一季的預估營收不佳的話，那營運費用的話會如何呢？

目前的營運費用，處於 2021 Q1 以來的最低階段，支出僅為當時的一半，主要是因裁員＆組織重組導致，目前的員工數約為當時的 70%

但下一季的營運支出將小幅提升，主要是因為與 SEC 的官司，導致法務費用的上升，以及與 NBA 的合作提高了行銷方面的支出

## 後記
Coinbase 目前的表現，相比於二年前的熱潮，營收僅剩當時的 1/3，但在經歷了五個季度的虧損後（包含本季），已有明顯的好轉，再加上 FTX 與幣安美國的接連失利，Coinbase 後續的發展其實是值得期待的

但可惜，SEC 似乎還不打算放過加密貨幣產業，在上個月（6/6）也起訴了 Coinbase 未取得相關執照，就違法經營交易所，為 Coinbase 的未來增添了巨大的風險

雖然 Coinbase 的股價在今年漲了 200%，近一個月漲了 85%，但若輸了與 SEC 的這場官司，則會面臨與幣安美國一樣，被迫縮減業務，亦或是直接停業的風險

::: tip
比特幣今年漲了 81%，近一個月上漲 14%，Coinbase 的股價表現大幅超過比特幣
:::

政府單位的決策，比財報的表現更能決定 Coinbase 未來的天花板，也難怪 Coinbase 在財報中花了 1/3 的篇幅在解釋自己對於美國＆金融機構的重要性，企圖減輕政府對於加密貨幣產業的打擊

而現在敢買入 Coinbase 的人，與其說是看好 Coinbase 下次財報的表現，不如說是相信 Coinbase 會打贏這場與 SEC 的官司

::: warning
點擊觀看更多： <br />
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [加密貨幣系列文](https://ycjhuo.gitlab.io/tag/Crypto/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
