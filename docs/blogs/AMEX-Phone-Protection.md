---
title: 美國運通（American Express）信用卡手機保護（Cell Phone Protection）申請教學
description: '美國運通 American Express AE 大白卡|美國運通 AMEX Platinum 白金卡|信用卡福利|手機保護|手機保險 Cell Phone Protection|如何申請保險教學|AIG 美國運通保險'
date: 2021-07-29
tag: [Credit Cards, AMEX]
category: Investment
---

最近因為不小心摔壞了手機（Google Pixel 3XL），幸好有美國運通信用卡提供的手機保護（Cell Phone Protection）福利，報銷了購買新機（Google Pixel 4a）的消費，但還是有自付額 $50 <br />

![100-01](../images/100-01.png) <br />

我用來繳交電話費的信用卡是美國運通的白金卡（Schwab 版本），電信公司是 Mint Mobile <br />
雖然白金卡每次最高的賠償金額是 $800，但因為不確定報銷的金額是否跟摔壞的手機有關，因此選擇了相同品牌且價格最低的 Pixel 4a 來購買（含稅價 $372.12 扣掉自付額 $50，最後收到的支票金額為 $322.12）<br />

![100-01](../images/100-02.png) <br />

以下是我的申請流程： <br />


## 美國運通手機保護

美國運通針對持有特定信用卡的用戶，提供了手機保護（Cell Phone Protection）的服務 <br />
顧名思義，只要我們用 AMEX 的信用卡，繳交每個月的電話費，就可以享有手機保護（Cell Phone Protection）這項福利 <br />
符合的信用卡有：
- Amex Centurion（黑卡）
- Amex Platinum（白金卡，包含聯名卡）
- Amex Delta Reserve, Amex Delta Platinum

官方介紹請至：[Cell Phone Protection](https://www.americanexpress.com/us/credit-cards/features-benefits/policies/cell-phone-protection.html) <br />

## 符合條件
- 要用有提供手機保護的信用卡繳交每個月的電話費
- 保險生效日：繳交電話費的下一個月（1 號）開始生效，停止繳交電話費的下一個月失效（1 號）
- 賠償修復手機的金額或購買新機的價格
- 最高賠償金額為 $800（單次），一張卡一年限用二次（以白金卡為例）
- 每次賠償須自付 $50（以白金卡為例），也就是若我購買新機的價格是 $300，則賠償的金額為 $250

## 申請流程

### 致電 AIG 來成立 Case
美國運通將手機保護（Cell Phone Protection）這項服務外包給了 AIG 保險的旅遊部門，因此我們須先打給 AIG 來成立 case
AIG 的電話是：833-784-1467，服務時間為：星期一 ~ 星期五 8:00 A.M. - 8:00 P.M. ET（美國東岸時間）

致電後，AIG 會馬上寄封 email 到你的信箱，並告知我們 Claim No. 是多少

### 準備相關文件
在 AIG 的 email 中，會要求我們提供下列文件：
- Claim Form（共有 3 張）：在 AIG 寄給我們的 email 中可找到（印出來，填上 Claim No. 基本資料及簽名後，掃描回傳）
- PROOF OF ELIGIBLE CELLULAR WIRELESS TELEPHONE（購買手機型號的截圖）
- PROOF OF PAYMENT AMEX BILLING STATEMENT（用美國運通信用卡繳交電話月租費的截圖或 Statement）
- PROOF OF PAYMENT PROVIDERS BILLING STATEMENT（手機電話費帳單的截圖或 Statement）
- REPAIR ESTIMATE（維修店所開立的估價單）

### 我準備的文件
- 損壞手機的照片 & 損壞手機當時購買的明細
- 新手機的照片 & 新手機的購買明細（或發票）
- AMEX 信用卡 Statement （裡面有新手機的購買紀錄，證明是用 AMEX 信用卡購買的）
- 電信公司的 Statement（證明我的確是用 AMEX 信用卡來付帳單）
- AMEX 信用卡 Statement （裡面有每個月月租費的扣款紀錄，證明是用 AMEX 信用卡支付電話月租費）

文件準備齊全後，AIG 若沒有其他問題，就會 email 通知： <br />
該 Claim 已被審核通過，將會寄支票到你的住家地址，而支票的金額是 $XXX - 自付額

## 後記
手機保護不同於我在上篇寫到的 [購買保護（Purchase Protection）](https://ycjhuo.gitlab.io/blogs/AMEX-Purchase-Protection.html) <br />

由於```手機保護```是外包給 AIG ，因此在文件審核、方便性、速度上都比不上美國運通親自處理的```購買保護``` <br />

手機保護從我申請到實際拿到支票共花了 2 個多月（05/20 成立 claim，07/26 才收到支票）<br />

中間來來回回跟 AIG 溝通（email）所須的文件，且之前已經交過的文件不知怎樣還是一直被要求要重交 <br />

且 AIG 的回信速度很慢，幾乎都要 5-7 個工作天才會回一封信，也因為這樣，處理時間才拖得很長 <br />

對比```購買保護```，美國運通僅花 7 天就從提交文件到審核通過，```手機保護``` AIG 則是花了 67 天才完成，美國運通的高效服務實在令人讚賞 <br />


衍伸閱讀：<br />
[美國運通（American Express）信用卡手機保護（Cell Phone Protection）申請教學，以 Mint 為例](https://ycjhuo.gitlab.io/blogs/AMEX-Phone-Protection-Mint.html)<br />

對購買保護有興趣的可參考：<br />
[美國運通（American Express）信用卡購買保護（Purchase Protection）提交文件](https://ycjhuo.gitlab.io/blogs/AMEX-Purchase-Protection-Submit.html)<br />
[美國運通（American Express）信用卡購買保護（Purchase Protection）申請教學](https://ycjhuo.gitlab.io/blogs/AMEX-Purchase-Protection.html)<br />

::: warning
點擊觀看更多： <br />
- [美國運通福利文](https://ycjhuo.gitlab.io/tag/Credit%20Cards/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::