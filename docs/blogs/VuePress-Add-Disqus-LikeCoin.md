---
title: '如何在 VuePress 加入 Disqus 及 LikeCoin Button ( 以 RECO 主題為例 )'
description: 'VuePress Disqus|VuePress LikeCoin Button|VuePress Disqus 教學|VuePress 留言|VuePress Disqus 安裝|VuePress LikeCoin Button 加入|VuePress LikeCoin Button 安裝'
date: 2020-12-06 23:47:04
tag: [VuePress, Disqus, LikeCoin, Blog]
category: Blog
---

上一篇[如何在 VuePress 中利用 Valine, Vssue 加入評論功能( 以 RECO 主題為例 )
](https://ycjhuo.gitlab.io/blogs/Vuepress-Use-Valine-Vssue.html) 提到了二個可以作為 Blog 留言工具的套件，那除了這二個，是否還有其他的呢？

這篇介紹的 Disqus 一個也是能夠讓 Blog 加入留言功能的套件，但在設定上跟前二個稍微不同。因為 RECO 主題還沒將 Disqus 作為內置的功能，因此我們須在程式碼內自行添加。

## 如何在 VuePress 加入 Disqus
首先註冊 Disqus 這方面就直接跳過。在註冊完後，會取得 shortname（由我們自行取名），而這個 shortname 將會是我們稍後要加入 config.js 中的參數。

![41-1](../images/41-1.png)

### 安裝 Disqus 套件
接著用 npm 或 yarn 安裝 Disqus 套件，安裝完後在 package.json 可以看到 Disqus 已被加進 devDependencies

```powershell
# 用 npm 安裝
$ npm i vuepress-plugin-disqus -D

# 用 yarn 安裝
$ yarn add vuepress-plugin-disqus
```

```javascript
"devDependencies": {
    "vuepress-plugin-disqus": "^0.2.0",
  },
```

### 複製 vuepress-theme-reco 資料夾
在 node_module 資料夾中找到 vuepress-theme-reco 資料夾，接著將整個資料夾貼到 .vuepress 資料夾底下，並將 vuepress-theme-reco 資料夾改名為 theme。

VuePress 在生成頁面時，會優先在 .vuepress 資料夾中搜尋 theme 資料夾，若找不到該資料夾才會讀取 node_module 裡面的主題。
因此，我們這麼做的用意是讓 vuepress 優先去讀取我們剛複製出來並取名為 theme 的這個資料夾。

::: tip
若 VuePress 有成功讀取剛剛複製出來的資料夾的話，可以看到編譯時，顯示 Apply theme local ...
:::
如下圖：

![41-2](../images/41-2.png)

### 修改 Page.Vue
在 .vuepress -> theme -> compoents 找到 Page.Vue 這個檔案，加入下面的 5 -11 行。就可以讓 Disqus 顯示在每篇文章的下方。

```vue
<ModuleTransition delay="0.32">
    <Comments v-if="recoShowModule" :isShowComments="shouldShowComments"/>
</ModuleTransition>

<ModuleTransition class="comments-wrapper">
    <template>
        <div>
          <Disqus/>
        </div>
    </template>
</ModuleTransition>
```
::: tip
我們可以從網頁的程式碼中看到文章下方被 ```<div class="comments-wrapper"></div>```
包裹起來，因此我們可用這個 class 來指定顯示 Disqus 的地方。
:::
![41-3](../images/41-3.png)

### 將 Disqus Plugin 加進 config.js

最後在 config.js 中，將我們一開始安裝的 Disqus 加入 plugins 區塊中，就可以看到 Disqus 已經成功顯示在文章下方了。
```javascript
plugins: {
    "disqus": {
      "shortname": "XXXXX"
    }
}
```

![41-4](../images/41-4.png)

---

## 如何在 VuePress 加入 LikeCoin Button
若有在用 LikeCoin 的也可以利用上面這個方法來將 LikeCoin Button 一併加入 VuePress，並顯示在文章下方。

### 將 LikeCoin Button 連結加進 Page.Vue
複製下方這段程式碼，並將裡面的 ycjhuo54702 改成自己的 LikeCoin 帳號，並貼在 Page.Vue 中的 Disqus 區塊上方，就可以看到 LikeCoin Button 出現在 Disqus 上方囉。

```javascript
<ModuleTransition class="comments-wrapper">
    <div>
        <a class="embedly-card" data-card-controls="0" href="https://button.like.co/ycjhuo54702"></a>
        <script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
    </div>
</ModuleTransition>
```

如下圖：

![41-5](../images/41-5.png)

不過這樣做其實會產生一個問題，就是每篇文章中的 LikeCoin Button 連結都是共通的，表示當有人在你的其中一篇文章按讚之後，他在你的其他文章中就會顯示已按過讚，沒辦法再給讚。

因此，我們須要將 LikeCoin Button 的連結再加上每篇文章的網址，來讓每篇文章都有獨立的 LikeCoin Button 連結。

將上面的 ModuleTransition 區塊改成下面這個，利用 currentPath 取得當前文章的網址。再將取得的網址加入到 likeCoinLink 這個變數中，最後把 likecoin 作為讀取的來源，加入到 iframe 中就可以了。
```javascript
<ModuleTransition class="comments-wrapper">
      <div>
        <iframe id="likeCoinButton" scrolling='no' frameborder='0' 
          sandbox='allow-scripts
                   allow-same-origin
                   allow-popups
                   allow-popups-to-escape-sandbox
                   allow-storage-access-by-user-activation'
          style='height: 212px; width: 100%;'
          src="">
        </iframe>

        <script type="text/javascript">
          //Get current path
          currentPath = location.href;
          likeCoinLink = 'https://button.like.co/in/embed/ycjhuo54702/button?referrer=' + currentPath;
          
          document.getElementById("likeCoinButton").src = likeCoinLink;
        </script>
      </div>
    </ModuleTransition>
```