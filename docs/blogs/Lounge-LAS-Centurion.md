---
title: 開箱！美國運通百夫長貴賓室（拉斯維加斯-麥卡倫國際機場 LAS Airport）
description: '美國運通貴賓室|百夫長貴賓室|拉斯維加斯貴賓室|麥卡倫機場貴賓室|Las Vegas Centurion Lounge|Las Vegas AMEX Lounge'
date: 2021-09-24
tag: [Lounge, Las Vegas]
category: Travel
---

藉著拉斯維加斯五天四夜的旅行，總算有機會來看看，究竟賭城-拉斯維加斯的百夫長貴賓室裡面長什麼樣子呢？<br />

拉斯維加斯的機場的貴賓室是在檢查行李的外面，對於僅是體驗貴賓室，而須再轉換航廈的人，非常的方便 <br />

::: tip
貴賓室須在班機起飛前的三小時內才可進入，若是太早到貴賓室，服務人員會請你先在外面等，時間到了才會讓你入內
:::

## 大廳＆休息區
大門一樣維持著美國運通百夫長貴賓室（The Centurion Lounge）一貫的藍色外觀
![LAS The Centurion Lounge 大門](../images/114-01.jpg)

在櫃檯檢查過登機證＆美國運通白金卡後，即可進到大廳
![LAS The Centurion Lounge 大廳](../images/114-02.jpg)

可能是因為這次班機的時段較冷門（週五下午，拉斯維加斯飛往洛杉磯），休息區的位置很多
![LAS The Centurion Lounge 休息區](../images/114-03.jpg)

美國運通的特殊擺飾
![LAS The Centurion Lounge 休息區](../images/114-04.jpg)

## 用餐區
下圖是熟食區，由左至右分別是：墨西哥辣椒，馬鈴薯沙拉（熱），牛排（太熱門已被拿完），義大利麵
![LAS The Centurion Lounge 用餐區](../images/114-05.jpg)

下面二張圖則是冷盤區的沙拉＆水果
![LAS The Centurion Lounge 用餐區](../images/114-06.jpg)
![LAS The Centurion Lounge 用餐區](../images/114-07.jpg)

## 調酒區
總是排著長長隊伍的調酒區，雖然在貴賓室裡是吃到飽的概念，但多數人在點調飲料時仍會給調酒師 $1 - $2 的小費（不喝酒的人也可以點可樂或是氣泡水）
![LAS The Centurion Lounge Lounge Bar](../images/114-08.jpg)

## 後記
不愧是要持有年費 $695 （約台幣 2.1 萬）的白金卡才可進入的百夫長貴賓室，休息區與餐點的選擇都比航空公司自營的貴賓室來得多，雖然目前仍有不少機場的百夫長貴賓室仍因疫情而暫時關閉，但這次能有機會進入，真的覺得很幸運，也讓常搭飛機的人更願意持有美國運通白金卡（Platinum）來取得百夫長貴賓室的資格



看更多美國運通百夫長貴賓室（The Centurion Lounge）：<br />
[開箱！美國運通百夫長貴賓室（紐約-約翰·甘迺迪國際機場 JFK Airport）](https://ycjhuo.gitlab.io/blogs/JFK-Centurion-Lounge.html) <br />

[開箱！美國運通百夫長貴賓室（西雅圖-塔科馬國際機場 SEA Airport）](https://ycjhuo.gitlab.io/blogs/SEA-Centurion-Lounge.html) <br />