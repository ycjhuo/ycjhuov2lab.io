---
title: 為買推特賣股？馬斯克賣股如何影響特斯拉股價
description: '美股 特斯拉|特斯拉 TSLA|TSLA Twitter|特斯拉 推特|馬斯克賣股|Elon Musk Sell Stock|馬斯克 抵押 質押 股票'
date: 2022-05-09
tag: [Stock, TSLA]
category: Investment
---

馬斯克在 4/14 首次提出要以 440 億美元收購推特（Twitter），消息公布後，特斯拉股價在這二週下跌了 11.6%  <br/>
而這是因為收購推特的原因，或是受到聯準會升息的影響呢？

馬斯克雖身為地球首富，440 億對他來說完全不成問題，但他的財富主要來自特斯拉＆其它未上市公司的股權，身上自然不會有 440 億的現金  <br/>

因此，為了籌措收購推特的資金，馬斯克已抵押了特斯拉股票給銀行來取得 125 億現金，此外，還須再拿出 210 億現金，才能湊齊收購推特的 465 億

因此，為了拿出 210 億的自備款，馬斯克只能選擇賣股，而我們要怎麼知道馬斯克賣了多少股，而這是否真的會影響特斯拉股價呢？


## 為收購推特而賣股
所幸，身為特斯拉 CEO，且持股 10%+ 的大股東，馬斯克買賣股票的交易均須上報到 SEC  <br/>
所以我們只要到 SEC Form 4 查詢即可知道馬斯克的確在 4/26 - 4/28 這三天賣出了總額 85 億的特斯拉股票（如下表）

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">4/26</th>
    <th align="center">4/27</th>
    <th align="center">4/28</th>
    <th align="center">Total</th>
  </tr>
  <tr>
    <td align="center">賣出股數</td>
    <td align="center">4,069,399</td>
    <td align="center">  345,601</td>
    <td align="center">5,230,000</td>
    <td align="center">9,645,000</td>
  </tr>
  <tr>
    <td align="center">交易金額（百萬）</td>
    <td align="center">$3,679</td>
    <td align="center">$  310</td>
    <td align="center">$4,528</td>
    <td align="center">$8,517</td>
  </tr>
  <tr>
    <td align="center">每股平均價格</td>
    <td align="center">$901</td>
    <td align="center">$898</td>
    <td align="center">$866</td>
    <td align="center">$883</td>
  </tr>
</table>

上表可看出，這三天馬斯克共賣出約 965 萬股的特斯拉股票，總金額約 85 億，平均價格為 $883

截至 4/28 賣完後，馬斯克身上仍約有 1.63 億股的特斯拉，這 965 萬股僅佔了馬斯克持有股數的 6%

受到這強大的賣壓影響，這三天特斯拉的股價從 995.43 → 877.51，下跌了 12%  <br/>
而在該星期，特斯拉共下跌了 11.05%，可看出，除了馬斯克賣股的這三天，其餘二天，特斯拉的股價都是上漲的  <br/>
同一星期，以科技股為主的那斯達克指數（Nasdaq Index）下跌了 3.25%，表示馬斯克賣股才是特斯拉下跌的主要原因

而在這星期，隨著聯準會公布未來升息＆縮表的消息，那斯達克指數下跌了 1.52%，但特斯拉卻逆勢上漲了 0.57%  <br/>
代表著，若馬斯克不賣股，雖然市場表現不好，有著良好財報加持的特斯拉，仍能打敗大盤

::: tip
特斯拉在這二星期（4/25 - 5/6）的平均交易量約 2,936 萬  <br/>
而在馬斯克賣股的那三天（4/26 - 4/28），平均交易量為 3,756 萬，比平時高出了 28%
:::


## 為繳稅而賣股
到底是不是馬斯克每次賣股都會造成股價下跌呢？來回顧 11/6，馬斯克在推特上發文詢問大家是否要賣出 10% 的股份來繳稅

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">11/09</th>
    <th align="center">11/10</th>
    <th align="center">11/11</th>
    <th align="center">11/12</th>
    <th align="center">11/15</th>
    <th align="center">11/16</th>
  </tr>
  <tr>
    <td align="center">賣出股數</td>
    <td align="center">1,553,479</td>
    <td align="center">  500,000</td>
    <td align="center">  639,737</td>
    <td align="center">1,200,000</td>
    <td align="center">  934,091</td>
    <td align="center">  934,091</td>
  </tr>
  <tr>
    <td align="center">交易金額（百萬）</td>
    <td align="center">$1,750</td>
    <td align="center">$  527</td>
    <td align="center">$  687</td>
    <td align="center">$1,236</td>
    <td align="center">$  931</td>
    <td align="center">$  973</td>
  </tr>
  <tr>
    <td align="center">每股平均價格</td>
    <td align="center">$1,127</td>
    <td align="center">$1,055</td>
    <td align="center">$1,074</td>
    <td align="center">$1,030</td>
    <td align="center">$  996</td>
    <td align="center">$1,042</td>
  </tr>
</table>

在這六個交易日（11/9 - 11/16），馬斯克共賣了 576 萬股，總金額約 61 億，平均價格為 $1,060，雖說賣股幅度不如這次賣推特的大，但仍造成特斯拉股價在這六天下跌 10.13%，同時間的那斯達克指數（Nasdaq Index）僅下跌 0.31%

不過依據 SEC 的資料顯示，馬斯克在 11/16 後到 12/28 之間，仍有 8 天是持續賣股的  <br/>
若將區間拉長，統計 11/9 - 12/28 這部分，特斯拉股價下跌了 7.25%，而同時期的那斯達克指數則是下跌 1.51%


## 後記
最後總結一下：

::: tip
為收購推特而賣股（4/26 - 4/28）：
- 共賣出約 965 萬股的特斯拉股票，總金額約 85 億，平均價格為 $883
- 造成股價下跌 12%，同時期的那斯達克指數下跌 0.4%

為繳稅而賣股（11/9 - 11/16）：
- 共賣出約 576 萬股，總金額約 61 億，平均價格為 $1,060
- 造成股價下跌 10.13%，同時間的那斯達克指數（Nasdaq Index）下跌 0.31%
:::

雖說短期間的大量賣股造成了市場上的買方無法迅速消化賣壓，讓股價的跌幅較大盤來得大，但若排除馬斯克賣股的因素，其餘時間特斯拉股價表現的確都優於大盤

回到文章一開始所說的，馬斯克為了收購推特須自備 210 億現金，而經過這三天的賣股，僅得到約 85 億，剩下的 125 億是否代表著還有另一輪的賣股？

所幸，幣安（Binance）與 Oracle 的創辦人等其他的投資者已確定要投資 71 億，協助馬斯克成功收購推特，這也減輕了馬斯克的資金不足問題

但仍無法保證馬斯克不須再賣股前籌集資金，但因馬斯克已抵押了特斯拉股票給銀行已取得資金，若再因自己賣股而導致特斯拉股價下跌，馬斯克就須拿出更多股票來做抵押，或是繳交保證金給銀行，這二者都不是目前的馬斯克所願意見到的

在這進退二難的情況下，我認為馬斯克並不會再繼續賣股導致股價下跌，而會轉而尋找更多投資人加入收購推特的計畫，以減輕自身的財務負擔

因此，特斯拉股價已可排除賣股的風險，僅剩下市場在升息恐慌下的風險，而由於特斯拉在大部分的時間下，股價表現都優於大盤，我認為特斯拉在財報表現均比其它科技股好的情況下，下跌的空間是有限的，就算現在買入後股價繼續下跌，第二季財報公佈後，也將會再次推升特斯拉的股價

若對馬斯克賣股資訊（SEC-Form-4）有興趣的，可到我的 [Google Sheet](https://docs.google.com/spreadsheets/d/1lgR2upioqGPZBsOeOpG_MjpL1MiUb2NO8OIt_ZytA-0/edit?usp=sharing) 觀看詳細數據


::: warning
點擊觀看更多：  <br/>
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::