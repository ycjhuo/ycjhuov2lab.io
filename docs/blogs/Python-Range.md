---
title:  Python 迴圈 佔位符範例 字串補空格範例
description: 'Python 迴圈教學 | Pyhton 迴圈介紹 | Pyhton 佔位符 | Python 字串補空格 | Python 輸出顯示 '
date: 2021-03-05 00:39:21
tag: [Python]
category: Programming
---

## 迴圈 for in range()
下面例子是：印出從 1 加到 10 的結果。
利用變數 i 在 range（）這個範圍中從 1 開始跑到 10 就停 ( 11 不算)<br />
最後分別把 i 儲存到 a 這個變數中，最後 a 就是 i 從  1 加到 10 的總和。

```python
a = 0
for i in range (1 , 11):
    # 也可以寫成 a += i 
    a = a + i
    print("a = ", a)
```
:::tip
若 range 內有三個參數 eg. for i in range (1, 11, 2) <br />
則表示 從 1 印到 10，中間一次跳二個數，變成印出 1, 3, 5, 7,9 <br />
沒寫的話則預設為 1 <br />
:::

## 二個變數運算及佔位符
```python
# 1 hour = 60 minutes x 60 seconds
hourToSec = 60 * 60
# 1 minute = 60 seconds
minuteToSec = 60

# eg. 一小時 10 分是幾秒
getSecs = hourToSec * 1 + minuteToSec * 10
print(" 1:10:00 = %d seconds" % getSecs) # 1:10:00 = 4200 seconds
```
![59-01](../images/59-01.png)

::: tip
%d 用於顯示整數 <br />
%f 用於顯示小數（浮點數）<br />
%s 用於顯示字串（string）<br />
%% 單純輸出百分號（%）<br />
:::

## Python format 輸出顯示
```python
print('Using format')
aa = 123
str1 = 'abcd'
# 8d 表示印出共八位數的字串，（前方的 123 加上後面的 5 個空白）
# 加上 小於符號 (<) 則表示空白加在參數 (123) 的後面，沒加 < 則是加在（參數）前面
print(format(123, '<8d'), format(123, '8d')) # '123     ', '     123'

# 加號(+)也算一個字元，因此空格變為 4 個 
print(format(aa, '+8d'), format(-123, '<+8d')) # '    +123', '-123    '
print(format(str1, '8s'), format("abcd", '>8s')) # 'abcd    ', '    abcd'

print()
print('Using .format')
# .format 的用途跟上面提到的佔位符一樣，都是用來將參數帶到前方的括號中 { }
print('{0:8d} {1:8d}'.format(456, 456), '{0:8d} {1:8d}'.format(456, 456)) # '     456      456', '     456      456'
print('{0:<8d} {1:+8d}'.format(456, 456)) # 456          +456
print('{0:8s} {1:>8s}'.format("abcd", str1)) # abcd         abcd

print()
print('Using %')
print('%-8d %8d'%(aa, 123), '%-8d %8d'%(aa+123, 456)) # '123           123', '246           456'
print('%+8d %+8d'%(123, -aa)) #     +123     -123

# 因為 %d 是用來顯示整數，而參數的 abcd 是字串，因此會出現下面這個錯誤訊息
print('%8ds %-8s' %("abcd", "abcd")) # TypeError: %d format: a number is required, not str
```
![59-02](../images/59-02.png)