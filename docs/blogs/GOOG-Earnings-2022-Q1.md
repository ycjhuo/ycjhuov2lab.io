---
title: Google（GOOG）2022 Q1 季報，拆股前的股價表現如何？
description: '美股投資 Google GOOG|Google 拆股 |Google GOOG 2021 Q4 第四季財報分析|拆股股價上漲|美股 拆股 影響'
date: 2022-07-16
tag: [Stock, GOOG]
category: Investment
---

Google 自 2004 上市以來，在 2014 進行了第一次的股票分割，而在下週一（7/18），Google 即將完成第二次的股票分割（20 for 1）<br />
屆時，目前股價 2255 的 Gooogle 股價將會重回百元（$112.75），對於資金不足的投資者來說，無疑是個好消息

Google 從公佈 2021 Q4 到 2022 Q1 財報的這段期間，股價下跌了 13.33%，而同期間的 S&P 500 指數則是下跌了 -8.17%

在股價持續下跌的時候，是否要在拆股後就趕快買入 Google 呢？就從 2022 Q1 的財報來分析 Google 目前的狀況

## Google 2022 Q1 財報關鍵數據
[來源](https://abc.xyz/investor/static/pdf/2022Q1_alphabet_earnings_release.pdf?cache=d9e9d97)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q4</th>
    <th align="center">2022 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">75,325</td>
    <td align="center">68,011</td>
    <td align="center">5.23%</td>
    <td align="center">15.67%</td>
    <td align="center">-9.71%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">24,402</td>
    <td align="center">18,934</td>
    <td align="center">4.91%</td>
    <td align="center">5.80%</td>
    <td align="center">-22.41%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">56.21%</td>
    <td align="center">56.48%</td>
    <td align="center">-0.03%</td>
    <td align="center">-1.38%</td>
    <td align="center">0.27%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">29.05%</td>
    <td align="center">29.55%</td>
    <td align="center">1.01%</td>
    <td align="center">-3.24%</td>
    <td align="center">0.49%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">30.69</td>
    <td align="center">24.62</td>
    <td align="center">33.46%</td>
    <td align="center">-15.64%</td>
    <td align="center">-19.78%</td>
  </tr>
</table>

- 總營收：與 2021 Q4 相比，衰退 9.71%，其中衰退幅度最多的業務為 YouTube 廣告，與上季相比下滑了 20.43%，但若與去年同期相比，仍成長了 14%
- 毛利＆營業利益率：在本季分別為 56.48% & 29.55%，與上季幾乎持平，顯示了 Google 在市場上仍保有競爭優勢
- 稅前淨利：較上季衰退了 22%，較去年同期衰退 23%，主要原因是在市場的劇烈波動下，Google 本季的股權投資部分，虧損了 10.7 億
- EPS：由於稅前淨利的大幅減少，EPS相較上季＆去年同期，分別衰退了約 16% & 20%

因 Google 的財報受季度影響，歷史紀錄顯示，Google 整年營收在 Q1 時最低，Q4 達到最高，也因此本季的營收、營業利潤皆低於上個季度，若與去年同期比較的話，年成長則都是上升的趨勢

Google 在各項業務的營收成長幅度，表現最好的無疑是雲端業務，所貢獻的營收較上季增加 5%（也是唯一正成長的業務），跟去年同期相比，也增加了 44%，且在成長的同時，虧損的部分也正逐漸減少，目前佔總營收約 8.6%


## Google 庫藏股＆拆股
Google 在本季花費了 133 億來買回庫藏股，幾乎相等於本季的稅前淨利（136 億），且財報揭露，董事會已決定再撥出 700 億來購買庫藏股（2021 購買庫藏股總金額約 500 億），可看出即使市場在加息的環境下，Google 仍認為目前股價是被低估的，而持續在市場上買回自己的股票

另外 Google 也在上季財報中提到將在 7/15 完成拆股（1 股變 20 股），剛好市值接近的 Amazon 也在上個月完成拆股（6/6），來看看 Amazon 在宣布拆股＆拆股後的股價表現如何：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">宣布拆股日 → 實際拆股日</th>
    <th align="center">實際拆股日 → 今天股價（7/15）</th>
  </tr>
  <tr>
    <td align="center">Amazon（AMZN）vs. 同期間 S&P 500</td>
    <td align="center">-10.4% vs. -3.66%</td>
    <td align="center">-9.01 vs. -6.27%</td>
  </tr>
  <tr>
    <td align="center">Apaphbet（GOOG）vs. 同期間 S&P 500</td>
    <td align="center">-18.21% vs. -15.03%</td>
    <td align="center">N/A</td>
  </tr>
</table>

上表中可看出，Amazon 在宣布拆股 → 實際拆股這個區間的表現，遠落後於大盤（S&P 500），而拆股後 → 現在的這一個月，股價表現仍舊遜於大盤 <br />
若再觀察 Google 從宣布拆股 → 實際拆股這個區間，Google 的股價表現仍比大盤差，就可推測出，在目前的市場環境下，拆股已不再是推動股價上升的關鍵了

## 後記
Google 身為在 2021 年表現最好的權值股，股價在 2022 的表現卻已下跌了 22%，雖然報酬率不如投資人的預期，但跟其它權值股相比，仍屬於前段班

![Google 股價 2022 表現](../images/186-01.png)

不過本益比也因為這個原因，來到了歷史新低的 20.2（疫情爆發時，本益比仍有 22.5）<br />

雖說目前市場普遍抱持悲觀態度，但從財報仍能看出 Google 強大的獲利能力 <br />
加上拆股後，股價較易吸引新投資人以及公司加強實施庫藏股的政策，現在 Google 仍是值得逢低買入的標的

::: warning
點擊觀看更多： <br />
- [Google 分析文](https://ycjhuo.gitlab.io/tag/GOOG/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
