---
title: 開箱！加州迪士尼樂園（Disneyland Park）必玩設施（星際大戰、印第安瓊斯）
description: '加州迪士尼 介紹|加州迪士尼 推薦|加州迪士尼 必玩|Disneyland Park 介紹|Disneyland Park 推薦|Disneyland Park 必玩|Disneyland Park 心得'
date: 2021-10-15
tag: [Los Angeles, Disney]
category: Travel
---

加州迪士尼樂園（Disneyland Park），從機場（LAX）搭 Uber 過去約 40 分鐘即可到達，費用約 $60 <br />
樂園位於 Anaheim，附近幾乎都是飯店，治安相比 LA Downtown 來說，安全很多，我們玩到樂園關門（23:00）走回飯店，也不會覺得危險 <br />

我們這次選擇的是走路距離迪士尼樂園僅 15 分鐘的 Hilton DoubleTree，有興趣的可參考這篇： <br />
[開箱！洛杉磯-希爾頓 Anaheim DoubleTree 飯店（DoubleTree Suites by Hilton Hotel Anaheim Resort - Convention Center）](https://ycjhuo.gitlab.io/blogs/Hotel-LA-Anaheim-Hilton-DoubleTree.html)

## Disneyland
加州迪士尼分為 Disneyland 與 Adventure 二種：
- Disneyland：遊樂設施較為和緩，適合帶小朋友的家長，或不喜歡太刺激的人
- Adventure：設施都較為刺激，適合喜歡挑戰自己膽量以及心臟強度的人

票價為：$104 - 154 之間，每天都不同，假日較貴，但晚上的煙火只有在六日才有 <br />
或是再 + $55 就可以二個樂園都進去（Park Hopper 方案），但一個樂園就可以玩一整天了，不建議同一天一次去二個樂園 <br />

這次介紹的設施都是 Disneyland Park

到樂園前可先下載 Disneyland 這個 App，上面顯示了園區地圖、我們目前的所在位置，以及每個設施的類型、大概等待時間等等


下面就介紹我認為 Disneyland 中最好玩的三個設施：
## Star Tours - The Adventures Continue
位於 Disneyland Park 右下角，Tomorrowland 園區的 ```Star Tours - The Adventures Continue``` <br />
設施類型為： Small Drop，Thrill Rides，Dark，Loud <br />

其實就是 4D 電影院，會配戴 3D 眼鏡，身歷其境的體驗一段星際大戰的（原創）劇情，真的可以體會到身歷其境的感覺，因此我把這項設施排為第一 <br />
![加州 Disneyland Park 必玩 Star Tours - The Adventures Continue](../images/125-02.jpg)

## Millennium Falcon: Smugglers Run
第二個必玩的設施位於 Disneyland Park 左上角的 Star Wars: Galaxy's Edge 園區，也是屬於星際大戰類型的設施 <br />
設施類型為： Small Drop，Thrill Rides，Dark，Loud <br />

六個人一組進入船艙（千年鷹），分別擔任船長（操控方向）、射擊手（攻擊敵軍）、維修工（修補損害的地方）<br />

一樣是有點類似 4D 電影，座位會隨著劇情晃動，而我們也須跟著指令做出相對應的動作（但其實沒做一樣能繼續），最後結束則會統計分數 <br />
![加州 Disneyland Park 必玩 Millennium Falcon: Smugglers Run](../images/125-03.jpg)

值得一提的是，Star Wars: Galaxy's Edge 園區的擬真度很高，個人覺得是 Disneyland 的所有園區中最讓人驚豔的 <br />
![加州 Disneyland Park Star Wars: Galaxy's Edge 園區](../images/125-04.jpg)
![加州 Disneyland Park Star Wars: Galaxy's Edge 園區](../images/125-05.jpg)
![加州 Disneyland Park Star Wars: Galaxy's Edge 園區](../images/125-06.jpg)

## Indiana Jones Adventure
最後一個是位於 Disneyland Park 左下角的 Adventureland 的 Indiana Jones Adventure <br />
設施類型為： Small Drop，Thrill Rides，Dark，Loud，Scary <br />

乘坐印第安納·瓊斯的探險車，前往地下考古，過程有點像小型雲霄飛車，但是是室內的

![加州 Disneyland Park 必玩 Indiana Jones Adventure](../images/125-07.jpg)

## 後記
這次去的時間為九月底，時間上已經快接近聖誕節了，因此園區內也有不少萬聖節的裝飾 <br />
![加州 Disneyland Park 萬聖節限定](../images/125-08.jpg)
![加州 Disneyland Park 萬聖節限定](../images/125-09.jpg)

Disneyland 不愧是以兒童為導向的樂園，我選擇的這三個已經算是裡面最刺激的設施了，但還是有很多小朋友也有一起玩 <br />

我在非寒暑假，且非假日的時候去，光一個設施就要排 40 - 60 分鐘了，若是不想花時間排隊在不喜歡的設施上，且也喜歡稍微刺激的人，可以優先選擇上面介紹的三種設施 <br />