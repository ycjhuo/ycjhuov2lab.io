---
title: 如何讓 Python 在打包後讀取外部 ini 設定檔（含有中文字）
description: 'Python 讀取 INI 檔 | Pyhton ini 教學 | Python ini 中文 | python MissingSectionHeaderError' 
date: 2021-07-28
tag: [Python, Sendmail, ini, Pyinstaller]
category: Programming
---

上篇 [如何讓 Python 讀取外部 ini 設定檔](https://ycjhuo.gitlab.io/blogs/Python-Read-Ini-File.html)，介紹了如何用 python 讀取 ini 檔 <br />
若 ini 裡面有中文字 或是 程式須經過 ```Pyinstaller``` 打包使用的話，可參考以下的注意事項及教學 <br />

## ini 含有中文字
若 ini 檔中含有中文字的話，則 encoding 要改成 ```utf-8-sig``` 才可成功讀取 <br />

例如下面這個 ini 檔：<br />

```ini
[File]
Path = \\8.8.8.8\d$\Public_Data\Private\02.B&C 聯同\Daily FFCDB
```

在路徑中含有中文，若我們用了 ```config.read('config.ini', encoding='utf-8')'``` <br />
程式執行時就會出現```MissingSectionHeaderError```這個錯誤訊息 （如下）：

```
configparser.MissingSectionHeaderError: File contains no section headers.
file: 'C:\\Users\\009641\\Desktop\\code\\codes\\ImportFFCDB\\config.ini', line: 1
'\ufeff[File]\n'
```
這是因為 ini 檔裡面的中文字，導致```configparser``` 在 ```config.ini``` 中，找不到相對應的 Section <br />
雖然 ini 檔中，我們的確有 File 這個 Section，但程式會將這個 Section 判斷成 ```'\ufeff[File]\n'```


因此，若將 ```encoding``` 改為 ```utf-8-sig```（如下）：

```python{2}
config.read('config.ini', encoding='utf-8')
config.read('config.ini', encoding='utf-8-sig')
```

程式即可成功取得 ini 檔的資訊

## Pyinstaller 打包後讀取外部檔案
程式在透過 ```Pyinstaller``` 打包成 exe 檔，且須讀取外部檔案時，就須在程式中加入下面這段：

```python {4-7}
config = configparser.ConfigParser()

# determine if application is a script file or frozen exe
if getattr(sys, 'frozen', False):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(__file__)

config_path = os.path.join(application_path, 'config.ini')
print('config_path = ', config_path)

#config.read('config.ini', encoding='utf-8')
config.read(config_path, encoding='utf-8-sig')
```

這段是用來判斷程式是直接執行 ```py``` 檔，或是透過 ```exe```執行，並依據執行方式的不同來得到執行路徑 <br />
因為 ```pyinstaller``` 在執行時會先進行解壓縮，在解壓縮後，程式的路徑就不會是一開始我們在程式中寫的路徑 <br />

因此為了讓程式在打包後還能依據 ```exe``` 檔案的相對位置找到 ini 檔，我們得在程式中加上判斷執行路徑才可成功取得 ini（或其他外部）檔 <br />
