---
title: 開箱！洛杉磯-希爾頓 Downtown DoubleTree 飯店（DoubleTree by Hilton Hotel Los Angeles Downtown）
description: 'LA 飯店推薦|LA 洛杉磯飯店推薦|LA 洛杉磯飯店介紹|LA 洛杉磯 飯店 希爾頓推薦|LA 洛杉磯 飯店評價|LA 洛杉磯 飯店 CP 值'
date: 2021-09-26
tag: [Hotel, Hilton, Los Angeles]
category: Travel
---
::: tip
入住時間：2021/09/24 - 09/26 <br />
房型：1 King Bed <br />
每晚價格（稅後）：$207.48 / 49,000 點 <br />
Google 評價：4.2 顆星（2,624 評價）
:::

這次在 LA Downtown 的三天二夜行程選擇了市中心的希爾頓 DoubleTree 飯店，下面就來看看 Hilton DoubleTree Downtown LA 的介紹吧

## 飯店外觀＆大廳
從飯店外觀來看，不僅有 DoubleTree 的 Logo，旁邊還有 Kyoto Garden 的 Logo，可以看得出是一家具有日式風格的飯店
![Hilton DoubleTree Downtown LA 外觀](../images/112-01.jpg)

進入大廳後，左邊是 Check in 櫃檯，右邊則是 Lounge Bar
![Hilton DoubleTree Downtown LA 大廳](../images/112-02.jpg)

## Lounge Bar
因疫情關係，目前美國境內的 Hilton 都不提供早餐 <br />
替代方案則是一天給 $30 credit（黃金會員），可用在飯店內的 Lounge Bar，點了 2 杯調酒剛好用掉
![Hilton DoubleTree Downtown LA Lounge](../images/112-08.jpg)

## 房間＆浴室
這次房間選擇的是 King Bed 的 Bathtub 房型，雖然櫃檯說有升級到 City View 的房間，但打開窗戶真的無感
![Hilton DoubleTree Downtown LA 房間](../images/112-03.jpg)

浴室雖然維持的很乾淨，但還是看得出老式建築的陳舊感
![Hilton DoubleTree Downtown LA 浴室](../images/112-04.jpg)
![Hilton DoubleTree Downtown LA 浴室](../images/112-05.jpg)

盥洗用具則是提供知名護膚品牌瑰柏翠（Crabtree & Evelyn）
![Hilton DoubleTree Downtown LA 盥洗用具](../images/112-06.jpg)

## 後記
以上就是 Hilton DoubleTree Downtown LA 的介紹，可以看出雖然飯店外觀＆大廳雖然維持的不錯，但房間本身其實蠻舊的，也沒有微波爐＆冰箱，而作為主打的日式花園，看了之後因為找不到亮點，就不多做介紹了 <br />

若要說優點的話，可能只有位置不錯，距離各個景點都是走路就可到的距離，例如：<br />
天使鐵路、The Last bookstore、The Board、Grand Central Market <br />

但其實這是優點也是缺點，LA towntown 的治安其實不太好，飯店外面二條街就能看到許多流浪漢及帳篷，建議來玩的人，晚上六點後就盡量避免出門

綜合以上原因，感覺不值得用 49k 點數（現金含稅價則是 $207.48） 的兌換價值（2021/09/24）

![Hilton DoubleTree Downtown LA 外圍](../images/112-09.jpg)
![Hilton DoubleTree Downtown LA 外圍](../images/112-10.jpg)


點擊 [飯店開箱](https://ycjhuo.gitlab.io/tag/Hotel/)，看更多飯店開箱 <br />
或 [Los Angeles](https://ycjhuo.gitlab.io/tag/Los%20Angeles/)，看 Los Angeles 景點介紹 <br />

