---
title: 開箱！紐約-希爾頓 Niagara Falls DoubleTree 飯店（DoubleTree by Hilton Hotel Niagara Falls New York）
description: '尼加拉瀑布 飯店推薦|尼加拉瀑布 希爾頓|Niagara Falls 飯店推薦|Niagara Falls 希爾頓飯店|Niagara Falls 飯店評價'
date: 2021-10-13
tag: [Hotel, Hilton, New York, Niagara Falls]
category: Travel
---
::: tip
入住時間：2021/10/09 - 10/11 <br />
房型：1 King Bed - Fridge <br />
每晚價格（稅前）：$247 / 50,000 點 <br />
Google 評價：4.3 顆星（2,127 評價）
:::

這間 Hilton DoubleTree 距離著名的尼加拉瀑布（Niagara Falls）僅有 1.12 km，走路只要 15 分鐘即可到達尼加拉瀑布州立公園（Niagara Falls State Park）<br />
走路到位於山羊島（Goat Island）的瀑布觀景平台（Terrapin Point）也只要 20 分鐘，優良的地理位置是來尼加拉瀑布的最佳選擇之一 <br />

## 飯店外觀＆大廳
Hilton DoubleTree Niagara Falls 算是蠻新的建築，地上 9 樓，地下 1 樓，地下室有泳池＆健身房
![Hilton DoubleTree Niagara Falls NY 外觀](../images/123-01.jpg)

進入大廳後，前方就是 lounge bar + 餐廳，早上則是吃早餐的地方，左邊是 Check-in 櫃檯
![Hilton DoubleTree Niagara Falls NY 大廳](../images/123-02.jpg)

Hilton DoubleTree Niagara Falls 的服務人員我覺得在 DoubleTree 裡面算是很友善的，且除了每天提供的 2 瓶水之外，如果想再多拿，他們也很樂意提供 <br />

![Hilton DoubleTree Niagara Falls NY 櫃檯](../images/123-03.jpg)

Check-in 櫃檯旁還設有 ATM 提款機＆微波爐
![Hilton DoubleTree Niagara Falls NY 櫃檯](../images/123-04.jpg)

## 房間
這次因為在長週末時入住，且遇上有人在這邊辦婚宴，飯店住房全滿，沒有任何房間可供升級，房間被安排在 2 樓
![Hilton DoubleTree Niagara Falls NY 房間](../images/123-05.jpg)

除了 King size 床外，還配備一組沙發、沙發椅、小冰箱等
![Hilton DoubleTree Niagara Falls NY 房間](../images/123-06.jpg)

因為房間在 2 樓，且剛好被飯店陽台擋住，窗戶外面完全看不到尼加拉瀑布的景
![Hilton DoubleTree Niagara Falls NY 窗景](../images/123-07.jpg)

## 浴室
浴室為乾濕分離的無浴缸設計
![Hilton DoubleTree Niagara Falls NY 浴室](../images/123-08.jpg)

備品是知名護膚品牌瑰柏翠（Crabtree & Evelyn）
![Hilton DoubleTree Niagara Falls NY 備品](../images/123-09.jpg)

盥洗的空間很大，一次容納二個人也沒問題，裡面還設計了可以坐的地方
![Hilton DoubleTree Niagara Falls NY 浴室](../images/123-10.jpg)

## Lounge bar & 餐廳
因疫情關係，目前美國境內的 Hilton 都不提供早餐 <br />
相對的，飯店提供了 $ 12 credit（黃金會員）一天，二人入住則是$ 24，可用在飯店內的 Lounge Bar ＆ 餐廳（早晚菜單不同）<br />
![Hilton DoubleTree Niagara Falls NY Lounge bar](../images/123-11.jpg)

餐廳很大，早上提供一般西式早餐，晚上則為牛排館
![Hilton DoubleTree Niagara Falls NY 餐廳](../images/123-12.jpg)

餐廳外面在早上時段還有提供咖啡，可自由取用，但很淡，說是咖啡水也不為過
![Hilton DoubleTree Niagara Falls NY 咖啡櫃檯](../images/123-13.jpg)

$24 credit 剛好可以點一個主餐、貝果、蘋果汁（或是二個主餐）
![Hilton DoubleTree Niagara Falls NY 早餐](../images/123-14.jpg)

這間 Niagara Falls DoubleTree 除了早餐 credit 外，還額外提供了一人一張酒券（但其實只能折 $10），這點是其他 DoubleTree 所沒有的
![Hilton DoubleTree Niagara Falls NY Lounge bar](../images/123-15.jpg)

地下室的游泳池還具備了按摩池
![Hilton DoubleTree Niagara Falls NY Pool](../images/123-16.jpg)

## 後記
這間 Niagara Falls DoubleTree 不只飯店新，服務人員友善，還額外提供了酒券，外面治安也很好，晚上出門散步完全不用擔心
![Hilton DoubleTree Niagara Falls Perks](../images/123-17.jpg)

飯店內還有旅行社駐點提供各式行程以及接駁車到各大景點，若不租車也很容易到各個景點

若非旺季來的話，再搭配上尼加拉瀑布的景色，真的無可挑剃（僅限紐約端），若要去加拿大端的話，因為看瀑布的角度不同，且加拿大房價比紐約便宜許多，則可以有更多選擇 <br />


點擊 [飯店開箱](https://ycjhuo.gitlab.io/tag/Hotel/)，看更多飯店開箱 <br />
或 [Niagara Falls](https://ycjhuo.gitlab.io/tag/Niagara%20Falls/)，看 Niagara Falls 景點介紹 <br />
