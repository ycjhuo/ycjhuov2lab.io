---
title: Block（SQ）2022 Q4 財報，遭到放空的 SQ 前景如何
description: 'Block SQ 2022 Q4 第四季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報|Block 收購 Afterpay|Block 收購先買後付平台|Block buy now pay later” (BNPL)'
date: 2023-04-17
tag: [Stock, SQ]
category: Investment
---

Block（股票代號：SQ），從 Q3 - Q4 財報發布期間（11/3 → 2/23），股價上漲了 30%（$57.27 → $74.15），同期間 S&P 500 指數上漲 8%

雖說股價在這段時間表現不錯，但著名空頭公司興登堡（Hindenburg Research）在 3/23 發佈了看空 SQ 的報告，發佈後，SQ 股價直接跌到今年的最低點（$60.88），讓 SQ 的未來又多了一個不確定性

下面就來看看 Block（SQ）在 2022 Q4 的財報，了解 Block 目前的狀況

## Block 財務指標
[來源](https://s29.q4cdn.com/628966176/files/doc_financials/2022/q4/Block_Shareholder-Letter-4Q22.pdf)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">2022 Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">44 億</td>
    <td align="center">45.2 億</td>
    <td align="center">46.5 億</td>
    <td align="center">11.21%</td>
    <td align="center">2.52%</td>
    <td align="center">3%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">33.37%</td>
    <td align="center">32.7%</td>
    <td align="center">35.7%</td>
    <td align="center">0.67%</td>
    <td align="center">-0.67%</td>
    <td align="center">1%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-4.85%</td>
    <td align="center">-1.08%</td>
    <td align="center">-2.91%</td>
    <td align="center">0.87%</td>
    <td align="center">3.77%</td>
    <td align="center">-1.83%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">-0.36</td>
    <td align="center">-0.02</td>
    <td align="center">-0.19</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：接續 Q2 的趨勢，目前已連續三季營收成長，來到了 46.5 億（季成長 3%），各營收占比分別為：「比特幣」（39%），「交易」（32%），「訂閱服務」（28%），若與 2022 年初的營收相比的話，「比特幣」＆「訂閱服務」則分別是成長速度最快的類別
- 毛利率：由於毛利率最高的「訂閱服務」所帶來的增長，連帶將毛利率拉升 1%，來到 35.7%，2022 的平均毛利率為 34.18%
- 營業利潤率：因營收成長速度跟不上營業費用，本季營利率較上季降低 2%，為 -2.9%，2022 的平均營利率為 -3.6%
- EPS：由上季的 -0.02 降低至 -0.19，2022 整年的 EPS 為 -0.93（Q1 ＋ Q2 就佔了 80%）

在 2022 的最後一季，Block 因「訂閱服務」的營收逐季增長，帶動了毛利率的提升，由 32.7%（Q1）→ 35.7%（Q4），上升了 3%

營業利潤率則由 -5.73%（Q1） → -2.91%（Q4），也約上升了 3%

大部分的營業費用均來自於先前收購的 AfferPay 及相關的收購費用

![Block（SQ）2022 Q4 財報](../images/217-01.png)

## Block 財報重點
2022 Q4 實現了 16.6 億的毛利（年成長 40%），其中 Square 貢獻了 8.01 億（年成長 22%），而 Cash App 貢獻了 8.48 億（年成長 64%），淨虧損為 1.14 億

2022 整年的毛利為 59.9 億（年成長 36%），Square 貢獻了 30 億（年成長 30%），Cash App 貢獻了 29.5 億（年成長 43%），淨虧損為 5.41 億

### Square 的近況發展
1. 客戶黏性增加：
44% 毛利來自使用了 4 個以上 Square 服務的賣家，且對於在中端市場（總支付量為 12,5 - 50 萬）使用超過 4 種服務的賣家，比起只使用 1 種服務的賣家，高出 15 倍以上的客戶留存率

2. 搶攻高端市場：
目前中端市場（總支付量為 12,5 - 50 萬）的賣家，毛利的年成長率為 16%，而高端市場（年支付量 50 萬以上的賣家）的客戶約佔總交易量的 39%

3. 國際化：目前 Square 約有 17% 的毛利來自美國之外（約佔總毛利 8%）

### Cash App Ecosystem
Cash App 在 Q4 的營收 ＆ 毛利年成長率分別為 12% ＆ 64%，可說是在收購 AfferPay 後有著更明顯的成長，所實現的毛利＆成長性也超越了主打商家的 Square

截至 2022 年底，Cash App 有 5,100 萬的月活用戶（年成長 16%），約佔美國總人口的 16%，且 Cash App Card（Cash App 附贈的簽帳卡）在 2022 年貢獻了超過 7.5 億的毛利（年成長 56%），佔了總 Cash App 毛利的 26％

可惜的是由於 Block 在 2020 Q4 與 2021 Q1 分別購買 5,000 萬 與 1.7 億 比特幣，因此在 2022 Q4 提交了 900 萬的損失，2022 整年為 4,700 萬損失，這些無形資產的減損，也是 Block 在財報上無法實現獲利的原因之一


若排除比特幣無形損失，收購相關，以及股權相關損失，則 Block 調整後 Q4 的 EBITDA 為 2.8 億（年成長 52%）， 2022 整年則是 9.9 億，雖然 Block 在財報上說自己是用這個方式來衡量企業營運的狀況，但因為收購＆股權等相關費用仍是需要付出去的錢，投資人可做為參考即可


## 後記
Block 在收購了 AfterPay 後，雖然營收＆毛利雙雙提升了，但隨之而來的收購＆整合等事項，也大幅增加了 Block 的營業費用與收購相關支出，讓 Block 對於何時能實現財報上的獲利仍是個問號

最近又遭受到空頭公司的狙擊，對於 Block 的股價更是一大打擊，直接將原本在 $70 - $80 徘徊的股價，打壓到了 $60 - $70
種種不利因素下，更加考驗投資者對於 Block 的耐心，更別說是從去年 $275 高點買進的人

::: warning
點擊觀看更多： <br />
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
