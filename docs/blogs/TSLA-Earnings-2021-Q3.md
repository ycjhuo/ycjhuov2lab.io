---
title: 特斯拉（TSLA）公佈 2021 Q3 季報，股價寫下新紀錄
description: '特斯拉財報|TSLA 財報|特斯拉 2021 Q3 第三季財報|TSLA 2021 Q3 第三季財報'
date: 2021-10-22
tag: [Stock, TSLA]
category: Investment
---

特斯拉上季（Q2）財報回顧：[特斯拉（Tesla）2021 Q2 季報，打破依靠碳積分盈利的質疑](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q2.html)


相比 Q2 期間的股價表現（-11%），特斯拉在 Q3 的表現（+31.7%）總算扳回一城，並大幅領先大盤指數（同期 S&P 500 指數為 +2.58%） <br/>

這個季度不僅帶來了史上最好的財報，也將特斯拉的股價寫下新紀錄  <br/>

下面就來看看本季的財報： <br/>

## 特斯拉 2021 Q3 財報
[來源](https://tesla-cdn.thron.com/static/TWPKBV_TSLA_Q3_2021_Quarterly_Update_SI1AKE.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22TSLA-Q3-2021-Quarterly-Update.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">電動車總營收（百萬）</td>
    <td align="center">9,002</td>
    <td align="center">10,206</td>
    <td align="center">12,057</td>
    <td align="center">11.8%</td>
    <td align="center">15.35%</td>
  </tr>
  <tr>
    <td align="center">電動車總利潤（百萬）</td>
    <td align="center">2,385</td>
    <td align="center">2,899</td>
    <td align="center">3,673</td>
    <td align="center">17.73%</td>
    <td align="center">21.07%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">26.5%</td>
    <td align="center">28.4%</td>
    <td align="center">30.5%</td>
    <td align="center">1.9%</td>
    <td align="center">2.1%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">5.7%</td>
    <td align="center">11%</td>
    <td align="center">14.6%</td>
    <td align="center">5.3%</td>
    <td align="center">3.6%</td>
  </tr>
    <tr>
    <td align="center">EPS</td>
    <td align="center">0.39</td>
    <td align="center">1.02</td>
    <td align="center">1.44</td>
    <td align="center">61.76%</td>
    <td align="center">29.17%</td>
  </tr>
</table>

- 電動車營收：Q3 的季成長為 15.35%，連續二次季成長 > 10%（ Q2 季成長為 11.8％）
- 電動車利潤：Q3 利潤的季成長為 21.07%（ Q2 季成長為 17.73％）
- 毛利率：本季首次站上 3 字頭，為 30.5%，相比 Q2，上升了 2.1%
- 營業利益率：相比 Q2 的 11%，本季再次成長到了 14.6%
- EPS：Q1 EPS 為 1.44，季成長為 29.17%，成長度低於 Q2 的 61.76%

 <br/>

引用上篇 [特斯拉（Tesla）2021 Q2 季報，打破依靠碳積分盈利的質疑](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q2.html)提到的：
```
全球市值前 2 - 5 名車廠：豐田（TM），通用（GM），蔚來（NIO），福特（F）
毛利率皆 < 20%（特斯拉 Q2 為 28.4%，Q3 為 30.5%）
營益率皆 < 8%（特斯拉為 Q2 為 11%，Q3 為 14.6%）
```

可以看出，特斯拉已遠遠拉開與其他車廠的差距了，且現階段特斯拉的「自動駕駛」等其他高毛利率的「軟體訂閱服務」仍未全面開展，毛利率＆營業利潤率就能有此成績  <br/>
相信未來特斯拉會朝著與蘋果類似的軟硬體結合路線成長（蘋果毛利率約 43%，營業利益率為 29.6%） <br/>

接著來看是什麼原因讓特斯拉在本季有如此的高成長

## 特斯拉 2021 Q3 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">Q1 vs. Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">0</td>
    <td align="center">2,340</td>
    <td align="center">8,941</td>
    <td align="center">-100%</td>
    <td align="center">N/A</td>
    <td align="center">282.09%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">180,338</td>
    <td align="center">204,081</td>
    <td align="center">228,882</td>
    <td align="center">10.19%</td>
    <td align="center">13.17%</td>
    <td align="center">12.15%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">2,030</td>
    <td align="center">1,895</td>
    <td align="center">9,289</td>
    <td align="center">-89.3%</td>
    <td align="center">-6.65%</td>
    <td align="center">390.18%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">182,847</td>
    <td align="center">199,409</td>
    <td align="center">232,102</td>
    <td align="center">13.08%</td>
    <td align="center">9.06%</td>
    <td align="center">16.39%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">180,338</td>
    <td align="center">206,421</td>
    <td align="center">237,823</td>
    <td align="center">0.32%</td>
    <td align="center">14.46%</td>
    <td align="center">15.21%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">184,877</td>
    <td align="center">201,304</td>
    <td align="center">241,391</td>
    <td align="center">2.33%</td>
    <td align="center">8.89%</td>
    <td align="center">19.91%</td>
  </tr>
</table>

- Model S/X 生產量：從 Q2 的 2,340 增加到 8,941，季成長率為 282.09%（上季因 Q1 Model S 仍處在改款階段，季成長 N/A）
- Model 3/Y 生產量：從 204,081 -> 228,882，季成長 12.15%（與上季成長 13.17% 相差不大）
- Model S/X 交付量：從 Q2 的 1,895 增加到 9,289，季成長率為 390.18%（Model S 於 Q2 末完成改款，季成長為 -6.65%）
- Model 3/Y 交付量：從 199,409 -> 232,102，季成長 16.39%，相比 Q2 的季成長 9.06%，Q3 交付量有著顯著的成長

從 Q3 生產/交付量可以看出，本季財報的功臣無疑是 Model S/X，在 Q2 改款完成後，正式於 Q3 發揮全力生產  <br/>
而因 Model S/X 屬於高價車款（價格在 9 - 13萬 之間），相比於 Model 3/Y（價格在 4.2 - 6.2 萬），理所當然也拉高的特斯拉整體的毛利＆營業利益率  <br/>


## 後記
財報最後揭露了歐洲柏林工廠會在今年底取得生產許可，現階段雖然 Model 3/Y 的生產量已接近穩定（季成長 12%） <br/>
若柏林廠投入生產後 Model Y 產量即可再有大幅度的成長  <br/>

軟體方面，特斯拉已於 10 月推送 FSD 給更多車主，且還增加了 Disney+ 在車上及其他遊戲；同時特斯拉也在德州開始了保險業務  <br/>

種種跡象都可看出特斯拉除了主要的造車業務，在軟體，保險等各方面也正逐漸開展，可以想見未來的營收將會有多樣化的來源  <br/>

股價方面，特斯拉從 Q2 財報公佈時（07/26 盤後）的 $657.62 到了 Q3 公佈（10/20 盤後）時的 $865.8，上漲了 31.7%，且在這週結束（10/22）時迎來了股價的最高點 $909.68  <br/>

相比五月中時候的谷底（$563），五個月期間股價漲了 61.6%，若考慮到特斯拉目前的成長速度及未來的發展性，我認為特斯拉仍是不錯的選擇  <br/>