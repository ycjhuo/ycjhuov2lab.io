---
title: 開箱！亞特蘭大-希爾頓 The Candler Hotel Atlanta, Curio Collection（The Candler Hotel Atlanta, Curio Collection by Hilton）
description: 'Altanta 亞特蘭大飯店介紹|Altanta 亞特蘭大飯店推薦|Altanta 亞特蘭大飯店|Altanta 亞特蘭大飯店 CP 值|Altanta 亞特蘭大希爾頓 Hilton 介紹|Hilton Curio Collectio|希爾頓 Curio Collection 系列|Hilton Curio Collection 介紹'
date: 2022-06-25
tag: [Hotel, Hilton, Georgia, Atlanta]
category: Travel
---
::: tip
入住時間：2022/06/18 - 06/20 <br />
房型：Room, 1 King Deluxe-city View <br />
每晚價格（稅後）：$218.19 / 60,000 點 <br />
Google 評價：4.5 顆星（615 評價）
:::

這次在亞特蘭大的三天二夜行程，我們選擇了希爾頓的 The Candler Hotel Atlanta，位於 Atlanta Downtown

距離熱門景點奧林匹克公園（Centennial Olympic Park）、可口可樂博物館（World of Coca-Cola）、水族館（Georgia Aquarium）以及 CNN 總部（CNN Studio Tours）都只要走路 10 分鐘即可到達

下面就來開箱位於喬治亞州亞特蘭大的 The Candler Hotel Atlanta

::: tip
Curio Collection 是希爾頓自 2014 年才有的新品牌，以保有原本的特色而聞名，僅是掛上 Hilton 的招牌，且都是屬於 4、5 星級的高端酒店，截至 2021，全球僅有 129 間
:::



## 飯店外觀＆大廳（Hotel Building & Lobby）
The Candler Hotel 是一棟 17 層樓高的歷史建築，1906 年由可口可樂公司的創辦人 Asa Candler 建立，也是亞特蘭大的第一個高樓大廈，距離現在已有 117 年，從外觀能看出是一棟具有悠久歷史的建築物，且建築內外都有許多精美的動物雕刻，整棟建築風格以大理石為主

![The Candler Hotel Atlanta, Curio Collection 外觀](../images/180-01.jpg)

一進門就能看到的 Check-In 櫃檯
![The Candler Hotel Atlanta, Curio Collection 大廳](../images/180-02.jpg)

## 房間內部（Room, 1 King Deluxe-city View）
這次原本訂的只是 1 King Bed 房型，幸運被升到 City View 房型，但打開窗後，發現面對的是停車場，所以完全無感
![The Candler Hotel Atlanta, Curio Collection 房間](../images/180-03.jpg)

雖然有沙發＆桌子，不過插座都在沙發後面，要在這邊用電腦其實很不方便
![The Candler Hotel Atlanta, Curio Collection 房間](../images/180-04.jpg)

房間內的二瓶水，以及茶包等都可以直接取用，不會被另外收費
## 衛浴（Bathroom）
進門的左手邊就是浴室，非常寬敞，且淋浴區的磁磚還是防滑的
![The Candler Hotel Atlanta, Curio Collection 衛浴](../images/180-05.jpg)
洗手台則是配有化妝必備的放大＆發光鏡
![The Candler Hotel Atlanta, Curio Collection 廁所](../images/180-06.jpg)

備品用的是 Pure By Gloss 這個紐約牌子，在 Amazon 上也有售，下圖四瓶約 $32 左右
![The Candler Hotel Atlanta, Curio Collection 盥洗用品](../images/180-07.jpg)

## 餐廳（Restaurant & Bar）
The Candler Hotel 內的餐廳，名為 By George，餐點以法式為主，裡面位置很寬敞，可供小酌的酒吧也是必備的

若是有早餐額度的話也是在這邊用

![By George Restaurant at The Candler Hotel 吧台](../images/180-10.jpg)
![By George Restaurant at The Candler Hotel 大廳](../images/180-11.jpg)
![By George Restaurant at The Candler Hotel 大廳](../images/180-12.jpg)
晚間餐點以法式為主，價位約 $30 - $40，下圖依序為：烤羊排、烤豬排以及附贈的麵包 <br />
羊排可自選熟度，我們是選五分熟、豬排有點小硬，但吃起來不乾，只是不好切，麵包的口感則很接近月餅 <br />
![By George Restaurant at The Candler Hotel 晚餐](../images/180-13.jpg)

若有希爾頓（黃金/鑽石）會員的話，早餐額度的 $30（二人份）則是可用在這邊，早餐價位約 $15 - $19
下圖為我們這二天的早餐，依序為：炸雞鬆餅＋貝果 ＆ 貝果＋ 可頌三明治
![By George Restaurant at The Candler Hotel 早餐](../images/180-14.jpg)

附上 By George 的菜單
![The Candler Hotel Atlanta, Curio Collection](../images/180-15.jpg)

## 後記
到了可口可樂發源地的亞特蘭大，不免俗的要入住由可口可樂公司創辦人所出資建造＆掛名的飯店 <br />
建築內的設計＆擺設都很典雅，雖已建造了 117 年，卻沒有給人陳舊的感覺 <br />
飯店內的健身房，各類設施都很齊全，連划船機也有，美中不足的是沒有游泳池

![The Candler Hotel Atlanta, Curio Collection 健身房](../images/180-08.jpg)
![The Candler Hotel Atlanta, Curio Collection 健身房](../images/180-09.jpg)

若使用美國運通 THC 入住該飯店，即可得到 $100 的退額，這 $100 的退額可用在餐廳 ＆ Check-In 櫃檯旁的零食櫃 <br />
但零食櫃旁的飲料櫃則不能使用這 $100 的退額，因為飲料櫃裡的這些玻璃瓶裝可樂（一瓶 $5），賣出的收益都會直接進到 The Candler 慈善基金會，算是在使用餐廳退額上要注意的地方

::: warning
點擊觀看更多： <br />
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
:::
