---
title: 騰訊（TCEHY）2021 Q3 財報，是否已脫離監管影響
description: '騰訊 2021 Q3 第三季財報|TCEHY 2021 Q3 第三季財報|騰訊 監管 股價'
date: 2021-11-13
tag: [Stock, TCEHY]
category: Investment
---

騰訊（TCEHY），於 11/10 公佈了 Q3 財報，在這段期間：08/08（公佈 Q2 財報） - 11/10，股價上漲了 3.71%（$57.71 → $59.85），同期 S&P 500 上漲了 5.61%

從二月中的最高點 $99.1 持續下跌以來，騰訊似乎已經歷了谷底，開始緩慢向上，下面來看看 Q3 財報是否證明了騰訊已擺脫監管困境


## 騰訊（TCEHY）2021 Q3 財報
[資料來源](https://www.tencent.com/zh-cn/investors/financial-news.html)

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">QoQ（Q2）</th>
    <th align="center">QoQ（Q3）</th>
  </tr>
  <tr>
    <td align="center">營收（RMB in million）</td>
    <td align="center">135,303</td>
    <td align="center">138,259</td>
    <td align="center">142,368</td>
    <td align="center">2.18%</td>
    <td align="center">2.97%</td>
  </tr>
  <tr>
    <td align="center">營業利潤（RMB in million）</td>
    <td align="center">56,273</td>
    <td align="center">52,487</td>
    <td align="center">53,137</td>
    <td align="center">-6.73%</td>
    <td align="center">1.24%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">45.38%</td>
    <td align="center">46.29%</td>
    <td align="center">44.07%</td>
    <td align="center">-0.91%</td>
    <td align="center">-1.31%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">41.59%</td>
    <td align="center">37.96%</td>
    <td align="center">37.32%</td>
    <td align="center">-3.63%</td>
    <td align="center">-0.64%</td>
  </tr>
  <tr>
    <td align="center">EPS（diluted）</td>
    <td align="center">4.92</td>
    <td align="center">4.39</td>
    <td align="center">4.07</td>
    <td align="center">-10.78%</td>
    <td align="center">-7.13%</td>
  </tr>
</table>

- 營收：本季營收約 1,424 億（季成長 2.18%），季成長 2.97%（Q2 季成長為 2.18%），僅線上廣告小幅下滑 1.48%，其餘營收來源皆小幅上升
- 營業利潤：約 531 億，季成長 1.24%（Q2 季成長為 -6.73%），在營收成長約 3% 的同時，營業利益僅上升 1.24%，可看出毛利＆營業利益率在 Q3 皆為下滑
- 毛利率＆營業利益率：分別為 44.07% ＆ 37.32%，與 Q2 相比，下降了 1.31% & 0.64%（Q2 時下降了 0.91% & 3.63%）
- EPS：這季因合資公司虧損持續擴大，本季 EPS 為 4.07，季成長為 -7.13%（Q2 為 -10.78%）

## 使用者月活數（Monthly active users）
單位百萬（In millions）:
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q3</th>
    <th align="center">Q2 QoQ（％）</th>
    <th align="center">Q3 QoQ（％）</th>
  </tr>
  <tr>
    <td align="center">微信 + WeChat 月活數（MAU）</td>
    <th align="center">1,262.6</th>
    <td align="center">0.78%</td>
    <td align="center">0.89%</td>
  </tr>
  <tr>
    <td align="center">QQ 智慧裝置月活數（Smart device MAU of QQ）</td>
    <th align="center">573.7</th>
    <td align="center">-2.56%</td>
    <td align="center">-2.91%</td>
  </tr>
  <tr>
    <td align="center">增值服務付費會員數（Fee-based VAS registered subscriptions）</td>
    <th align="center">235.4</th>
    <td align="center">1.64%</td>
    <td align="center">2.62%</td>
  </tr>
</table>

雖然在強力的監管影響下，Q3 的增值服務會員仍成長了 2.62%，也因此，增值服務的營收成長速度為所有分類中最高（4.43%，第二名為金融服務的 3.4%）

財報中揭露了，騰訊已全面落實了針對「未成年人士的遊戲防沈迷規定」，在本季度（2021/09）：
- 未成年用戶在本土市場的遊戲時間為 0.7%，去年同期為 6.4%
- 未成年用戶在本土市場的營收貢獻降為 1.1%，去年同期為 4.8%

## 後記
騰訊從二月的到八月的谷底，最大跌幅為 45.9%，而現在距離谷底已上漲了 16.7% <br />
從這次財報來看，營收＆營業利潤皆有成長，毛利＆營業利潤率的下降也沒有繼續擴大，剩下的 EPS 衰退是因合資公司的虧損較 Q2 又擴大了約 47% 所導致 <br />

這些跡象可以看出，騰訊已經脫離了最慘的時期，並漸漸朝著恢復先前的成長速度，這點從股價 & 本益比也看得出來投資人已對騰訊恢復了先前的投資信心

騰訊目前的本益比為 20.64，對比我在八月購買的 18.91，也增加了 9.15%，雖然本季的股價表現略輸大盤（S&P 500），但我仍騰訊抱持信心，認為騰訊仍處在可以投資的價位