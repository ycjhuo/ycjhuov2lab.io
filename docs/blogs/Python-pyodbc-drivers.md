---
title: 如何用 python 來連接使用資料庫 (MS SQL)
description: 'Python 連接資料庫 | Python 資料庫教學 | Python MSSQL | Pyhton PYODBC 介紹 | Python PYODBC 教學 | Pyhton 讀取資料庫 MSSQL | Python 微軟資料庫'
date: 2022-01-28
tag: [Python, MSSQL]
category: Programming
---

若要用 Pyhton 存取資料庫的話，幾乎都會用到 ```pyodbc``` 這個套件來操作 & 訪問資料庫 <br />

在使用套件前須先安裝 ```pyodbc```，語法如下：<br />

```pip install pyodbc```

安裝完後，即可用下面這行幫助我們與資料庫建立連線：

```python
import pyodbc
conn = pyodbc.connect('DRIVER={SQL Server}; SERVER=%s; DATABASE=%s; UID=%s; PWD=%s; Trusted_Connection=no;' %(server, db, uid, pwd))
```

其中，DRIVER 有多種，如：SQL Server，SQL Server Native Client 11.0，ODBC Driver 17 for SQL Server 等等

而要怎麼確認我們的 ODBC Driver 呢？

## 確認 ODBC Driver
1. 在 Command Prompt 中輸入 ```odbcad32```，即可叫出 ODBC Data Source Administrator 視窗
2. 再切換到 Drivers 的分頁後，即可得知這台電腦上的 ODBC 版本

![ODBC Data Source Administrator](../images/ODBC_Data_Source_Administrator.png)


而可用的 Drivers 如下：[來源](https://github.com/mkleehammer/pyodbc/wiki/Connecting-to-SQL-Server-from-Windows)

- {SQL Server} - released with SQL Server 2000
- {SQL Native Client} - released with SQL Server 2005 (also known as version 9.0)
- {SQL Server Native Client 10.0} - released with SQL Server 2008
- {SQL Server Native Client 11.0} - released with SQL Server 2012
- {ODBC Driver 11 for SQL Server} - supports SQL Server 2005 through 2014
- {ODBC Driver 13 for SQL Server} - supports SQL Server 2005 through 2016
- {ODBC Driver 13.1 for SQL Server} - supports SQL Server 2008 through 2016
- {ODBC Driver 17 for SQL Server} - supports SQL Server 2008 through 2019

若 Driver 不支援的話，在程式執行時，會跳出下面的錯誤訊息： <br /> 
```pyodbc.OperationalError: ('08001', '[08001] [Microsoft][ODBC SQL Server Driver][DBNt][ODBC SQL Server Driver][DBNETLIB]ConnectionOpen (SECCreateCredentials()). (1)')```

若使用的 MS Server 版本較新的話，可將 Driver 直接設為： ```ODBC Driver 17 for SQL Server```

## pyodbc.connect 參數
在確認了 Driver 後，剩下的參數 Server, Database, UID, PWD，則分別為：

- Server：要連接的資料庫 IP
- Database：資料庫中的 DB 名稱
- UID：登入資料庫的帳號
- PWD：登入資料庫的密碼

## pyodbc 操作資料庫

連接到資料庫後，只要在設定好操作資料庫的語法即可，若要查詢 table ，以及用 pandas 的 dataframe 印出的話，如下： <br />

```python
script = """
SELECT * FROM [dbo].[XXXX] 
WHERE XXXX
"""

df = pd.read_sql(script, conn)
```

若要將資料庫的資料匯入可參考這篇： <br />
[如何用 Python 將 CSV 檔案匯入資料庫（MSSQL）](https://ycjhuo.gitlab.io/blogs/Python-Import-CSV-To-MSSQL.html) <br />

匯出的話，為： <br />
[利用 Python 直接匯出資料庫 (MSSQL) 的資料 (Pandas ReadSQL)](https://ycjhuo.gitlab.io/blogs/Python-Pandas-Download-Data-From-MSSQL.html) <br />