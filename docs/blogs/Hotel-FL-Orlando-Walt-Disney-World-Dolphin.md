---
title: 開箱！奧蘭多-華特迪士尼海豚飯店（Walt Disney World Dolphin）
description: '佛羅里達 奧蘭多 華特迪士尼 飯店 介紹 推薦|Florida Orlando Walt Disney World 飯店|奧蘭多 Orlando 萬豪 迪士尼 飯店 推薦 CP 值|Walt Disney World Dolphin 華特迪士尼 海豚 飯店 介紹 推薦 開箱'
date: 2023-06-29
tag: [Hotel, Disney, Orlando, Florida, Marriott]
category: Travel
---
::: tip
入住時間：2022/12/19 - 12/21 <br />
房型：2 Double Beds <br />
每晚價格（稅後）：$303（含每天 $45 的 resort fee） / 62,000 點 <br />
Google 評價：4.3 顆星（13,193 評價）
:::

這間位於奧蘭多（Orlando）著名景點：華特迪士尼世界（Walt Disney World）的海豚飯店，雖然名字有迪士尼，卻不是真正的迪士尼飯店，而是由萬豪（Marriot）跟迪士尼合作的聯名飯店

雖不是由迪士尼直接運營的飯店，但一樣享有提早入園、接駁巴士等跟迪士尼飯店一樣的福利 <br />
唯一差別只是房間內的裝潢沒有迪士尼的元素

## 飯店外觀＆大廳（Hotel Building & Lobby）
一到飯店，映入眼簾的是金字塔造型，搭配金魚噴泉的外觀，飯店中很多金魚的元素，若是改叫金魚飯店感覺會比海豚飯店（Dolphin）來的貼切
![華特迪士尼海豚飯店（Walt Disney World Dolphin）外觀](../images/222-01.jpg)
飯店大廳
![華特迪士尼海豚飯店（Walt Disney World Dolphin）外觀](../images/222-02.jpg)
飯店櫃檯有分給萬豪金卡會員以上的 Elite 櫃台，但通常櫃台只會有 2-3 個服務人員，而入住的旅客又多 <br />
所以不管是排哪個櫃台，通常 Check-in 都會花費半小時以上的時間
![華特迪士尼海豚飯店（Walt Disney World Dolphin）Check-in 櫃檯](../images/222-03.jpg)

## 房間內部（2 Double Beds）
這次入住的是 2 Double Beds 房型，房內有小冰箱及二瓶水，沒有微波爐 <br />
由於是透過 Amex 的 THC（The Hotel Collection）訂房，飯店說有升級到景觀房
![華特迪士尼海豚飯店（Walt Disney World Dolphin）房間](../images/222-04.jpg)
![華特迪士尼海豚飯店（Walt Disney World Dolphin）房間](../images/222-05.jpg)

房間看出去的景
![華特迪士尼天鵝飯店（Walt Disney World Swan）房間](../images/222-13.jpg)


## 衛浴（Bathroom）
衛浴跟其他飯店比起來沒什麼差別，盥洗用具附的是 Gilchrist & Soames 這個牌子 <br />
另外，飯店不會主動提供牙刷、牙膏、拖鞋，若會用到的話，要主動跟飯店拿
![華特迪士尼海豚飯店（Walt Disney World Dolphin）衛浴](../images/222-06.jpg)
![華特迪士尼海豚飯店（Walt Disney World Dolphin）盥洗用具](../images/222-07.jpg)

## 餐廳（Restaurant & Bar）
若是萬豪的白金會員入住的話，一人可以得到 $10 的早餐額度，一間房最多 $20 <br />
在餐廳點一份餐都會超過這個額度，且因為有提早半小時入園的福利，所以比較推薦在飯店的 Grab & Go 買麵包帶在路上或是園區吃
![華特迪士尼海豚飯店（Walt Disney World Dolphin）餐廳](../images/222-08.jpg)
![華特迪士尼海豚飯店（Walt Disney World Dolphin）餐廳](../images/222-09.jpg)

## 飯店設施
下面是飯店的游泳池、天鵝船以及健身房，都是可以免費使用的 <br />
這次入住的海豚飯店，隔壁是天鵝飯店（Walt Disney World Swan），這二間都是萬豪跟迪士尼聯名的飯店，所以戶外的游泳池跟天鵝船都是共用的
![華特迪士尼海豚飯店（Walt Disney World Dolphin）飯店設施 游泳池](../images/222-10.jpg)
![華特迪士尼海豚飯店（Walt Disney World Dolphin）飯店設施 天鵝船](../images/222-11.jpg)
![華特迪士尼海豚飯店（Walt Disney World Dolphin）飯店設施 健身房](../images/222-12.jpg)

## 後記
這次是第一次入住迪士尼的飯店，雖然迪士尼附近有很多價格較低的飯店（$50 - $100 都有），但考慮到可提早半小時入園的優勢，以及到迪士尼各個園區（包含 Disney Springs）的免費接駁巴士，我覺得還是划算的

若是住非迪士尼的飯店，來回園區的 Lyft/Uber 價格差不多落在 $25-46，且若是待到煙火放完才離開園區的話，則會因為人潮太多，可能等了半小時都還叫不到車

若是加上這些福利的話，也就不會覺得迪士尼飯店貴外面很多了，更何況若是有在累積萬豪會籍的旅客，還能累積房晚呢

觀看位於海豚飯店隔壁的天鵝飯店（Walt Disney World Swan）介紹文： <br />
[開箱！奧蘭多-華特迪士尼天鵝飯店（Walt Disney World Swan）](https://ycjhuo.gitlab.io/blogs/Hotel-FL-Orlando-Walt-Disney-World-Dolphin.html)

::: warning
點擊觀看更多： <br />
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
- [迪士尼相關](https://ycjhuo.gitlab.io/tag/Disney/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
