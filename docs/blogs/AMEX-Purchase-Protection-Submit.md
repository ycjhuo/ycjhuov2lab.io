---
title: 美國運通（American Express）信用卡購買保護（Purchase Protection）提交文件
description: '美國運通 American Express AE 大白|美國運通 Platinum 白金卡福利|信用卡購買保護|需要那些保險文件|如何申請上傳文件 '
date: 2021-07-03
tag: [Credit Cards,AMEX]
category: Investment
---

接續上篇[美國運通（American Express）購買保護（Purchase Protection）申請教學](https://ycjhuo.gitlab.io/blogs/AMEX-Purchase-Protection.html) <br />

## 所需文件
在美國運通的 [Claims Center](https://claims-center.americanexpress.com) 填寫完申請後，約 1 ~ 2 天後就會收到美國運通（AMEX Assurance Company）的信，主旨是：Purchase Protection from AMEX Assurance Company, American Express

打開後，裡面有個附件，寫著我們須到上面寫的網站將購買商品的收據上傳（也有傳真及寄實體 mail 的選項）

![93-01](../images/93-01.png)

點擊附件內連結後，就可進到 Claim Document Upload Site

輸入驗證碼，案 Continue 進到下一步

## 上傳文件
![93-02](../images/93-02.png)

進到上傳頁面後，左邊會有我們的 Claim Number 以及 Document Upload Number，點擊右邊的 Upload another document 就可以上傳檔案了（一次只能上傳一個檔案）

成功上傳後，檔案會顯示在下圖的紅框處，確認檔案都已經上傳後，按下 Submit 來提交文件

![93-03](../images/93-03.png)

之後就可以看到美國運通已經都到我們所上傳的檔案，並會開始進行 Review

![93-04](../images/93-04.png)

## 申請通過
上傳美國運通要求的檔案後，約 5 個工作天（網頁上是寫 10 個工作天以內），就會收到一樣由 AMEX Assurance Company 寄來的信，寫著這次申請購買保護的案件已被審核完成

![93-05](../images/93-05.png)

打開信件內的附件後（如下圖），可以看到案件已被審核通過，退款會直接顯示在信用卡帳單上

![93-06](../images/93-06.png)

接著登入信用卡帳戶，就能看到該商品的購買金額已經作為 Credit 顯示出來囉

![93-07](../images/93-07.png)

以上就是美國運通信用卡包含的購買保護（Purchase Protection）申請教學 <br />


對手機保護有興趣的可參考這篇： <br />
[美國運通（American Express）信用卡手機保護（Cell Phone Protection）申請教學](https://ycjhuo.gitlab.io/blogs/AMEX-Purchase-Protection.html)<br />


::: warning
點擊觀看更多： <br />
- [美國運通福利文](https://ycjhuo.gitlab.io/tag/Credit%20Cards/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::