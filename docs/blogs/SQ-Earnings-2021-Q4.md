---
title: Block（SQ）公佈 2021 Q4 財報，股價大漲 26%
description: 'Block SQ 2021 Q4 第四季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報'
date: 2022-02-27
tag: [Stock, SQ]
category: Investment
---

Block（股票代號：SQ），從 2021 Q3 財報發布（11/04），到 2021 Q4 財報發布（02/24）期間，股價大跌 62%（$247.46 → $94.99），同期間 S&P 500 也下跌了 8%<br />
而 Q4 財報公佈後，Block 隔天股價大漲了 26.14%，且還是在烏俄衝突的市場環境下，讓我們從 Block 2021 第四季財報中分析大漲的原因

複習 Block 2021 Q3 財報：<br />
[Square（SQ）公佈 2021 Q3 財報，近一年首度虧損](https://ycjhuo.gitlab.io/blogs/SQ-Earnings-2021-Q3.html)

## 財務指標
[來源](https://investors.block.xyz/overview/default.aspx)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">營收（億）</td>
    <td align="center">46.81 億</td>
    <td align="center">38.45 億</td>
    <td align="center">40.79 億</td>
    <td align="center">-17.86%</td>
    <td align="center">6.08%</td>

  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">24.38%</td>
    <td align="center">29.47%</td>
    <td align="center">28.98%</td>
    <td align="center">5.1%</td>
    <td align="center">-0.49%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">2.67%</td>
    <td align="center">0.6%</td>
    <td align="center">-1.34%</td>
    <td align="center">-2.07%</td>
    <td align="center">-1.94%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.4</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">-0.17</td>
  </tr>
</table>

- 營收：Block Q4 季成長為 6.08%，相比於 Q3 的 -17.86%，成長很多，且總算擺脫連續二季的負成長（Q2 時為 -7.45%），主要是 Bitcoin 業務的營收上升 8% 帶動了整體營收的成長
- 毛利率：因 Bitcoin 本來就是 Block 的業務中，利潤最低的，因此隨著 Bitcoin 營收的上升，Block 的毛利率在本季下降 0.5%，來到了 29%，2021 平均毛利率為 25.47%
- 營業利潤率：因為銷售＆行政費用的增加，Block 在本季的營利率為 -1.34%，為 2021 中最低，也是首次低於 0%，而 2021 平均營利潤為 0.82%%
- EPS：因無法從商業模式中實現盈利，EPS 也來到 -0.17，2021 全年的 EPS 為 $0.31

雖公佈財報後，股價大漲 26%，但從上面的財務指標中，能看出 Block 財務表現仍未好轉，營業費用的增加也不是在最重要的研發上，表示 Block 正把錢投注在拓展業務＆組織改造上

接著來看目前 Block 的業務近況

## Block 業務發展
Jack Dorsey 是 Square ＆推特的 CEO，在 2021/11/30 從推特卸任後，即把心力全數投注在 Square 上，而在卸任隔日，Square 也改名為 Block，表示公司將全力發展比特幣＆區塊鏈技術 <br />

Jack Dorsey 本身是比特幣的忠實支持者，也在 Block 開啟了 TBD54566975 這個專案，旨在建立一個平台，提供開發者更容易的取得比特幣＆其它區塊鏈的應用

關於去年收購的「先買後付」平台 Afterpay，財報中提到，在 1/31 時，公司已完全收購了 Afterpay，且準備將這功能導入 Square & Cash App 中，使其能同時為商家帶來更多客戶，也讓一般使用者也能在 Cash App 中找到有提供先買後付服務的商家

::: tip
財報中表示：有提供 Afterpay 服務的商家，平均顧客消費金額上升 40%，消費頻率上升 50% <br />
同時，2022 Q1 的財報也將正式納入 Afterpay
:::

而 Cash App 則是在近期推出了「免費報稅」的服務，來擴大 Cash App 的生態圈，增進使用者對於 Cash App 的黏性 <br />
財報顯示，目前約有 1300 萬的使用者在使用 Cash Card（綁定在 Cash App 中的簽帳卡），而若這些使用者透過 Cash App 來報稅，將可直接收取政府的退稅，並透過 Cash Card 來進行消費，近一步增加了 Block 的交易額＆利潤

::: tip
在美國，每個月都要先預繳一定的稅額，等到每年四月報稅後，再依據每個人要繳的稅「多退少補」，而平均每人的退稅額是 $2,700
:::

## 後記
雖然 Block 近期投注了不少心力在推廣＆開發新業務，但股價卻完全不給面子，短短三個月的時間就跌了 62%，而要回到先前的價位，則須上漲 161% 的漲幅；就算加上昨天已上漲的 26%，也還須上漲 107%

這表示若在下跌時，沒有投入更多資金來攤平成本，要等到股價從谷底回到先前的價位（$248），則須要二倍以上的漲幅；但這次財報後的大漲，也不代表著 Block 已徹底脫離谷底 <br />

畢竟以財務指標來說，這些數據仍在惡化，並沒有推動股價上漲的亮點；在財報中更多的是，描繪未來的發展＆可能性 <br />
而市場之所以由跌轉漲，更多的可能性是因為：<br />
- Block 已完成對 Afterpay 收購，且下季財報也會將 Afterpay 也一起納入 <br />
- Block 將會把 Afterpay 服務與 Square & Cash App 進行整合，讓交易額＆業務增長更上一層樓 <br />

前篇說到，在財報未見好轉的情況下，除非股價有大幅度下跌，不然不會考慮繼續買入 SQ；但因近期 TSLA 也處在低檔，因此我將大部分的錢繼續投入 TSLA，而沒有多餘資金再投入 SQ，也使得我目前的 SQ 總報酬率為 -36%，若要想報酬率回正，在幾個月內幾乎是無法達成的

另外，Block 在 2021 共購買了價值 1.7 億的比特幣，但因加密貨幣也處在低檔，短期沒有回升的跡象，也是 SQ 未來的風險之一

::: warning
點擊觀看更多： <br />
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::