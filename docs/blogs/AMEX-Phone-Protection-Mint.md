---
title: 美國運通（American Express）信用卡手機保護（Cell Phone Protection）申請教學，以 Mint 為例
description: '美國運通 American Express AE 大白卡福利|Platinum 白金卡 信用卡購買保護|美國運通 大白 Cell Phone Protection|如何申請手機保險教學|大白手機保險預付卡手機方案 Mint '
date: 2021-10-14
tag: [Credit Cards, AMEX]
category: Investment
---

上篇 [美國運通（American Express）信用卡手機保護（Cell Phone Protection）申請教學](https://ycjhuo.gitlab.io/blogs/AMEX-Phone-Protection.html) 介紹了大白附送的手機保護，以及如何申請

這篇來詳細說明須準備哪些文件給 AIG 審核

## 需要的文件

除了打電話到 AIG 成立 Case 後，會收到 AIG 給的三張 Forms 之外，AIG 還會請我們提供以下的文件讓他們審核：

1. 信用卡帳單：AMEX BILLING STATEMENT W/ PROOF OF LAST MONTH'S/PERIOD'S CELLULAR CHARGES（JULY）<br />
手機損壞日的上個月信用卡帳單，因為我的手機是八月壞掉的，所以 AIG 有註明是要七月的帳單，證明上個月的確是用 AMEX 信用卡來繳交電話費

![AMEX 手機保護 信用卡帳單](../images/124-01.jpg)

2. 通話費帳單：CELLULAR PROVIDER'S BILLING STATEMENTS FROM PRECEDING & CURRENT MONTH/PERIOD OF THE THEFT/DAMAGE <br />
這是要提供手機電信商的 Statement 上的付款紀錄，用來跟信用卡帳單核對金額是否一致

![AMEX 手機保護 電信帳單](../images/124-02.jpg)

3. 壞掉手機的購買證明：PROOF OF ELIGIBLE CELLULAR WIRELESS TELEPHONE <br />
信用卡手機保護上並沒有提到，手機須要用 AMEX 大白來購買，我猜 AIG 可能只是想知道當初這支手機買多少錢

![AMEX 手機保護 損壞手機當初的購買證明](../images/124-03.jpg)

4. RECEIPT OF PURCHASED CELLULAR TELEPHONE：新買的手機購買證明 <br />
AIG 須要確認你是否真的購買了手機，才能將該手機的金額寄支票給你（但會扣除自付額 $50）

![AMEX 手機保護 新手機購買證明](../images/124-04.jpg)

5. REPAIR ESTIMATE：手機維修店開立的估價單 <br />

這一點比較麻煩，須先拿到手機維修店來看看維修的估價是多少，並附上店家的估價單；不確定若沒有先估價而直接購買新手機會怎麼樣

![AMEX 手機保護 維修估價單](../images/124-05.jpg)

## 後記
這次我在 9/2 時致電 AIG 成立 Case，並在隔天（9/3）就準備好所有資料寄給 AIG，但在 9/15 收到 AIG 回覆說我缺少了信用卡帳單（AMEX BILLING STATEMENT W/ PROOF OF LAST MONTH'S/PERIOD'S CELLULAR CHARGES（JULY）），也就是上面的第一項 <br />

但其實我已經交了，所以我就再附上之前寄過的附件給 AIG，最後到了 9/27 時，AIG 就寄 email 通知我說審核已通過，會在近期內收到支票，而我也在 10/11 收到支票

![AMEX 手機保護 報銷支票](../images/124-06.jpg)

相比上次從成立 case 到拿到支票，花了 67 天，這次僅花了 40 天就完成，AIG 的效率提升不少（但也可能是因為有了上次的經驗，比較知道他們需要哪些資料來審核）

若想知道什麼是 AMEX 手機保護的話，可參考：<br />
衍伸閱讀：<br />
[美國運通（American Express）信用卡手機保護（Cell Phone Protection）申請教學](https://ycjhuo.gitlab.io/blogs/AMEX-Phone-Protection.html)<br />


::: warning
點擊觀看更多： <br />
- [美國運通福利文](https://ycjhuo.gitlab.io/tag/Credit%20Cards/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::