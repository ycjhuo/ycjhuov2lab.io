---
title: Global Entry 申請教學、面試流程全攻略
description: 'Global Entry 申請 流程 步驟|Global Entry 教學|Global Entry 申辦流程|TSA PreCheck|美國機場快速通關'
date: 2022-06-17
tag: [Global Entry, TSA PreCheck]
category: Travel
---

## Global Entry 介紹
Global Entry (全球入境) 是美國的「機場快速通關」計畫，有了這個後，在出入境美國時均不需經過原本的檢查行李 & 安檢程序，可大幅縮短我們在機場等待的時間，下面是官網的介紹以及 [Global Entry 申請連結](https://ttp.cbp.dhs.gov/)

![Global Entry 申辦](../images/179-01.png)

上圖可以得知，申辦的費用是 $100，申請失敗也不會退還，申請成功後，有五年的使用期限，在第四年開始時即可辦理續約

## Global Entry 事前準備
另外，在申辦 Global Entry 時，會需要用到台灣的「警察刑事紀錄證明書 (良民證)」 <br />
若沒有事先申辦的話，可在這邊線上申請 [警察刑事紀錄證明書 (良民證) 線上申辦頁面](https://eli.npa.gov.tw/E7WebO/index01.jsp)，再請家人去現場拿後，拍照給你即可

![Global Entry 良民證 申請結果](../images/179-02.png)

## Global Entry 申請流程
Global Entry 的申請步驟分為三部分：註冊帳號 → 填寫資訊 → 面試 <br />
在註冊帳號時，系統會要求我們使用二階段驗證（MFA / OTP）作為往後登入系統時使用 <br />
因整個申辦時程通常都是六個月以上，所以記得換手機時，原本的手機也須留著，避免換手機後無法登入自己的帳號

![Global Entry 建立帳號](../images/179-03.png)
![Global Entry 申請流程](../images/179-04.png)

在完成共 9 頁的資料填寫後，再來就是漫長的等待預約面試了

## Global Entry 預約面試
不知道是否為疫情的關係，我在 2021/10 月註冊填寫完 Global Entry 的資料後，到了 2022/03 才收到通知預約面試的信，整整過了快半年 <br />
收到信後，就可以回到網站開始選擇要去面試的地點了，但通常在大城市，面試的點幾乎都滿了


如下圖所示，當時在紐約市最快能預約到的時間為 2022/05/12，也就是在填寫完 Global Entry 的資料後，等了半年才可預約面試，接著到面試時間又要再等 2 個月，總共花了 8 個月的時間 <br />

在選擇預約時間/地點時，我們可以先選擇一個最近的日期，接著再每天到網站看，是否有更接近的日期釋出(通常都是有人放棄，或是他也換到了更早的時間) <br />

![Global Entry 選擇面試時間地點](../images/179-06.png)

在確認面試時間 & 地點時，是可以重複選擇的，因此不用怕選了後，卻因突然有事而無法更改時間 <br />
但一次只能選擇一個時間，因此，選了新的後，舊的就會被取消 <br />

![Global Entry 預約面試時間地點](../images/179-07.png)

在成功預約面試時間 & 地點後，我們申請的進度就從等候書面審核，變成參加面試的狀態了 (如下圖)

![Global Entry 等候面試通知](../images/179-05.png)

## Global Entry 面試通過
預約面試時間到了後，我們就可以前去參加面試了 <br />
面試時記得攜帶護照，在面試過程中，承辦人員僅詢問了先前有沒有犯罪紀錄之類的問題，再看了看我的護照就直接審核通過了 <br />
通過面試後，我們再登入 Global Entry 網站，即可看到系統狀態已更新為審核通過，並顯示我們的 Global Entry 號碼，而當初註冊 Global Entry 帳號的信箱也會收到審核通過的通知 <br />

![Global Entry 審核通過](../images/179-08.png)
![Global Entry 確認信](../images/179-09.png)

## 後記
而這個 9 位數的數字我們使用 Global Entry / TSA-Precheck 會用到的號碼，再來就可到各大航空網站把這個數字填到 Known Traveler Number 這個欄位 <br />
之後在買機票後，Check-In 時印出的登機證就會有 Global Entry / TSA-Precheck 小標記了 <br />
而這標記也代表著我們在安檢/通關時，不須再走原本的通道，而可以改走快速通關路線了 <br />

而申辦完 Global Entry 後要做哪些事呢？可參考[Global Entry 如何使用？辦好 Global Entry 後，要做哪些事](https://ycjhuo.gitlab.io/blogs/Global-Entry-Benefits.html)

::: warning
點擊觀看更多： <br />
- [旅遊相關文章](https://ycjhuo.gitlab.io/categories/Travel/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::