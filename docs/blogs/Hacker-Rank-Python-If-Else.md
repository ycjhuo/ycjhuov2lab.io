---
title: HackerRank Python If Else 解法
description: 'HackerRank 教學|HackerRank 解答|HackerRank Python|HackerRank Python If else|Python If else 教學用法|Python Range 用法'
date: 2021-03-13 10:24:17
tag: [Python, HackerRank, Easy]
category: HackerRank
---
## Problem
### Task
Given an integer, ```n```, perform the following conditional actions: <br />
If  ```n``` is odd, print Weird <br />
If  ```n``` is even and in the inclusive range of ```2``` to ```5```, print Not Weird <br />
If  ```n``` is even and in the inclusive range of ```6``` to ```20```, print Weird <br />
If  ```n``` is even and greater than ```20```, print Not Weird <br />

### Input Format
A single line containing a positive integer, ```n```.

### Constraints
1 <= ```n``` <= 100

### Output Format
Print Weird if the number is weird. Otherwise, print Not Weird.

## Answer
這題是測驗 Python 的 If-Else 熟悉度， 題目給了我們 n <br />
如果 n 是奇數的話，印出 Weird <br />
如果 n 是偶數的話，且數字介於 2 - 5 之間的話，印出 Not Weird <br />
如果 n 是偶數的話，且數字介於 6 - 20 之間的話，印出 Weird <br />
如果 n 是偶數的話，且數字大於 20 的話，印出 Not Weird <br />

這題重點在於 ```range``` 用法，```range```後面的數字並不會被涵蓋到範圍裡面，因此後面那個數要 +1 才能符合條件。
eg: 題目要求 數字介於 2 - 5 之間，就要寫 ```range(2, 6)```

### Source Code
```python
#!/bin/python3

import math
import os
import random
import re
import sys



if __name__ == '__main__':
    n = int(input().strip())
    if (n % 2 == 1):
        print("Weird")
    else:
        if (n in range(2, 6)):
            print("Not Weird")
        if (n in range(6, 21)):
            print("Weird")
        if(n > 21):
            print("Not Weird")
```