---
title: 如何讓 eBesucher 運行時不中斷， eBesucher Restarter 使用教學
description: 'eBesucher restarter 介紹｜eBesucher restarter 心得｜eBesucher restarter 推薦｜eBesucher restarter 教學'
date: 2022-07-15
tag: [Passive Income, eBesucher]
category: Investment
---

[上篇](https://ycjhuo.gitlab.io/blogs/eBesucher.html)提到如何透過 eBesucher 用瀏覽器就能自動賺取被動收入，但若是在掛網的過程中，瀏覽器當掉或是不動了該怎麼辦呢？這篇來介紹 eBesucher 自己出的 eBesucher restarter 這個應用程式 <br />

eBesucher restarter 可以幫我們監視瀏覽器有沒有正常運行，若是瀏覽器當掉的話，則會幫我們自動重開瀏覽器

下面就來看該如何安裝＆使用 eBesucher restarter

## eBesucher restarter 下載
1. 登入 eBesucher 後，點擊上方的 MEMBERS AREA，再點擊下方的 Surfbar & Clicks，選擇 restarter
2. 下方 Installation 選擇要下載的 eBesucher restarter 版本（Windows or Mac/Linux），如下圖紅框部分，下載完後就進到安裝環節

![eBesucher restarter 下載](../images/185-01.png)

## eBesucher restarter 安裝
不管是下載哪個版本，都須先安裝 Java 環境（Java Runtime Environment, JRE），可至[官網連結](https://www.java.com/en/download/manual.jsp)依照自己的作業系統選擇，安裝完後，Windows 版本要自行設定[環境變數](https://www.runoob.com/w3cnote/windows10-java-setup.html)，Mac/Linux 則不用

::: tip
若不知道自己有沒有安裝過 Java 環境，可打開命令提示字元（Terminal），輸入 ```java -version```，若有顯示版本，則表示電腦已有安裝 JRE
:::

1. Windows 版本安裝較簡單，可直接跳到步驟 4，Mac/Linux 則須依照下面步驟
2. Mac/Linux 下載完解壓縮後，進到資料夾（restarter-setup-others.v1.2.03），對著 restarter.jar 按右鍵，選擇打開，會出現 ```Fatal error on loading language file``` 這個錯誤訊息 <br />
且會列出路徑 ```/private/var/folders/z3/***/***/***/lang/en.lang```，中間的 *** 表示每個人都不太一樣
3. 回到資料夾 restarter-setup-others.v1.2.03 ，將 lang 這個資料夾複製，貼到錯誤訊息顯示的路徑，之後再對著 restarter.jar 按右鍵，就可以看到原本打不開的 restarter.jar，已經可以打開了

![eBesucher restarter Mac Linux 安裝](../images/185-02.png)

4. 從這個步驟開始 Windows 跟 Mac/Linux 都是一樣的，打開 eBesucher restarter 後，一直按下一步，來到下圖這邊，User Name 填上當初註冊的 eBesucher ID，Restarter access code 則可在這篇文章第一張圖的灰框那邊找到

![eBesucher restarter access code 在哪](../images/185-03.png)

5. 最後，則要選擇使用的瀏覽器，eBesucher restarter 會依據我們選擇的瀏覽器來做監控，而那個瀏覽器也要安裝 eBesucher addon 套件才可，後續都可以在 eBesucher restarter 的 Setting 那邊做更改，不需擔心現在選了就不能改

![eBesucher restarter 選擇瀏覽器](../images/185-04.png)

6. 安裝完後，即可看到下圖的畫面，按下 Start Surfbar，eBesucher restarter 即會自動開啟瀏覽器並執行 Surfbar 開始掛網，右下角則會顯示 eBesucher restarter 過幾秒就會確認 Surfbar 是否有正常運作，沒有的話，則會自動重啟瀏覽器並執行 Surfbar

![eBesucher restarter 如何使用](../images/185-05.png)

## 後記
以上是 eBesucher restarter 的安裝＆使用說明，有興趣加入 eBesucher 在用電腦或是不用電腦時都能賺取被動收入的，再請用 [我的 eBesucher 推薦連結](https://www.eBesucher.com/?ref=YCJHUO) 來註冊

若想知道 eBesucher 如何創造被動收入，可參考這篇介紹文 [eBesucher，開著瀏覽器就能自動賺取被動收入](https://ycjhuo.gitlab.io/blogs/eBesucher.html)

::: warning
點擊觀看更多： <br />
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::