---
title: IBM MQ 常用指令 dmpmqmsg 介紹，如何從佇列中取出訊息，並放到另一個佇列上
description: 'IBM MQ 佇列 轉移訊息|IBM MQ Explorer Queue 轉移交易|IBM MQ Explorer Queue Message Transfer|如何取出 MQ 中的訊息|將 MQ 訊息匯入其它佇列|如何將 MQ 佇列中的訊息轉移到其它佇列'
date: 2022-12-14
tag: [IBM MQ]
category: System
---

本文介紹了在使用 IBM MQ 時的常用指令 dmpmqmsg <br />
透過這個指令可幫我們將佇列 (Queue) 中的訊息取出/放入 其它佇列 (Queue)，或是另存為文字檔 <br />
方便我們測試不同佇列 (Queue) 的連線是否有通 <br />
 
以下指令皆直接在 MQ Server 中，打開 Command Prompt 後，直接輸入即可

## 取出佇列 (Queue)中的訊息
若我們要將 佇列 (Queue) 裡面的訊息取出，並存放在文字檔裡面，語法如下：
```sh
dmpmqmsg -m 佇列管理員名稱 -i 佇列 (Queue) 名稱 -f 檔案名稱.txt
```

下面的例子，將 Queue Manager Name 用 QMN 代替，Queue 的名稱為 Q1，將裡面的訊息備份到檔案 (file.txt)，語法如下：

```sh
dmpmqmsg -m QMN -i Q1 -f file.txt
```
::: tip
檔案會存到 Command Prompt 目前所指向的路徑
:::

這樣即可將 Q1 這個 Queue 裡面的訊息/交易都放到 file.txt 這個文字檔裡面

## 將檔案中的訊息放入佇列
而若要將檔案裡面的訊息放到佇列 (Queue) 裡面，語法為：
```sh
dmpmqmsg -m 佇列管理員名稱 -o 要放入訊息的佇列 (Queue) 名稱 -f 檔名
```

## 將訊息從 A 佇列 (Queue) 匯入 B 佇列 (Queue)
而若我們要將 Q1 這個佇列 (Queue) 裡面的訊息/交易取出，並放到 Q2 這個佇列 (Queue) 中，則語法如下：

```sh
dmpmqmsg -m 佇列管理員名稱 -I Q1 佇列 (Queue) 名稱 -o Q2 佇列 (Queue) 名稱 -r #1
```

例：
```sh
dmpmqmsg -m QMN -I Q1 -o Q2 -r #1
```

::: tip
這邊 #1 指的是取出並匯入 1 筆 訊息/交易，若有 2 筆的話，則是 #2
:::

參考資料：
[IBM MQ 9.1 官方文件](https://www.ibm.com/docs/en/ibm-mq/9.1?topic=systems-examples-using-dmpmqmsg-utility)