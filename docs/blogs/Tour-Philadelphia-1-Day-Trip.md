---
title: 疫情間的費城一日遊（紐約當天來回）獨立廳 自由鍾 Elfreth's Alley 費城市政廳
description: '費城 景點|費城 介紹|費城 推薦|紐約 費城|費城 自由鐘|費城 獨立廳|費城 獨立公園|費城 冰淇淋'
date: 2021-08-31
tag: [Philadelphia]
category: Travel
---

如果要說最能代表美國歷史的城市，非費城莫屬了。這裡誕生了美國憲法以及獨立宣言，而常春藤聯盟之一的賓夕法尼亞大學（University of Pennsylvania）也在這裡 <br />

從紐約市搭客運過去只要 2 小時左右即可抵達，非常適合假日想要來個 1 - 2 天小旅行的紐約人 <br />

這次我是用 [Wanderu](https://www.wanderu.com/en-us/rf/99s/) 來訂票的，它是一個可以幫我們搜尋客運＆火車的網站，只要輸入地點及時間，即可找出這個範圍中有班次的大眾運輸

## 交通資訊
由於這次行程是當天來回，所以我選擇了 8:30 從紐約出發的灰狗巴士（Greyhound），約 11:00 抵達費城；回程則是搭乘 Peter Pan（18:00 從費城離開，19:50 回到紐約）

![Wanderu NYC To Philly](../images/109-01.png)

## 景點規劃
車站的地點離 Philly Downtown  & Old City 非常近，只要走 10 分鐘就能到今天介紹的景點，不須搭地鐵或是公車

![費城一日遊景點規劃](../images/109-02.png)

### 國家獨立歷史公園（Independence National Historical Park）
從車站下車後，走路 10 分鐘，就可以到國家獨立歷史公園，這個公園內有非常多的古蹟建築，代表著獨立戰爭的自由鐘 (Liberty Bell)，以及簽署美國憲法及獨立宣言的獨立廳（Independence Hall）都在這邊 <br />

若要進入獨立廳的話得先在對面的遊客中心領取號碼牌；而要進入自由鐘前也須在門口先進行類似機場安檢的檢查 <br />

![Independence Visitor Center 費城獨立遊客中心](../images/109-09.jpg)<center>費城獨立遊客中心（Independence Visitor Center）</center>

![獨立廳建築](../images/109-03.jpg)<center>費城獨立廳建築（Independence Hall）</center>

![獨立廳內部](../images/109-04.jpg)<center>獨立廳內部（Independence Hall）</center>

![自由鐘](../images/109-05.jpg)<center>自由鐘(Liberty Bell）</center>

這一區也被稱為 Old City，有許多著名的建築都在這邊，例如：
- 美國第一銀行（First Bank of the United States）及第二銀行（Second Bank of the United States）
- 費城木匠廳（Carpenters' Hall）：可入內參觀，是早期的一個會議廳
- 富蘭克林的家（Fragments of Franklin Court）：Benjamin Franklin （1706 - 1790）於 1763 - 1790 的家，旁邊還有個關於他的博物館，須買票入場（$10）

### 冰淇淋店 The Franklin - Fountain
逛完富含歷史特色的歷史公園區後，可以往右邊走一下，來到費城有名的手工冰淇淋店 The Franklin Fountain，店內位置很少，建議外帶到下個景點（Elfreth's Alley）吃 <br />

![The Franklin - Fountain](../images/109-06.jpg)<center>費城冰淇淋店（The Franklin - Fountain）</center>
![Menu of The Franklin - Fountain](../images/109-10.jpg)<center>費城冰淇淋店（The Franklin - Fountain）Menu</center>

### Elfreth's Alley
買完冰淇淋後，往北走 5 分鐘就到了拍照聖地 Elfreth's Alley <br />
這裡是美國最老的（有人居住）住宅街，距離現在已約 300 年，每年 12 月的第一個星期六，這裡的居民會開放他們的住家供遊客參觀 <br />

![Elfreth's Alley](../images/109-07.jpg)<center>Elfreth's Alley</center>

在 Elfreth's Alley 約 3 分鐘路程，就會到製作美國第一個國旗的 Betsy Ross House <br />

![Betsy Ross House](../images/109-11.jpg)<center>Betsy Ross House</center>

### 費城市政廳 Philadelphia City Hall
最後將費城市政廳（Philadelphia City Hall）作為一日遊的最後景點，因為這邊離客運站很近，只要 5 分鐘即可到客運站 <br />
這裡曾是世界上最高的大樓（1901 - 1908），建築風格非常具有歐式風

![Philadelphia City Hall](../images/109-08.jpg)<center>費城市政廳（Philadelphia City Hall）</center>

位於市政廳旁邊的 Reading Terminal Market 是費城著名的農夫市集，類似於紐約的 Chlsea Market <br />
若還有時間的話（只開到 18:00），也可到這邊逛逛，品嚐一下費城特產（Philly Cheese steak）<br />

![Reading Terminal Market](../images/109-12.jpg)<center>Reading Terminal Market</center>

P.S. 另外二家 Philly Cheese steak 名店為：Jim's South St. 與 Geno's Steaks，但跟上面介紹的景點距離較遠，且我覺得跟 Reading Terminal Market  裡面賣的吃起來差不多
