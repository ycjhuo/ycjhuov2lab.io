---
title: 用 Node.JS + Express 框架開發一個留言板
description: 'Node.JS Express 教學 | Node.JS Express 留言板 | Express 教學'
date: 2021-06-03
tag: [Node.JS, express]
category: Programming
---

上一篇[如何用 Node.JS 開發一個留言板](https://ycjhuo.gitlab.io/blogs/NodeJS-Build-Bulletins.html)介紹了如何用 Node.JS 來開發留言板 <br />

這篇就以上一篇為基礎，加上用 express 這個框架來建立留言板，透過這二篇就可以了解 express 為我們省略了多少的底層邏輯 <br />

這篇的目錄結構及其他檔案都與上篇一樣，僅就 ```app.js``` 及 ```post.html``` 做了修改


## 所需套件

```shell
npm install art-template --save
npm install body-parser --save
npm install express --save
npm install express-art-template --save
```

## 業務邏輯
### app.js

```javascript
var express = require('express')
var bodyParser = require('body-parser')

var app = express()


// body-parser 用來抓取 HTML Form Post 數據
// 加入後 req 就會多出 body 這個屬性
// 就可透過 req.body 得到 POST 的數據

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())


// 每次重開服務時，預先保存的留言(可省略)
var comments = [
  {
    name: 'User 1',
    message: 'Hello',
    dateTime: '2021-6-2 17:30:32'
  },
  {
    name: 'User 2',
    message: 'There',
    dateTime: '2021-6-2 17:30:32'
  }
]

// 先將 art-template 模板引擎引入 express
// 第一個參數表示將哪種頁面的副檔名 (html, jade, art) 帶入模板引擎中
app.engine('html', require('express-art-template'))


// Opening access to folder public (若不開放 user 存取則可省略)
// eg. http://localhost:3000/public/css/main.css

// 若省略第一個參數，則存取 public folder 內的檔案時則不須在網址列加上 /public
// eg. http://localhost:3000/css/main.css
app.use('/public/', express.static('./public/'))

// 路由邏輯
// Homepage
app.get('/', function (req, res) {
    // res.render('HTML 檔名), {要帶入的 data})
    // 第一個參數不須寫路徑，預設會去 views folder 裡面找
    // 若要更改默認目錄，則用 app.set('views', '默認目錄的路徑')
    res.render('index.html', {
    comments: comments
  })
})

// Post page
app.get('/post', function (req, res) {
  res.render('post.html')
})

// Click submit on post page
app.post('/submit', function (req, res) {
  var comment = req.body
  
  // today =  2021-05-28T18:40:03.622Z
  var today = new Date();
  
  var date = today.getFullYear() + '-' + (today.getMonth() + 1 ) + '-' + today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date + ' ' + time
  comment.dateTime = dateTime
  
  comments.unshift(comment)

  // submit 結束後跳轉至首頁
  res.redirect('/')
})

// 若頁面不存在則導向到 404 page
app.get('*', function (req, res) {
  res.status(404).render('404.html')
})

app.listen(3000, function () {
  console.log('running...')
})
```

## 建立 views
這裡的 post.html 與上篇一樣，只有將 ```<form action="/submit" method="post">``` method 從 get 改為 post <br />

### post.html
```html {22}
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Bulletin</title>
  <!-- Import bootstrap -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
  <div class="header container">
    <div class="page-header">
      <h1><a href="/">Homepage</a></h1>
    </div>
  </div>
  <div class="comments container">
    <!--
        submit 表單時需有 name 屬性
        action 為 submit 後會跳轉的 url 地址
     -->
    <form action="/submit" method="post">
      <div class="form-group">
        <label for="input_name">Name</label>
        <input type="text" class="form-control" id="input_name" name="name" placeholder="Type your name here" required minlength="2" maxlength="10">
      </div>
      <div class="form-group">
        <label for="textarea_message">Comments</label>
        <textarea class="form-control" id="textarea_message" name="message" cols="30" rows="10" required minlength="5" maxlength="20"></textarea>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
</body>

</html>
```