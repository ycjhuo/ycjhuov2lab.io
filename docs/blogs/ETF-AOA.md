---
title: 懶人投資，AOA，自動再平衡的 ETF
description: '懶人投資|美股 ETF|自動再平衡 ETF|ETF AOA 介紹|股票債券 ETF'
date: 2021-02-27 14:02:30
tag: [Index, ETF]
category: Investment
---
這星期（02/22 - 02/26），美股市場（集中在科技股）出現了幅度較大的一次下跌（從去年的熔斷之後）。<br />
佔科技股權重最大的前三支股票表現如何呢？
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">蘋果 AAPL</th>
    <th align="center">微軟 MSFT</th>
    <th align="center">亞馬遜 AMZN</th>
    <th align="center">那斯達克指數 QQQ</th>
    <th align="center">方舟基金 ARKK</th>
  </tr>
  <tr>
    <td align="center">漲跌幅（02/22 - 02/26）</td>
    <td align="center">-3.38%</td>
    <td align="center">-2.08%</td>
    <td align="center">-3.57%</td>
    <td align="center">-3.77%</td>
    <td align="center">-12.95%</td>
  </tr>
</table>

<br />
而近二年表現傑出的方舟基金（ARKK），跌幅約為那斯達克的 3.5 倍左右。ARKK 前四大持股的表現如下：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">特斯拉（TSLA）</th>
    <th align="center">Square（SQ）</th>
    <th align="center">ROKU（ROKU）</th>
    <th align="center">Teladoc Health（TDOC）</th>
  </tr>
  <tr>
    <td align="center">在 ARKK 中佔比</td>
    <td align="center">10.08%</td>
    <td align="center">5.71%</td>
    <td align="center">5.54%</td>
    <td align="center">5.23%</td>
  </tr>
  <tr>
    <td align="center">漲跌幅（02/22 - 02/26）</td>
    <td align="center">-11.4%</td>
    <td align="center">-15.42%</td>
    <td align="center">-13.61%</td>
    <td align="center">-11.4%</td>
  </tr>
</table>

上面這二個表可以看出，通常報酬率越高的股票，隱含的風險也越高。那有沒有哪一支股票是在空頭市場中有較少的跌幅，但在股票上漲時，也能有不低的漲幅呢？

## AOA，懶人投資首選
AOA（iShares Core Aggressive Allocation ETF），是一支由 80% 的全球股市以及 20% 的全球債券所組成的 ETF。前四大持股就佔了投資組合的 93.3%

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">S&P 500 ETF（IVV）</th>
    <th align="center">已開發國家 ETF（IDEV）</th>
    <th align="center">美元債券（IUSB）</th>
    <th align="center">新興國家 ETF（IEMG）</th>
  </tr>
  <tr>
    <td align="center">AOA 中佔比</td>
    <td align="center">41.06%</td>
    <td align="center">27.33%</td>
    <td align="center">15.01%</td>
    <td align="center">9.90%</td>
  </tr>
</table>

這是一隻由 ETF 所組成的 ETF。藉由持有不同市場的 ETF 來達到涵蓋大部分市場及分散風險的作用。<br />
這麼做的好處是：讓我們不會錯過任何一個市場的報酬，也不會因為哪個國家或產業的經濟衰退，嚴重影響到報酬率。<br />
且在 AOA 的投資組合中，涵蓋了 20% 的債券，在長期市場下跌時，也能提供一定的緩衝作用。

而為什麼會說懶人投資呢？若想要組成特定的股債比例，我們也可以自己配置。<br />
像是：買 80% VT（全球股市 ETF）以及 20% 的 BND（總體債券市場 ETF）一樣可以達到這個效果。還可以依據個人的喜好，將股/債比調整為 7:3，或是 6:4 等等。

但經過一段時間後，若股票漲得比較快，而債券的漲幅跟不上股票，那我們所配置的股/債比就會連帶失衡。<br />
而 AOA 的好處是```每半年就會自動幫我們再平衡一次```，自動將目前 ETF 內的股/債比再調整回 8:2。<br />
真正實現懶人投資，不用再自己每段時間就要再平衡一次。<br />

![57-01](../images/57-01.png)

## AOA 歷史表現如何
如上圖所示，iShare 的旗下有 4 種不同的再平衡 ETF，而 AOA 只是其中一種（管理費皆為 0.25%）：
- AOA：股/債比 8:2
- AOR：股/債比 6:4
- AOM：股/債比 4:6
- AOK：股/債比 3:7

雖然這四種 ETF 都有自動實現再平衡的功能，但若要我選擇的話，我會選擇股/債比 8:2 的 AOA。<br />
債券配置的比例越高，雖然降低了風險，但連帶的也會喪失上漲的動能。

來看看在去年熔斷時（2020/02/18 - 2020/03/18），AOA 的表現如何：
![57-02](../images/57-02.png)

<table style="width:100%">
  <tr>
    <th align="center">2020/02/18 - 2020/03/18</th>
    <th align="center">全球股/債 8:2（AOA）</th>
    <th align="center">S&P 500 指數（IVV）</th>
    <th align="center">全球股市 ETF（VT）</th>
    <th align="center">方舟基金（ARKK）</th>
    <th align="center">總體債券（BND）</th>
  </tr>
  <tr>
    <td align="center">漲跌幅</td>
    <td align="center">-26.8%</td>
    <td align="center">-28.61%</td>
    <td align="center">-30.54%</td>
    <td align="center">-35.56%</td>
    <td align="center">-5.23%</td>
  </tr>
  <tr>
    <td align="center">管理費</td>
    <td align="center">0.25%</td>
    <td align="center">0.03%</td>
    <td align="center">0.08%</td>
    <td align="center">0.75%</td>
    <td align="center">0.035%</td>
  </tr>
  <tr>
    <td align="center">目前規模（億）</td>
    <td align="center">11.8</td>
    <td align="center">2388.5</td>
    <td align="center">246.4</td>
    <td align="center">226.1</td>
    <td align="center">3053.5</td>
  </tr>
  <tr>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
    <td align="center"></td>
  </tr>
  <tr>
    <td align="center">2020/03/19 - 2021/02/28 漲跌幅</td>
    <td align="center">50.36%</td>
    <td align="center">59.13%</td>
    <td align="center">66.27%</td>
    <td align="center">275.79%</td>
    <td align="center">5.38%</td>
  </tr>
</table>

![57-03](../images/57-03.png)

因配置了 20% 的債券，在股市下跌時，表現比美股市場及全球股市的 ETF 好。但熔斷後，在上漲的表現上就完全相反。<br />
若是以長期投資，不想花時間研究市場，且打算配置一部分的債券來降低風險的人。AOA 是個不錯的選擇。

## 後記
最近我持有 AOA 滿三年了，這三年的報酬率約為 21.78%，加上配息則約為 24.5%。<br />
![57-04](../images/57-04.png)

這個報酬率看起來不差，但跟 VT（全球股市 ETF）相比下，少了約 10%。<br />
為了減少 ETF 在我的投資組合的比重（且我也不是屬於保守類的投資人）。我在週五時賣了 1/3 的 AOA，將得到的資金全部投入特斯拉。<br />
若下週，特斯拉的股價繼續下跌的話，我會繼續賣出 AOA，加碼在特斯拉上（高風險高報酬，若風險承受度不高的人，不建議這麼做）。


延伸閱讀：<br />
[方舟投資（ARK Invest），著重於破壞式創新的主動型 ETF](https://ycjhuo.gitlab.io/blogs/Ark-Invest.html)<br />
[七個月飆升 65%，方舟投資（ARK Invest）公開每日操作紀錄](https://ycjhuo.gitlab.io/blogs/Explore-Ark-Invest.html)<br />
[追蹤美股三大市場的指數型 ETF](https://ycjhuo.gitlab.io/blogs/US-Stock-Markets-Index-ETF.html)<br />