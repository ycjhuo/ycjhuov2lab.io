---
title: 點讚，留言，PO 文，寫部落格都能賺錢？Potato Media 介紹
description: 'Potato Media 介紹 | Potato Media 積分 | Potato Media CFO | Potato Media 推薦'
date: 2021-10-08
tag: [Blog, CFO, Potato Media, SocialFi, Crypto]
category: Blog
---

Potato Media 是一個新興的網路社群平台，與 Dcard、PTT 類似，平台內依據主題的不同而有不同的討論區，例如：3C、汽機車、寵物、美食等等 <br />

相似的討論平台有這麼多，Potato Media 有什麼不同呢？

Potato Media 最大的賣點＆不同處就是：對文章按讚，留言都可獲得「積分」，而「積分」每天都會由系統自動轉化為「虛擬貨幣 CFO」，而使用者可以到交易平台將「CFO」兌換成其他加密貨幣並轉成現金，簡化的流程圖如下：

發表文章、按讚、留言來賺積分 -> 積分（每天）自動轉為 CFO -> CFO 換成 USDT -> USTD 轉成現金

## Potato Media 獲利模式
首先大家第一個疑問想必是，按讚，留言就能拿到錢，這個平台怎麼這麼好，是不是什麼問題，或是會不會倒？資金要從哪裡來...等等 <br />

Potato Media 的獲利方式跟其他社群平台一樣，都是透過「線上廣告」來賺錢，而這塊市場其實非常的大，就拿現在的線上廣告霸主 Google & Facebook 來說：<br />


依據 2021 第二季的財報，Facebook 從「線上廣告」賺到了 285.8 億（美金），而 Google 更是誇張，賺了 428.5 億（美金），這二個加起來總共賺了 714.3 億（美金）<br />

由此可知，「線上廣告」這個市場有多麽賺錢，而且還在持續成長中 <br />

對 Facebook & Google 收入模式有興趣的，點擊：[2021 Q2 財報，Facebook vs. Google，誰更具投資價值](https://ycjhuo.gitlab.io/blogs/FB-GOOG-2021-Q2-Earnings.html) <br />


因此 Potato Media 上的廣告非常多，幾乎是每進入一個頁面都會跳出廣告，且在文章裡面也至少會有 4 個廣告（頁首、頁尾、橫幅、側邊），但因為文章排版做得不錯，因此並不會太過影響閱讀體驗 <br />

而 Potato Media 為了得到越多的流量，進而從廣告商賺到更多的錢，因此 Potato Media 才透過使用者只要在上面互動，就會得到相對應的積分來吸引＆增加使用人數 <br />


而這也就形成了一個正向循環：<br />

Potato Media 將一部分的廣告收益，經由 CFO 的方式，與使用者分享，而使用者為了為了獲得積分在平台上持續互動，Potato Media 得到更多流量，也增加了廣告收益 <br />


## Potato Media 積分＆ CFO 代幣
在 Potato Media 中，對讀者來說：只要對文章按讚即可得到 20 積分，留言則是 30 積分 <br />

對作者來說：發布文章不會得到積分，但只要有人對你的文章按讚，一個人可以讓你得到 10 積分，一個留言可以讓你得到 15 積分 <br />

這些積分每天加起來，依據每天要發放的代幣（CFO）總數，發放給每個人 <br />

例如：每天要發放的代幣（CFO）有 100 個，而今天共產生了 10,000 個積分，那麼每 1 積分就代表著 0.01 的 CFO <br />

因為每天產生的積分都不同，因此就算你每天都拿到了同樣數量的積分，獲得的 CFO 也不會一樣 <br />

## CFO 代幣保值嗎
如同現在百家爭鳴的加密貨幣，CFO 也是基於區塊鏈的基礎上，開發出來的一款加密貨幣（如：比特幣，以太幣等）<br />

這些貨幣沒有價值，是使用者賦予它們價值，越多使用者願意持有它、購買它，也就讓加密貨幣有了價值 <br />

相反的，若是使用者對這款加密貨幣失去信心，它也會很快的失去價值  <br />

目前在（10/08） [ProEX 交易所](https://www.proex.io/zh_CN/)，CFO 兌換 USDT 的匯率是 0.5385（2021 一月時的匯率是 0.5395），從這一年來看，波動並不大，相比其他加密貨幣來說，CFO 相對穩定

## USDT 是什麼
USDT 也是一款加密貨幣，目前在 Coinbase 交易所中，是市值第四的加幣貨幣 <br />

前三名為：比特幣（BTC），以太幣（ETH）、艾達幣（ADA）

USDT 與其他加密貨幣不同的是，它保證了使用者可以用 1 USDT 兌換成 1 美元 <br />

::: tip
發行 USDT 的公司（Tether）僅是一般公司，並不是政府機構，因此若公司經營不善，或是倒閉，也就無法實現 1 USDT 兌換成 1 美元 的承諾
:::

## 後記
因此，既然 CFO 兌換 USDT 的匯率是 0.5385，也就表示 一個 CFO 相當於 0.5385 美元（約新台幣 15.078）<br />

對於平常就有在經營 Blog 或是逛論壇的人，或許 Potato Media 會是個「點讚成金」的地方 <br />

下篇會再分享我在 Potato Media 上經營了 3 個月的感想＆實際收益

## Potato Media 推薦碼
對 Potato Media 有興趣，請用我的[推薦碼](https://www.potatomedia.co/signup?invite=pRUdIeRnB)註冊，成為會員，那麼我們雙方都可以得到 100 積分 <br />

或是到 [我的 Potato Media 頁面](https://www.potatomedia.co/user/83aed257-a06a-4b3c-beee-e356c2ba1efb) 看我的文章（無須註冊即可觀看）<br />

若對於 Potato Media 有其他問題也可留言，我會就我所知的盡量解答 <br />

對 Potato Media 收益有興趣的，可點擊：[真的能在 Potato Media 賺到錢嗎？公開 Potato Media 收益](https://ycjhuo.gitlab.io/blogs/Potato-Media-Earnings.html)