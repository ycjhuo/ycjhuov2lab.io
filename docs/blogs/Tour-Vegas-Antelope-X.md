---
title: 開箱！羚羊峽谷Ｘ（Antelope Canyon X）
description: '羚羊峽谷介紹|羚羊峽谷推薦|拉斯維加斯 羚羊峽谷|拉斯維加斯 必去|拉斯維加斯 景點|拉斯維加斯 推薦|馬蹄灣 介紹|馬蹄灣 推薦|Antelope Canyon 介紹|Antelope Canyon 推薦|Horseshoe Bend 介紹|Horseshoe Bend 推薦|Las Vegas Horseshoe Bend|Las Vegas Antelope Canyon|Horseshoe Bend Antelope Canyon'
date: 2021-10-04
tag: [Las Vegas, Arizona]
category: Travel
---

上篇[開箱！羚羊峽谷（Antelope Canyon）＆馬蹄灣（Horseshoe Bend）拉斯維加斯 當天來回](https://ycjhuo.gitlab.io/blogs/Tour-Vegas-Horseshoe-Bend-Antelope.html) 介紹了我們這次訂的行程以及馬蹄灣，這篇就來介紹重頭戲的：羚羊峽谷 X（Antelope Canyon X）

## 羚羊峽谷（Antelope Canyon）介紹
羚羊峽谷，分為：上羚羊峽谷 ＆ 下羚羊峽谷，上羚羊峽谷地勢較為平坦，坐車即可進入；下羚羊峽谷則較陡峭，須爬上爬下，而我們訂的[行程](https://www.getyourguide.com/las-vegas-l58/antelope-canyon-and-horseshoe-bend-tour-from-las-vegas-t169954/?utm_source=getyourguide&utm_medium=sharing&utm_campaign=activity_details&visitor_id=LIQMMS3HSYZST4AK8F33SLT674NW97XG) 有註明會去下羚羊峽谷，或是羚羊峽谷Ｘ，而這次我們去的則是網路資料較少的羚羊峽谷Ｘ<br />

::: tip
若要拍到跟上篇類似的光照，則須選擇正午入谷的團，岩石的顏色也會較為紅，我們這次入谷時是 13:30（九月底），已經沒有什麼光會照進峽谷了
:::

## 羚羊峽谷 X（Antelope Canyon X）
吃完午餐後，再開半小時就到羚羊峽谷了，這邊須由當地的導遊陪同才可進入，且只能攜帶手機、水、相機，其它東西都要先放車上，在等待當地導遊來的時候，安東尼會先幫我們辦好相關手續，並簽署失蹤不負責切結書，就可以準備進入羚羊峽谷了 <br />

進入羚羊峽谷前，要先走一段樓梯下去，約 5 -10 分鐘 <br />
![羚羊峽谷 X（Antelope Canyon X）樓梯](../images/118-01.jpg)

走下來後，就可看到洞口，若沒日照的話，則岩石差不多就是這個顏色 <br />
![羚羊峽谷 X（Antelope Canyon X）洞口](../images/118-02.jpg)

進入峽谷後，往上可以看到一個明顯的Ｘ，這也是之所以會被稱為「羚羊峽谷 X」的由來 <br />
![羚羊峽谷 X（Antelope Canyon X）](../images/118-03.jpg)

峽谷不長，約 5 - 10 分鐘即可走到盡頭，過程中，當地導遊也會跟我們說哪個點以及哪個濾鏡（iPhone）比較好拍 <br />
![羚羊峽谷 X（Antelope Canyon X）](../images/118-04.jpg)

因疫情期間，所以在峽谷內不可以脫下口罩。下圖是沒被陽光照到的顏色 <br />
![羚羊峽谷 X（Antelope Canyon X）](../images/118-05.jpg)

參觀完峽谷後，走上一開始來的階梯，就有吉普車載我們跟原本的司機安東尼會合了（這時我有給當地導遊 $5 的小費，但也沒強制要給）<br />
![羚羊峽谷 X（Antelope Canyon X）](../images/118-06.jpg)

在回 Vegas 的路上，安東尼一樣會沿途停靠 3 個休息站，在 20:30 左右就到 Las Vegas 結束行程囉 <br />
為了感謝辛苦的司機安東尼，我也給了他 $20 的小費（也沒強制要給）<br />

## 後記
這個 Tour 稅前為 $249 美金，稅後 + 小費差不多 $300，不能說便宜，也不能說貴 <br />
畢竟光是開車時間就佔了 10 小時左右，路上景色非常的單調，也沒有其他車，自己開很容易就會睡著，而且一路上完全都是沒有網路＆訊號的狀態 <br />
很難想像，如果在半路拋錨的話，完全無法找不到人幫忙 <br />

租車的話，若是一般五人座小車（Ford Focus）加上保險的話，則差不多 $130，差不多是這個 Tour 的一半 <br />
但若是同行的有 3 人以上，互相可以有個照應的話，租車也不失為一個選擇 <br />

::: tip
自行租車的話，一樣只能開到羚羊峽谷的外圍，進去一樣要由當地導遊陪同（票價約 $50），有興趣的可參考這個行程：[Ken's Tours](https://www.lowerantelope.com)
:::