---
title: 疫情間的西雅圖三天二夜（Day 1）派克市場 星巴克創始店 西雅圖藝術博物館 西雅圖中央圖書館 西雅圖摩天輪
description: '西雅圖 Seattle 景點|西雅圖 Seattle 推薦|西雅圖 介紹|西雅圖 Seattle 星巴克創始店|西雅圖 Seattle 三天二夜|西雅圖 Seattle 第一間星巴克|西雅圖 Seattle Starbucks|西雅圖 Seattle 必去'
date: 2021-03-31 00:42:01
tag: [Seattle, Covid]
category: Travel
---
去年十月因為華盛頓州還沒強制規定跨州旅客須隔離，趁著這個機會從紐約到西雅圖來個三天二夜的旅行。從紐約 JFK 機場到西雅圖的 SEA 機場，搭乘 10:15 起飛的阿拉斯加航班，到西雅圖時是 13:30

順便體驗剛開幕的 JFK 美國運通百夫長貴賓室，有興趣的可參考：<br />
[開箱！美國運通百夫長貴賓室（紐約 約翰·甘迺迪國際機場 JFK Airport）](https://ycjhuo.gitlab.io/blogs/JFK-Centurion-Lounge.html)

---

到達 SEA 機場後，乘坐西雅圖的輕軌（Link light rail station）從機場站（SeaTac / Airport Station）到西雅圖市區（Westlake Station），約 40 分鐘車程 <br />

而之所以選擇 Westlake Station 這站下車是因為要入住的飯店就在這站附近

![Link light rail station Map](../images/71-01.jpg)

::: tip
International District / Chinatown 到 Westlake / Seattle 這段是西雅圖市中心，大部分的景點都介於這四站之間
:::

## 景點規劃
第一天的行程是：<br />
派克市場（Pike Place Market）➔ 西雅圖藝術博物館（Seattle Art Museum）➔ 西雅圖中央圖書館（Seattle Public Library-Central Library）➔ 西雅圖大摩天輪（The Seattle Great Wheel）

在地圖上為從北往南移動。

![Seattle Attractions](../images/71-02.png)

## 派克市場 Pike Place Market
來到西雅圖，第一站當然就是派克市場，裡面除了星巴克創始店之外，其他熱門的店還有：<br />
巧達濃湯（Pike Place Chowder），俄羅斯麵包店（Piroshky Piroshky），中式西點店（Mee Sum Pastry）以及手做起司店（Beecher's Handmade Cheese），這些店都位在星巴克創始店旁邊而已
![Pike Place Market](../images/71-03.jpg)

## 星巴克創始店
派克市場裡面最有名的就是星巴克創始店<br />
雖然是疫情期間，但門口還是大排長龍，這間星巴克無法透過 Starbucks App 點餐，因此只能關乖乖排隊了
![First Starbucks store](../images/71-04.png)
![First Starbucks store](../images/71-05.jpg)
![First Starbucks store](../images/71-06.jpg)
![First Starbucks store](../images/71-07.jpg)

## 西雅圖藝術博物館（Seattle Art Museum）
逛完派克市場後，繼續往南走，經過有名的口香糖牆（The Gum Wall）後<br />
就會來到第二個景點：西雅圖藝術博物館（Seattle Art Museum），博物館的外面有個巨大的人形雕像，由於時間關係，就沒有進去了<br />
![Seattle Art Museum](../images/71-08.jpg)

但博物館的對面，有我認為西雅圖市區最漂亮的地方，很適合看夕陽（位於開頭地圖的藍色區域）
這裡旁邊有一間 Starbucks Reserve，但不大
![The Most beautiful place in seattle](../images/71-09.jpg)


## 西雅圖中央圖書館（Seattle Public Library-Central Library）
離開上個景點後，往博物館那個方面走，就會到達西雅圖中央圖書館（博物館在 2 大道，圖書館在 4 大道，西雅圖的大道數字越大，地勢越高）
建築外觀很特別，但因為去的時候已經關門了，所以也沒有進去
![Seattle Public Library-Central Library](../images/71-11.jpg)

## 西雅圖大摩天輪（The Seattle Great Wheel）
離開博物館後，往第一大道的方向走（下坡的方向），就可以看到摩天輪了。
![The Seattle Great Wheel](../images/71-10.jpg)

看完摩天輪後，就可以看要再往南到西雅圖舊城區的拓荒者廣場（Pioneer Square），沈船停車場（Sinking Ship）或是史密斯塔（Smith Tower）看夜景或是回飯店休息結束這天。

我自己是覺得下方的三個景點沒有什麼特別之處，如果時間特別趕的話，可以到摩天輪後就結束行程

因為我那時選擇的飯店（The Charter Hotel Seattle）在派克市場旁邊，所以就又按照原路走回去

---
下篇：[疫情間的西雅圖三天二夜（Day 2）瑞尼爾山 The Spheres 太空針塔 凱利公園](https://ycjhuo.gitlab.io/blogs/Seattle-3-Days-Itinerary-Day-2.html)

對飯店有興趣的可參考：<br />
[開箱！西雅圖希爾頓 The Charter 飯店（ The Charter Hotel Seattle, Curio Collection by Hilton）](https://ycjhuo.gitlab.io/blogs/The-Charter-Hotel-Seattle.html) <br />

[開箱！西雅圖萬豪 Renaissance 飯店（ Marriott Renaissance Seattle Hotel ](https://ycjhuo.gitlab.io/blogs/Renaissance-Seattle-Hotel.html) <br />
