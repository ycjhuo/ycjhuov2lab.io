---
title: 開箱！亞特蘭大 The Club 貴賓室（亞特蘭大-Hartsfield-Jackson 國際機場 ATL Airport）
description: 'Atlanta 亞特蘭大貴賓室| Hartsfield-Jackson Atlanta Lounge 貴賓室|亞特蘭大 ATL The Club 貴賓室|The Club Atlanta 貴賓室'
date: 2022-06-27
tag: [Lounge, Atlanta]
category: Travel
---

## 亞特蘭大機場（ATL）介紹
亞特蘭大機場（Hartsfield-Jackson Atlanta International Airport）是全世界最繁忙的機場，主要是因亞特蘭大機場是美國國內線航班的重要轉機點之一，整個機場共有 7 個航廈（T, A, B, C, D, E, F）

幾乎每個航廈都有貴賓室，但若非搭乘達美航空（Delta），新貴通卡（Priority Pass）能進的貴賓室僅有位於 B 航廈的 Minute Suites 以及 F 航廈的 The Club ATL

::: tip
在亞特蘭大機場（ATL）只要過了安檢之後，就可以搭乘機場捷運在各個航廈間快速移動 <br />
這個機場捷運每 85 秒就有一班車，從最遠的 F 航廈搭到 T 航廈僅要 7 分鐘
:::

![亞特蘭大機場 Hartsfield-Jackson Atlanta International Airport (ATL) 機場捷運](../images/182-01.jpg)

位於 B 航廈的 Minute Suites 貴賓室內有單獨的小房間讓旅客在裡面休息，但要先預約使用的時間才可進入，不太適合做為我們休息等候的貴賓室 <br />
這樣一來，若非搭乘達美航空(Delta airline)，而能讓我們悠閒等登機的貴賓室，就只剩下 F 航廈的 The Club 了
![亞特蘭大機場 Hartsfield-Jackson Atlanta International Airport (ATL) Minute Suites 貴賓室 ](../images/182-02.jpg)

## The Club Atlanta 介紹
搭乘機場捷運抵達 F 航廈後，可看到告示牌寫著機場貴賓室（Airline Clubs）在最上層，上了手扶梯後，就能看到 The Club ATL 了
![亞特蘭大機場 Hartsfield-Jackson Atlanta International Airport (ATL) F 航廈](../images/182-03.jpg)

由於當天是長週末的結尾，The Club 門外排了長長的隊伍，排了半小時後總算到了櫃檯 <br />
結果被告知由於裡面人數過多，需要先留電話，等簡訊通知後才可入內；沒想到一等就是一小時，還好當天因為班機 Delay，所以我們時間還是很充裕
![亞特蘭大機場 ATL 貴賓室 The Club Atlanta 介紹](../images/182-04.jpg)

進入 The Club Atlanta 貴賓室後果真人山人海，就算有管控人數，仍是不好找到座位
![亞特蘭大機場 ATL 貴賓室The Club Atlanta 座位區](../images/182-05.jpg)

而當天提供的飲食偏向中東類型，有二種飯可以選擇：<br />
分別為黑豆飯、以及下圖的甜菜（Beet）蕃茄飯 <br />
配菜有：起司、生菜、皮塔麵包（Pita）以及鷹嘴豆泥醬（Hummus）；零食的選擇則只有：M&M 巧克力、芥末豆 <br />
![亞特蘭大機場 ATL 貴賓室The Club Atlanta食物](../images/182-06.jpg)

可能是飲食習慣不同，個人覺得整個貴賓室內最好吃的東西就是芥末豆，其它的吃過一次就完全不會想再吃 <br />
飲料的部分則是可樂、咖啡等等，但可能是貴賓室人太多，飲料機出來的氣泡飲料均已完全沒氣，等於只是在喝糖水 <br />
酒精飲料的部分，貴賓室內也有 bar 可以點酒

## 後記
這次在 ATL 機場是搭聯合航空（United Airline）回紐約，登機航廈是在 T，與 F 航廈的 The Club Atlanta 有段距離，本來以為在不同航廈間移動會需要安檢多次，結果只要安檢一次就夠了，很方便我們在不同航廈間移動 <br />

而機場捷運的班次很多，不到二分鐘就有一班車，讓我們可以在貴賓室好好休息，不用趕著回到原本要登機的航廈，留個 30 分鐘再出貴賓室就很足夠了 <br />

唯一缺點就是飲食部分不太屬於大眾口味，加上又要等一小時才能入內，若不是很早到機場的話，The Club Atlanta 貴賓室實在不太吸引人跑這一趟

::: warning
點擊觀看更多： <br />
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [貴賓室開箱文](https://ycjhuo.gitlab.io/tag/Lounge/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::