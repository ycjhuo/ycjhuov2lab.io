---
title: Honeygain 掛網被動收入還不夠？教你如何用 JMPT 轉到 Anchor 賺取 20% 年利率（上）
description: 'Honeygain 介紹|Honeygain 推薦|Honeygain 心得|Honeygain 掛網賺錢|Honeygain 被動收入|被動收入推薦|被動收入 Passive Income|JMPT 出金|JMPT換現金|MetaMask 小狐狸錢包轉 Terra|MetaMask 小狐狸錢包 UST|MetaMask 小狐狸錢包 幣安鏈 BSC BEP|UST Anchor 利率 MetaMask 小狐狸錢包'
date: 2022-04-17
tag: [Passive Income, Honeygain, Anchor]
category: Investment
---

自從 Honeygain 新導入 JumpTask 模式＆出金方式後，我們不僅可以多獲得 50% 的流量獎勵外，還可以利用 JMPT 的無最低出金門檻限制，大幅增加出金的效率 <br />

這篇就來介紹：如何運用 Honeygain 的 JMPT 出金模式，來獲得年利率 20% 的利息，流程如下： <br />

::: warning
Honeygain → JMPT → MetaMask → Terra Station → Anchor
:::

要達成這個方法，必須具備：
1. Honeygain 帳號（對 Honeygain 有興趣？可參考[有網路就能賺錢？Honeygain 介紹](https://ycjhuo.gitlab.io/blogs/Honeygain.html)）
2. MetaMask 錢包，設置幣安智能鏈 BSC（Binance Smart Chain Mainnet），且有少許 BNB <br />
 怎麼免費取得 BNB？參考 [Peer2Profit，以加密貨幣為主的掛網賺錢 App](https://ycjhuo.gitlab.io/blogs/Peer2Profit.html)
3. TerraStation 錢包

以下為幣安智能鏈（BSC）的設定，來源 [JMPT 官方網站](https://jumptask.zendesk.com/hc/en-gb/articles/4415873024017-Setting-up-Metamask-Wallet-and-adding-Binance-Smart-Chain-BSC-network)
::: tip
Network Name: Binance Smart Chain Mainnet <br />
New RPC URL: https://bsc-dataseed.binance.org/ <br />
ChainID: 56 <br />
Symbol: BNB <br />
Block Explorer URL: https://bscscan.com <br />
:::

## 將 Honeygain 的 JPMT 轉到 MetaMask
::: warning
[Honeygain → JMPT]() → MetaMask → Terra Station → Anchor
:::

1. 在 Honeygain 的 JumpTask 模式下，點擊 JumpTask Dashboard 進到 JumpTask 的頁面
2. 點擊 Transfer to my wallet，即可將 JPMT 傳送到 MetaMask 錢包 <br />
 按鈕下方會提示，此交易會耗費 0.04152 JMPT 作為 Gas fee

![Transfer JMPT to Metamask](../images/171-01.png)

3. 完成後，我們就可以在 MetaMask 錢包看到剛剛轉入的 JMPT 了 <br />
 下圖顯示 38 個 JMPT 是因為我的錢包中本來就有 7 個 JMPT 在裡面

![JMPT in Metamask](../images/171-02.png)

## 將 JMPT 轉換為 UST
::: warning
Honeygain → JMPT → [MetaMask]() → Terra Station → Anchor
:::

將 JMPT 轉到 MetaMask 錢包後，我們就要來將 JMPT 轉為 UST 這個能幫我們賺取 20% 年利息的穩定幣 <br />
在這個步驟上，我們須在 MetaMask 的 BSC 鏈上加入 UST token（如下圖）
::: tip
Toten Address: 0x23396cf899ca06c4472205fc903bdb4de249d6fc <br />
Token Symbol: UST <br />
Token of Precision: 18 <br />
:::

![Add UST token toMetamask](../images/171-03.png)

1. 打開 MetaMask 的 Swap
2. 在 Swap to 那邊選擇 UST，在 slippage tolerance 選擇 2%，以避免在交易量小的時候，兌換價格差距太大
3. 按下 Swap 後，可以看到我們成功將 38.81 個 JMPT 後，兌換成 86.63 個 UST（Gas fee 約 0.00177 BNB）

![Swap JMPT to USD on Metamask](../images/171-04.png)

## 將 UST 從 Metamask 轉到 Terra Station 錢包
::: warning
Honeygain → JMPT → [MetaMask → Terra Station]() → Anchor
:::

在這個步驟，我們要將 UST 從 MetaMask 錢包 轉到 Terra Station 錢包，以便達成把 UST 存到 Anchor 的目的 <br />

1. 首先進到 [Terra Bridge](https://bridge.terra.money/) 的頁面（確認右上角的 MetaMask 錢包已連線）
2. 在 From 的網路選擇 BSC，也就是我們在 MetaMask 存放 UST 的網路；To 選擇 Terra
3. Asset 選擇要轉移的幣種，也就是 UST，以及要轉移的金額
4. Destination Address 填上收款地址，也就是自己的 Terra Station 錢包地址
5. 按下 Next 後，會來到交易確認頁，之後再按 Confirm

![Transfer UST from Metamask to Terra Station](../images/171-05.png)

6. 在交易確認頁，按下 Confirm 後，會跳出 MetaMask 視窗，詢問是否要執行這筆交易，點選 Confirm 後，交易即完成 <br />
 這時仍會需要 BNB 作為 Gas fee（約為 0.00019 BNB）
![Transfer UST from Metamask to Terra Station](../images/171-06.png)

## 後記
由於篇幅原因，這篇就先介紹到這邊，下篇將會介紹該如何將 Terra Station 裡面的 UST 轉到 Anchor 中賺取 20% 的年利率 <br />
請見 [Honeygain 掛網被動收入還不夠？教你如何用 JMPT 轉到 Anchor 賺取 20% 年利率（下）](https://ycjhuo.gitlab.io/blogs/Anchor-Protocol-Steps.html)

延伸閱讀：<br />
[賺取 Anchor 的 20% 年利率，手續費大公開，至少要存入多少錢才划算](https://ycjhuo.gitlab.io/blogs/Anchor-Cost.html)


::: warning
點擊觀看更多： <br />
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::