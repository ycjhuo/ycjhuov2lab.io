---
title: 如何在 Vue-Cli 使用路由來新增頁面
description: 'Vue-Cli 路由 介紹|Vue-Cli 路由 教學|Vue-Cli 新增頁面|Vue 路由 介紹|Vue 路由 教學|Vue 新增頁面'
date: 2021-01-31 14:43:54
tag: [Vue.js, Router]
category: Programming
---

在用 Vue-Cli 腳手架建立一個新專案後，該如何在這個專案中新增頁面呢？以下我們用這個例子來說明。
## 基本介紹
### 建立專案
首先，利用下面指令建立一個 myblog 的專案，選擇 Manually select features，之後將 Router 打勾（按空白鍵）。
```powershell
$ vue create myblog
```
::: tip
這裡要注意：建立專案時，如果專案名稱有大寫的話，會報錯。建議可用 - 來分隔。如 my-blog
:::
建立完後，可以看到 myblog 這個資料夾已被建立。

### 檔案介紹
在進入 myblog 資料夾後，要注意的檔案有：
1. src 項下的 App.vue：這個檔案整個專案的首頁，可以想成網頁中的 index.html
2. views 資料夾裡面的 vue 檔案：這二個是專案中會用到的頁面（components），稍後我們要新增頁面時就在這裡創建 .vue 檔
3. router 資料夾中的 index.js：專案中設置路由規則的地方，新增頁面後要在這邊設置要跳轉的頁面路徑

創建好專案後，我們先 cd 進入專案資料夾，接著啟動 server
```powershell
$ cd myblog
$ yarn serve
```
在瀏覽器中輸入 ```http://localhost:8080/``` 後，就可以看到頁面已被啟動（如下圖）。

![51-01](../images/51-01.png)

上圖中紅框的 Home 與 About：是上面第 2 點介紹的，位在 views 資料夾中的 About.vue 及 Home.vue  <br/>

而藍框的部分則是由上面第 1 點說的 App.vue 所組成，如下面第 4 行引入 Home 頁面後，在第 7 行顯示頁面。

```App.vue```
```js{4,7}
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link> |
      <router-link to="/about">About</router-link>
    </div>
    <router-view/>
  </div>
</template>
```
 <br/>

在 Home.vue 中，可以看到它在第 4 行引入了 HelloWorld，並設定 msg。因此可看出藍框的資料其實都是來自 HelloWorld.vue

```Home.vue```
```js{4}
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
  </div>
</template>

<script>
// @ is an alias to /src
import HelloWorld from '@/components/HelloWorld.vue'

export default {
  name: 'Home',
  components: {
    HelloWorld
  }
}
</script>
```
而之所以 Home.vue 可以在引入 HelloWorld.vue 時，設定 msg 的值，是因為 HelloWorld.vue 用了 props（如下）
```HelloWorld.vue```

```js{3,10-11}
<template>
  <div class="hello">
    <h1>{{ msg }}</h1>
  </div>
</template>

<script>
export default {
  name: 'HelloWorld',
  props: {
    msg: String
  }
}
```
## 新增頁面
經過了上面的介紹，對於專案中的檔案都有些認識了。接著，我們在 myblog 中建立一個 Account 的頁面
1. 在 views 資料夾中建立 Account.vue。 <br/>
```Account.vue```
```js
<template>
  <div>
    <h1>This is an account page</h1>
  </div>
</template>

<script>
export default {
    name: 'Account'
}
</script>
```
2. 建立好頁面後，接著設定路由。 <br/>
到 index.js 中加入 account 頁面的路由規則（要記得將 Account.vue import 進 index.js 中）
```js{4,22-25}
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Account from '../views/Account.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/account',
    component: Account
  }
]

const router = new VueRouter({
  routes
})

export default router
```
3. 回到主頁面 App.vue，建立可跳轉到 Account 頁面的連結（可在第 5 行後面加上 | 隔開 About 及 Account） <br/>
```App.vue```
```js{6}
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link> |
      <router-link to="/about">About</router-link> |
      <router-link to="/account">Account</router-link>
    </div>
    <router-view/>
  </div>
</template>
```

最後在啟動 server ```yarn serve```，就可以看到首頁中```http://localhost:8080/``` 出現了 Account 的超連結， <br/>且點擊即可跳轉到我們剛剛建立的 account 頁面 ```http://localhost:8080/#/account```  <br/>

![51-02](../images/51-02.png)