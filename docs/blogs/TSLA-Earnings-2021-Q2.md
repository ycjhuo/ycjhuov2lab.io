---
title: 特斯拉（Tesla）2021 Q2 季報，打破依靠碳積分盈利的質疑
description: '特斯拉財報|TSLA 財報|特斯拉 2021 Q2 第二季財報|TSLA 2021 Q2 第二季財報'
date: 2021-07-31
tag: [Stock, TSLA]
category: Investment
---

特斯拉上季財報回顧： <br/>
[特斯拉（Tesla）2021 Q1 季報，缺席了高價車的財報依舊強勢](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q1.html)  <br/>

特斯拉從 2021 Q1 季報公佈後（04/26 盤後），到公佈 Q2 季報（07/26 盤後），三個月的時間，股價表現為：738.2 → 657.62，下跌了約 11%，這段期間內，最大跌幅為 -23.67%  <br/>


## 特斯拉 2021 Q2 財報
[來源](https://tesla-cdn.thron.com/static/ZBOUYO_TSLA_Q2_2021_Update_DJCVNJ.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22q2_2021.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4</th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">Q1 vs. Q4</th>
    <th align="center">Q2 vs. Q1</th>
  </tr>
  <tr>
    <td align="center">電動車總營收（百萬）</td>
    <td align="center">9,314</td>
    <td align="center">9,002</td>
    <td align="center">10,206</td>
    <td align="center">-3.35%</td>
    <td align="center">11.8%</td>
  </tr>
  <tr>
    <td align="center">電動車總利潤（百萬）</td>
    <td align="center">2,244</td>
    <td align="center">2,385</td>
    <td align="center">2,899</td>
    <td align="center">6.28%</td>
    <td align="center">17.73%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">24.10%</td>
    <td align="center">26.5%</td>
    <td align="center">28.4%</td>
    <td align="center">2.4%</td>
    <td align="center">1.9%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">5.4%</td>
    <td align="center">5.7%</td>
    <td align="center">11%</td>
    <td align="center">0.3%</td>
    <td align="center">5.3%</td>
  </tr>
    <tr>
    <td align="center">EPS</td>
    <td align="center">0.24</td>
    <td align="center">0.39</td>
    <td align="center">1.02</td>
    <td align="center">62.5%</td>
    <td align="center">61.76%</td>
  </tr>
</table>

- 特斯拉在 Q2 的營收及利潤，與 Q1 相比下，均達到二位數的增長（11.8% & 17.73%） <br/>
- 毛利＆營益率也是雙雙成長，其中更是首次突破個位數，實現了 11% 的目標  <br/>
- EPS 受到上面幾個因素的影響，與上季相比也成長了 61.76%，來到了 1.02  <br/>

::: tip
特斯拉 2020 全年的 EPS 僅為 0.64，僅這一季，EPS 就達成 2020 全年的 159%  <br/>

全球市值前 2 - 5 名車廠：豐田（TM），通用（GM），蔚來（NIO），福特（F） <br/>
- 毛利率皆 < 20%（特斯拉為 28.4%）
- 營益率皆 < 8%（特斯拉為 11%）
:::

下面來看看是什麼因素推動特斯拉在第二季的強勁增長

## 特斯拉 2021 Q2 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4</th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">Q1 vs. Q4</th>
    <th align="center">Q2 vs. Q1</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">16,097</td>
    <td align="center">0</td>
    <td align="center">2,340</td>
    <td align="center">-100%</td>
    <td align="center">N/A</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">163,660</td>
    <td align="center">180,338</td>
    <td align="center">204,081</td>
    <td align="center">10.19%</td>
    <td align="center">13.17%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">18,966</td>
    <td align="center">2,030</td>
    <td align="center">1,895</td>
    <td align="center">-89.3%</td>
    <td align="center">-6.65</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">161,701</td>
    <td align="center">182,847</td>
    <td align="center">199,409</td>
    <td align="center">13.08%</td>
    <td align="center">9.06%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">179,757</td>
    <td align="center">180,338</td>
    <td align="center">206,421</td>
    <td align="center">0.32%</td>
    <td align="center">14.46%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">180,997</td>
    <td align="center">184,877</td>
    <td align="center">201,304</td>
    <td align="center">2.33%</td>
    <td align="center">8.89%</td>
  </tr>
</table>

Model S Plaid 由於剛於 6/10 完成改款，剛回到生產階段，因此第二季的生產＆交付量仍低於正常水準  <br/>
但 Model 3/Y 的生產及交付量依舊保持著快速的成長，單一季度的生產量已突破 20 萬輛  <br/>

::: tip
特斯拉 2020 的 Model 3/Y 生產量約 45.5 萬輛，單這一季的Model 3/Y 生產量就已達到去年的 45%
:::

## 後記
特斯拉本季的獲利來源主要是作業流程的改善（研發＆行政費用與上季相比，分別下降 15.6% & 5.8%） <br/>

在碳積分收入（regulatory credits）這方面，與上季相比，下降了 46.33%，僅佔營業利益的 26.98% ＆ 稅前淨利的 27.38%  <br/>

是特斯拉從 2019 Q3 盈利以來，首度碳積分佔的比重最低（先前均佔 50% & 70% 以上）  <br/>

---

在最近中概股均被打壓的情況下，與其投資目前股價偏低還可能更低的中概股，不如選擇已逐漸走出低點的特斯拉  <br/>

特斯拉目前的盈利能力幾乎都建立在中階車款（Model 3 / Y），毛利率更高的 Model S 仍蓄勢待發，準備在下一季度繼續推升特斯拉的盈利能力  <br/>

![102-01](../images/102-01.png)