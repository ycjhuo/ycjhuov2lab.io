---
title: 公佈第四季及 2020 全年財報，比特幣概念股 SQ 表現如何？
description: '美股 Square SQ 2020 財報分析|比特幣概念股|Bitcoin 概念股|美股 Fintech 龍頭|PayPal vs. Square'
date: 2021-03-21 22:24:59
tag: [Stock, SQ]
category: Investment
---
Square 在 02/23 公佈了 2020 Q4 及 全年財報，雖然財報表現不差，但剛好遇上科技股修正時期。從財報發佈後，股價距離最低點約跌了 21%。而現在回升到財報發佈日的 -12.37%。

而 Square 股價是否已脫離谷底，或是還會有更大的跌幅。我們可以從這次財報中一探究竟。

## 財務指標
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
    <th align="center">Difference (Q4 - Q3 / 2020 - 2019)</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">31.6 億 vs. 30.3 億</td>
    <td align="center">95 億 vs. 47.1 億</td>
    <td align="center">3.96% / 50.37%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">25.44% vs. 26.19%</td>
    <td align="center">28.78% vs. 40.09%</td>
    <td align="center">-0.74% / -11.31%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">1.43% vs. 1.63%</td>
    <td align="center">-0.20% vs. 0.56%</td>
    <td align="center">-0.20% vs. -0.76%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">0.65 vs. 0.08</td>
    <td align="center">0.48 vs. 0.88</td>
    <td align="center">87.69% vs.-83.33%</td>
  </tr>
</table>

2020 年的 Q3 及 Q4 營收成長的很快，光這二季的營收加起來就約 62 億，約是 2019 一整年營收的 1.32 倍（2019 營收為 47.1 億）。但營收增長的同時，毛利卻沒有跟著增長，相反的，跟 2019 年的毛利率相比，2020 的毛利率下降了 11.31％。<br />

下面我們來看看營收的組成，從中分析是哪項業務拉低了毛利率。

## 收入組成
Square 的收入由下面這四個項目所組成：
<table style="width:100%">
  <tr>
    <th align="center">營收組成</th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
    <th align="center">Difference (Q4 - Q3 / 2020 - 2019)</th>
    <th align="center">Gross Margin (2020 Q4)</th>
  </tr>
  <tr>
    <td align="center">交易收入</td>
    <td align="center">29.41% vs. 30.05%</td>
    <td align="center">34.69% vs. 65.37%</td>
    <td align="center">-1.09% / -30.67%</td>
    <td align="center">42.38%</td>
  </tr>
  <tr>
    <td align="center">訂閱收入</td>
    <td align="center">14.23% vs. 14.75%</td>
    <td align="center">16.21% vs. 21.88%</td>
    <td align="center">-0.53% / -5.67%</td>
    <td align="center">85.53%</td>
  </tr>
    <tr>
    <td align="center">硬體收入</td>
    <td align="center">0.77% vs. 0.9%</td>
    <td align="center">0.97% vs. 1.79%</td>
    <td align="center">-0.13% / -0.83%</td>
    <td align="center">-47.74%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin 收入</td>
    <td align="center">55.59% vs. 53.85%</td>
    <td align="center">48.13% vs. 10.96%</td>
    <td align="center">1.74% / 37.18%</td>
    <td align="center">2.32%</td>
  </tr>
</table>

上表可看出，在營收成長上做出最大貢獻的是 Bitcoin 收入，單這項收入就佔了總收入的 55% 左右，而這項收入在 2019 僅佔了 11% 左右。
::: tip
這點也可表現出 2020 的確是比特幣大爆發的一年，在整個 2020 年，比特幣價格從 9,350 上升到 29,002，共上漲了 210%
:::
在各項收入的毛利率中，Bitcoin 收入也是毛利最低的一項業務。因此 Square 雖被視為比特幣概念股，但 Bitcoin 對於 Square 整體淨利的幫助，並沒有想像中的大。

而雖然表中的硬體收入，毛利率是負的。但硬體收入對於 Square 來說，算是載體的角色。有了這個載體， Square 才可以從每筆交易中收取手續費以及相關軟體的訂閱費。

## Seller vs. Cash App ecosystem
接著我們將 Square 分為 B2B 業務（Seller）以及 B2C 業務（Cash App），來比較哪個業務對於 Square 的獲利更為重要。
<table style="width:100%">
  <tr>
    <th align="center">營收組成</th>
    <th align="center">2020 Q4 vs. 2020 Q3</th>
    <th align="center">Full Year 2020 vs. 2019</th>
    <th align="center">Difference (Q4 - Q3 / 2020 - 2019)</th>
  </tr>
  <tr>
    <td align="center">交易收入（Seller）</td>
    <td align="center">27.14% vs. 27.84%</td>
    <td align="center">32.23% vs. 65.86%</td>
    <td align="center">-0.69% / -33.63%</td>
  </tr>
  <tr>
    <td align="center">訂閱收入（Seller）</td>
    <td align="center"> 3.33% vs. 3.08%</td>
    <td align="center"> 3.96% vs. 8.08%</td>
    <td align="center">0.25% / -4.12%</td>
  </tr>
  <tr>
    <td align="center">硬體收入（Seller）</td>
    <td align="center"> 0.77% vs. 0.90%</td>
    <td align="center"> 0.97% vs. 1.85%</td>
    <td align="center">-0.13% / -0.89%</td>
  </tr>
  <tr>
    <td align="center">交易收入（Cash App）</td>
    <td align="center"> 2.26% vs. 2.66%</td>
    <td align="center"> 2.46% vs. 1.60%</td>
    <td align="center">0.87% / -0.40%</td>
  </tr>
  <tr>
    <td align="center">訂閱收入（Cash App）</td>
    <td align="center">10.89% vs. 11.67%</td>
    <td align="center">12.25% vs. 11.30%</td>
    <td align="center">0.94% / -0.78%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin 收入（Cash App）</td>
    <td align="center">55.59% vs. 53.75%</td>
    <td align="center">48.13% vs. 11.31%</td>
    <td align="center">36.83% / 1.74%</td>
  </tr>
</table>
以收入組成來區分的話，在 2020 Q4 財報中，前三大業務分別是：<br />
1. Cash App 的 Bitcoin 收入（55.59%） 2. Seller 的交易收入（27.14%） 3. Cash App 的訂閱收入（10.89%)<br />

而在 2019 全年財報中，卻是：<br />
1. Seller 的交易收入（65.86%） 2. Cash App 的 Bitcoin 收入（11.31%） 3. Cash App 的訂閱收入（11.30%）

若將這些收入比重 x 各自的毛利率的話，最能帶動 Square 淨利的業務無疑是 Seller 的交易業務。

::: tip
Seller ecosystem 的總體毛利率從 2019 的 40.16% 成長到 43%，而 Cash App 在 2019 的毛利率約為 41.4%，2020 時約為 20.53%。
:::

## 後記
Square 的股價在 2020 年，從 $63 一路漲到了 $217.64，漲幅約為 245%（還記得 Bitcoin 的漲幅為 210% 嗎？），如此看來 Square 是個比 Bitcoin 更為良好的投資標的（在 2020 年）。<br />

而 Square 去年因為疫情的衝擊，許多餐廳，咖啡店都停止了營業，損害了 Square 的 Seller 業務成長（與 2019 年相比，Seller 的交易收入下降了 33.63%）。<br />

而我認為，這也是造成 Square 的營業利潤率及 EPS 表現都不如 2019 的原因。但隨著疫苗的施打已開始進行，政府表示五月底前，美國將會有足夠的疫苗可提供給每位成年人施打。而餐廳，零售業等也都從之前的僅能提供外帶服務，變成允許內用人數 <= 室內可容納空間的 50%。

因此，在未來幾個月的經濟復甦中，Square 將可從中獲益，靠著 Seller 業務的幫助，將獲利再往上提升。