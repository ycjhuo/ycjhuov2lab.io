---
title: 黃石公園、大提頓、總統山、魔鬼峰、瘋馬巨石、獨立岩、大鹽湖、鹽湖城市區 6 日遊
description: Yellowstone National Park 黃石國家公園|Grand Teton 大提頓|Jackson Hole 傑克遜鎮|Mount Rushmore 總統山|Crazy Horse Monument 瘋馬巨石|Devils Tower 魔鬼峰|Independent Rock 獨立岩|Salt lake City 鹽湖城
date: 2022-09-26
tag: [Yellowstone, Grand Teton, Jackson Hole, Mount Rushmore, Crazy Horse Monument, Devils Tower, Independent Rock, Salt lake City]
category: Travel
---

這次參加了從 [玩哪兒 wannar](https://www.wannar.com/salt-lake-city-yellowstone-rocky-mountains-independent-rock-6-days-tour-11911.html) 訂的黃石公園 6 天 5 夜團，這篇來為每天的行程做個紀錄，也讓有興趣的朋友可做個參考

## 行程規劃
本次行程雖說是 6 天 5 夜，但實際上第一天 ＆ 最後一天是沒有行程的，只是待在飯店，所以實際行程只有 4 天 3 夜，行程如下： 
- Day 1：從機場自行入住飯店
- Day 2：鹽湖城 (Salt lake City) → Jackson Hole → 大提頓（Grand Teton） → 黃石公園（Yellowstone）
- Day 3：黃石公園（Yellowstone）
- Day 4：魔鬼峰（Devils Tower） → 總統山（Mount Rushmore） → 瘋馬巨石（Crazy Horse Monument）
- Day 5：獨立岩（Independent Rock） → 鹽湖城市區 → 大鹽湖
- Day 6：從飯店自行到機場

下圖是這次行程的路線圖：紅色是住宿的飯店，藍色是景點，灰色則是中途停留吃飯＆上廁所的休息站

![玩哪兒 Wannar 黄石公园、大提顿、总统巨石、魔鬼峰、疯马巨石、独立岩、大盐湖、盐湖城市区 6日游](../images/194-01.png)

除了左上方的黃石公園 (Yellowstone) ＆ 右上方的總統山 (Mount Rushmore) 距離其它景點較密集外，大部分車程皆在 2 小時以上，但也不用擔心上廁所的問題，司機在每 1.5 - 2 小時都會固定停靠休息站，讓旅客可下車休息

## 交通
這次安排到的是當地的旅遊公司「海鷗旅遊」，搭乘的遊覽車共 58 個位置，而我們這次總共有 36 位團員，所以車上其實不擠 <br />
雖說是華人旅遊公司，但因提供中英文導覽，所以參加的人來自：英國、德國、印度、菲律賓等，並不是我們想像中，以住在美國當地的華人為主

![玩哪兒 Wannar 黄石公园、大提顿、总统巨石、魔鬼峰、疯马巨石、独立岩、大盐湖、盐湖城市区 6日游 遊覽車](../images/194-02.jpg)
![玩哪兒 Wannar 黄石公园、大提顿、总统巨石、魔鬼峰、疯马巨石、独立岩、大盐湖、盐湖城市区 遊覽巴士](../images/194-03.jpg)

## 費用
這次黃石公園 6 天 5 夜的行程費用如下
- 在 [玩哪兒 wannar](https://www.wannar.com/salt-lake-city-yellowstone-rocky-mountains-independent-rock-6-days-tour-11911.html) 訂的費用為 $978.4/人（刷卡）
- 行程開始後，導遊會跟收各景點的門票 $150/人（現金或轉帳）
- 行程的最後一天，則會收取導遊＆司機服務費，$12/人/天，因真正的行程只有四天，所以是共收取 $48/人（現金或轉帳）

全部加起來，總共 $1,176.4/人，扣掉住了 5 晚的飯店（每晚算 $100），其實一人一天約 $100 左右 <br />
對比自己租車，一天約 $143（含稅），雖只須租有行程的四天，，但費用是差不多的，且又不須自己開車，其實相當划算

## 後記
這次是第二次從玩哪兒這個平台訂 local tour，相比於上次疫情前的 [西雅圖瑞尼爾山一天團](https://ycjhuo.gitlab.io/blogs/Seattle-3-Days-Itinerary-Day-2.html)，這次的 36 人團，真的感覺差別很大 <br />
但好在團員都是蠻準時集合的，很少發生遲到的狀況，所以這 6 天玩下來，其實也沒遇上什麼大問題 <br />

但若不喜歡車上人多吵鬧的話，下次可以考慮訂私人小團，行程也會更加順暢
