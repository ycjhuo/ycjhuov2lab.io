---
title: 開箱！紐約曼哈頓哈利波特旗艦店（Harry Potter New York）
description: '紐約 哈利波特 介紹|曼哈頓 哈利波特 推薦|New York Harry Potter 介紹'
date: 2021-10-26
tag: [New York]
category: Travel
---

![紐約曼哈頓哈利波特旗艦店](../images/128-01.jpg)

位於曼哈頓 5 大道＆ 22 街的哈利波特旗艦店在今年的 6/3 開幕，吸引了滿滿的哈利波特迷，直到四個月後的現在，仍要排約 3 小時才可進入一探究竟 <br />

一進門就可看到鄧不利多所飼養的佛客使（Fawkes）& 哈利波特相關商品
![紐約曼哈頓哈利波特旗艦店](../images/128-02.jpg)
![紐約曼哈頓哈利波特旗艦店](../images/128-03.jpg)

## 魔杖區
有許多魔杖可供選擇，價格分為 $38 & $40 二種
![紐約曼哈頓哈利波特旗艦店](../images/128-04.jpg)
一根根的魔杖放滿整個櫃子，吸引著哈利波特迷帶它回家
![紐約曼哈頓哈利波特旗艦店](../images/128-05.jpg)
旁邊有著一個水晶球，只要握著魔杖就可以看到一段短片
![紐約曼哈頓哈利波特旗艦店](../images/128-06.jpg)

## 活米村奶油啤酒＆柏蒂口味豆
除了魔杖外，當然也有賣活米村的奶油啤酒（Butterbeer）& 柏蒂口味豆（Bertie Bott's Every Flavour Beans）<br />
奶油啤酒一瓶是 $6，一手四瓶的話是 $16，很適合送給哈利波特迷的朋友
![紐約曼哈頓哈利波特旗艦店](../images/128-07.jpg)
而在哈利波特中常出現的「柏蒂口味豆」，總共有 20 個口味，除了一些正常口味外，居然還有「嘔吐物（vomit）」口味
![紐約曼哈頓哈利波特旗艦店](../images/128-08.jpg)

## Butterbeer Bar
除了瓶裝的奶油啤酒外，當然也有現場喝的版本，喝起來就像沙士 + 奶油，所以小朋友也可以喝
![紐約曼哈頓哈利波特旗艦店](../images/128-09.jpg)
排著長長的人龍，跟星巴克買一送一時有得比
![紐約曼哈頓哈利波特旗艦店](../images/128-10.jpg)

如果你有買杯子（一個 $7）的話，喝完後可以在這邊洗完再帶走
![紐約曼哈頓哈利波特旗艦店](../images/128-11.jpg)

## 史萊哲林＆巫師袍
最後來到地下室，整體的裝潢是史萊哲林的主題，天花板上還有一條長長的蛇
![紐約曼哈頓哈利波特旗艦店](../images/128-12.jpg)
除了魔杖，巫師袍當然也不可或缺，一件為 $100 左右，旁邊也有客製化服務，可以在巫師袍繡上你的名字（+$10）<br />
手套、圍巾等配件約在 $45 左右
![紐約曼哈頓哈利波特旗艦店](../images/128-13.jpg)


以上就是紐約曼哈頓哈利波特旗艦店的介紹囉