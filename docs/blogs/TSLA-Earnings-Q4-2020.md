---
title: 特斯拉（Tesla）公布 Q4 季報，成長速度是否減弱
description: '特斯拉財報|TSLA 財報|特斯拉 2020 Q4 第四季財報|TSLA 2020 Q4 第四季財報'
date: 2021-02-01 02:04:13
tag: [Stock, TSLA]
category: Investment
---

上週三（01/27）盤後，特斯拉公佈了 2020 的第四季財報。而在公佈的後的二天股價下跌了 8.17%。與此同時以科技股為主的納斯達克（Nasdaq）指數下跌了 1.27%。

是因為財報的關係讓特斯拉的跌幅達到大盤的 6 倍嗎？下面來看看這季財報是否為造成特斯拉大跌的原因。 <br/>
::: tip
雖然一般季報的比較都是今年的 Q4 vs 上一年的 Q4。但以特斯拉這種仍處於成長期的企業來說，一年的改變太大了，且目前特斯拉的產能還屬於供不應求的階段，所以用 Q3, Q4 比較並不會不合理。
:::


## 特斯拉 Q4 vs Q3 財報
[來源](https://tesla-cdn.thron.com/static/1LRLZK_2020_Q4_Quarterly_Update_Deck_-_Searchable_LVA2GL.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22TSLA-Q4-2020-Update.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4</th>
    <th align="center">2020 Q3</th>
    <th align="center">差距</th>
  </tr>
  <tr>
    <td align="center">電動車總營收（百萬）</td>
    <td align="center">9,314</td>
    <td align="center">7,611</td>
    <td align="center">+22.38%</td>
  </tr>
  <tr>
    <td align="center">電動車總利潤（百萬）</td>
    <td align="center">2,244</td>
    <td align="center">2,105</td>
    <td align="center">+6.6%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">24.09%</td>
    <td align="center">27.66%</td>
    <td align="center">-3.57%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">5.35%</td>
    <td align="center">9.22%</td>
    <td align="center">-3.87%</td>
  </tr>
    <tr>
    <td align="center">EPS</td>
    <td align="center">0.24</td>
    <td align="center">0.27</td>
    <td align="center">-0.03%</td>
  </tr>
</table>

從上表可看出，Q4 營收相比上季成長了 22%（Q3 與 Q2 相比成長了 47%），但利潤卻只成長 6.6%（Q3 與 Q2 相比成長了 60%），顯示了特斯拉造車的毛利率不如 Q3。 <br/>
接著看了營業利益率的下降程度跟毛利率差不多，表示營業費用應該沒有成長太多。 <br/>
這樣來說，特斯拉的毛利降低原因，是因為車款降價所導致？或是平民車款的 Model 3 佔總銷售比重比較大影響的呢？ <br/> <br/>
為了找出原因，我們來觀察 Q4 及 Q3 的生產/交付量

## 特斯拉 Q4 vs Q3 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020 Q4</th>
    <th align="center">2020 Q3</th>
    <th align="center">差距</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">16,097</td>
    <td align="center">16,992</td>
    <td align="center">-5.27%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">163,660</td>
    <td align="center">128,044</td>
    <td align="center">+27.82%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">18,966</td>
    <td align="center">16,992</td>
    <td align="center">+24.16%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">161,701</td>
    <td align="center">124,318</td>
    <td align="center">+30.07%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">179.757</td>
    <td align="center">145,036</td>
    <td align="center">23.94%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">180,997</td>
    <td align="center">139,593</td>
    <td align="center">29.72%</td>
  </tr>
</table>

上表顯示，Q4 的特斯拉。除了 Model S/X 的生產量有小幅衰退外，其餘數值皆有 25-30% 左右的提升。 <br/>
在 Q4 中，Model 3/Y 的交付量佔了為總交付量的 89.34%。而在 Q3 時，這個數值為 89.06%。 <br/>
表示 Q4 中的毛利率下滑 3.57% 的原因並不是因為 Model 3/Y 的交付比重大幅提升。我們可以將原因以較大的機率歸類在車款降價所導致。

## 後記
特斯拉在去年的 10/21 公佈 Q3 財報後，股價到現在上漲了 87.76%。很大的原因是因為特斯拉入選了 S&P 500。 <br/>

P.S: 特斯拉在 11/16 宣布將於 12/21 被納入 S&P 500 後，股價上升了 94.45%。這樣看起來去年在公佈 Q3 財報後反而小跌了 8%，接著才在消息宣布後開始飆升。若從 12/21 被納入 S&P 500 後開始算，到現在的漲幅約為 22% <br/>

延伸閱讀：[特斯拉（Tesla）公布 Q3 季報，繳出了史上最好的成績單](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-Q3-2020.html) 、[特斯拉入選 S&P 500 指數後，股價還能再漲多少？](https://ycjhuo.gitlab.io/blogs/TESLA-Set-To-Join-SP500.html) <br/> <br/>

因為股價漲幅太快，使大家的期待越來越高，因此在公佈財報後，股價會有一小段的下跌似乎是趨勢。若以管理階層說的下一季度生產/交付量將會再成長 50% 來估算的話，本季總交付量對比 Q3 約成長 30%，EPS 為 0.24，那麼下季 EPS 將可能來到 0.4（特斯拉在 2020 年總 EPS 為 0.64），雖說特斯拉一直都不是一家靠 EPS 驅動股價的公司。 <br/>

特斯拉在 2020 年達成了 EPS 為正的成績，接下來特斯拉將會轉變成一家真正有盈利的公司。 <br/>
雖然目前營業利益率只有 6.3%（2020 全年），且短期內自動駕駛及能源業務在營收方面的助力也不會有賣車的成長快，但隨著柏林及德州工廠正在如火如荼的建設中（這二個工廠都是以生產 Model Y 為主）。特斯拉的利潤率仍有機會在今年往上成長。

整體而言，本次特斯拉的財報表現還是不錯（除了毛利率小幅下降），但我認為在其他車款的產量慢慢上來後，將會連帶的提升整體的毛利率（何況還有自動駕駛這種邊際效應幾乎為零的潛力業務）。 <br/>

在上週五，股價跌破 $800 時打算入場的，可惜因為券商戶頭的資金不夠（只夠買 1 股），等星期一 ACH 到帳後，股價開盤直接就到 $810，而截至週二收盤已經到了 $872.79（二天漲了快 10%）。若有機會回到 $800 左右，則會繼續加碼。