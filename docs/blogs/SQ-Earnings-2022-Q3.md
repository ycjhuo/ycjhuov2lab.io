---
title: Block（SQ）2022 Q3 財報，股價崩跌 74% 後迎來曙光
description: 'Block SQ 2022 Q3 第三季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報|Block 收購 Afterpay|Block 收購先買後付平台|Block buy now pay later” (BNPL)'
date: 2022-11-13
tag: [Stock, SQ]
category: Investment
---
Block（股票代號：SQ），從 Q2 - Q3 財報發布期間（8/4 → 11/3），股價下跌了 40%（$89.7 → $53.9），同期間 S&P 500 指數下跌 10% <br />

這段期間，Block 的股價最低跌到了 $51.5，今年以來的報酬率為 -61%，若從去年的高點（$275）來算，股價表現達到 -74% <br />

受益於 CPI（消費者物價指數）下降的影響，這二天 Block 股價飆漲了 25% <br />
這是否代表著 Block 股價將扭轉跌勢，讓我們從 Block 第三季財報瞭解 Block 最新狀況 <br />

## Block 財務指標
[來源](https://investors.block.xyz/financials/quarterly-results/default.aspx)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">39.6 億</td>
    <td align="center">44 億</td>
    <td align="center">45.2 億</td>
    <td align="center">11.21%</td>
    <td align="center">2.52%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">32.70%</td>
    <td align="center">33.37%</td>
    <td align="center">32.70%</td>
    <td align="center">0.67%</td>
    <td align="center">-0.67%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-5.73%</td>
    <td align="center">-4.85%</td>
    <td align="center">-1.08%</td>
    <td align="center">0.87%</td>
    <td align="center">3.77%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">-0.38</td>
    <td align="center">-0.36</td>
    <td align="center">-0.02</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：較 Q2 小幅成長 2.5%，來到 45.2 億；各項業務均與 Q2 差異不大，總營收仍是以「比特幣」（佔 39%）＆「交易」（佔 34%）這二個業務為主力
- 毛利率：小幅上升至 34.7%，目前已連續三季保持在 32% 以上，在收購了 AfterPay 後，毛利率顯著提升了 3% - 6%
- 營業利潤率：因銷售費用較上季降低了 8.5%，將本季營利率提升至 -1.1%，在下個季度很有可能轉正
- EPS：因營利率提升的關係，將本季的 EPS 由上季的 -0.36 大幅縮減至  -0.02，下個季度將能轉虧為盈

本季因收購 AfterPay 後所帶來的服務收入增長＆銷售支出的縮減，讓營業利潤率提升到 -1.08%，以這個趨勢， Block 將在下個季度實現盈利 <br />

可惜的是，隨著疫情的開放，「交易」業務並未實現大幅增長，而隨著比特幣的價格下跌，加密貨幣的熱潮衰退下，「比特幣」業務所帶來的營收也開始下滑 <br />

以上二種因素，嚴重打擊了 Block 的增長速度，使得 Block 在成長股中，不再是個最佳選擇

## Block 財報重點
Block 的毛利年增率達到 38%（15.7 億），排除 AfterPay 後，年增率則是 25%（14.2 億），其中：
- Square 的毛利年增率為 29%（7.8 億），排除 AfterPay 後為 17%
- Cash App 的毛利年增率為 51%（7.7 億），排除 AfterPay 後為 37%

![197-01](../images/197-01.png)

而 AfterPay 在本季貢獻了 2.1 億的營收（佔總營收的 4.7%）以及 1.5 億的毛利（佔總毛利 9.6%）<br />

Block 在 2022 Q1 花費 290 億收購的 AfterPay，的確成功增強了 Block 的獲利能力，也連帶的拉高了 Block 的毛利率


## 後記
Block 從 2021 Q3 開始，就一直處於虧損狀態，而現在總算有希望在下一季開始轉虧為盈，再加上近期股價也開始轉跌為升了，
對投資者來說無疑是個好消息 <br />

畢竟 Block 從去年的高點（$275）到現在，股價已經跌了 -74% 了，要漲回去，可是需要 380% 的漲幅才夠 <br />

Block 雖說毛利的年增率均有約 40% 的水準，但相較其它成長股，還是略遜一籌 <br />
排除產業因素不談，我的另一隻重倉股 TSLA (特斯拉)，毛利的年增率也在 40% 左右，但稅前利潤的年增率可是到了 84% <br />
且二者的市值是 400億 vs. 6000億，相差了 15 倍 <br />

因此，若資金有限的話，我偏向於投資特斯拉，而非 Block

::: warning
點擊觀看更多： <br />
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
