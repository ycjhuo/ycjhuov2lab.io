---
title: 特斯拉（TSLA）發布 2022 Q3 生產及交付量，股價已被腰斬
description: '美股特斯拉|特斯拉 TSLA|特斯拉生產交車量 2022 Q3|特斯拉 Model 3 生產交車量 2022 Q3|特斯拉 Model Y 生產交車量 2022 Q3|特斯拉 拆股 2022'
date: 2022-10-17
tag: [Stock, TSLA]
category: Investment
---

特斯拉在 10/02（日）公佈了 2022 第三季的生產 & 交車數量  <br/>
在 Q2 財報發布 → 公佈 2022 Q3 交車數量期間（07/02 - 10/02），特斯拉股價微升了 0.8%，而 S&P 500 指數卻下跌了 9.45%  <br/>
乍看之下，特斯拉股價又再次跑贏大盤？

但在特斯拉公布生產 & 交車量後的第一個星期（10/03 - 10/07），股價卻跌了 12.3%（S&P 500 指數為 -1.05%）  <br/>
顯示了特斯拉在發布交車量前，股價表現是優於大盤的，但在發布後，股價卻大幅跑輸大盤  <br/>

而公布後的第二個星期，也就是這星期（10/10 - 10/14）特斯拉股價又跌了 8.36%（S&P 500 為 -1.05%），這二個星期，特斯拉股價總共下跌了 15%


如此驚人的跌幅是因為交車量大幅下滑，或是受到市場的影響呢？來看看特斯拉 2022 第三季的生產/交付量，是否為造成大跌的關鍵

## 特斯拉 2022 Q3 生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
    <th align="center">2022 Q2 vs. 2022 Q1</th>
    <th align="center">2022 Q3 vs. 2022 Q2</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">16,411</td>
    <td align="center">19,935</td>
    <td align="center">8.46%</td>
    <td align="center">15.42%</td>
    <td align="center">21.47%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">232,169</td>
    <td align="center">345,988</td>
    <td align="center">-0.53%</td>
    <td align="center">-16.83%</td>
    <td align="center">42.87%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">16,162</td>
    <td align="center">18,672</td>
    <td align="center">25.14%</td>
    <td align="center">9.77%</td>
    <td align="center">15.53%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">238,533</td>
    <td align="center">325,158</td>
    <td align="center">-0.53%</td>
    <td align="center">-19.23%</td>
    <td align="center">36.32%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">248,580</td>
    <td align="center">365,923</td>
    <td align="center">-0.14%</td>
    <td align="center">-15.33%</td>
    <td align="center">41.51%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">254,695</td>
    <td align="center">343,830</td>
    <td align="center">0.45%</td>
    <td align="center">-17.85%</td>
    <td align="center">35%</td>
  </tr>
</table>

Q3 生產/交付量：

- Model S/X：生產量約 2 萬台，與 Q2 相比，成長了 21.5%（上季的成長率約 15%） <br/>
而交付量挾著上季的成長趨勢，目前已連續二季成長率 > 15%

- Model 3/Y：生產量約 34.6 萬輛，交付量約 32.5 萬輛，前三季合計的生產量約 88 萬台，已約略等於 2021 整年度的交車量（2021 全年交車數為 90.6 萬）  <br/>
而成長率也擺脫了連續二季度的衰退（-0.5% & -17%），本季的生產/交付量與上季相比，大幅成長了 43% & 36%

## 特斯拉生產量重回高速成長
德國柏林廠＆美國德州廠雖早在前二個季度就已進入生產狀態，但因疫情＆供應鏈影響，遲遲無法為 Model Y 的生產量提供助力  <br/>
不過這個因素已在本季消失，也造就了本季 Model 3/Y 生產量比 Q2 飆升了 43% 的佳績

但本季生產量大幅增加的另一原因則是：在上季因部分零件短缺，很多組裝中的車輛差了最後一步，無法順利完成組裝並交貨  <br/>
而這些囤積的車，在本季因關鍵零組件在本季到貨後，即可馬上組裝完成  <br/>

因此，在 Q4 時，Model 3/Y 的生產量不一定能維持 43% 的成長速度

## 後記
特斯拉目前的股價已被腰斬，從一年前（2021/11/5）的歷史高點 $407.36，到上週五（2021/10/14）的收盤價 $204.99  <br/>
短短 1 年內，股價跌幅來到了 50%，而要想漲回到一年前的股價，則須上漲 100%  <br/>

如此巨大的跌幅，讓人聯想到是否為馬斯克為了要購買推特而再次賣股籌錢  <br/>
可參考 [為買推特賣股？馬斯克賣股如何影響特斯拉股價](https://ycjhuo.gitlab.io/blogs/TSLA-Drop-By-Twitter.html)  <br/>

但查了 SEC Form 4 後，這二週（10/3 - 10/14）賣股的僅有特斯拉的財務長（CFO），以平均 $250.5 的價格，賣出 3,750 股  <br/>
這總額約 94 萬的股票並不足以讓特斯拉在這二週就跌了 15%，因此可推斷跌幅多半是因為投資人對於聯準會在未來會繼續加息而引發的疑慮  <br/>

在即將迎來的第三季財報，由於有著大幅成長的交車數加持，特斯拉的財報表現應會優於預期  <br/>
我個人認為有助於扭轉特斯拉股價往下的趨勢，同時在星期三財報公布前，也是個合適的加碼點  <br/>

::: tip
聯準會從今年三月以來，已將基準利率從 0.25%　升到　3.25%，共 12 碼
:::

::: warning
點擊觀看更多：  <br/>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
