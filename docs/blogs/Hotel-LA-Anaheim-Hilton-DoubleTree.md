---
title: 開箱！洛杉磯-希爾頓 Anaheim DoubleTree 飯店（DoubleTree Suites by Hilton Hotel Anaheim Resort - Convention Center）
description: 'LA Disney 迪士尼飯店推薦|LA Disney 迪士尼飯店介紹|LA Disney 迪士尼 飯店 希爾頓|LA Disney 迪士尼 飯店評價|LA Disney 迪士尼 飯店 CP 值'
date: 2021-09-29
tag: [Hotel, Hilton, Los Angeles]
category: Travel
---
::: tip
入住時間：2021/09/26 - 09/28 <br />
房型：1 King Bed Standand Room（但入住後升級成 2 QUEEN 1 BEDROOM SUITE SOFABED W/TUB）<br />
每晚價格（稅後）：$133.64 / 33,000 點 <br />
Google 評價：4.3 顆星（1,356 評價）
:::

結束了在 LA Downtown 的三天二夜後，我們接著移動到 LA 的 Anaheim，這邊算是 LA 郊區，附近景點並不多，唯一的景點就是迪士尼樂園（Disneyland park），而這間飯店距離迪士尼樂園走路僅要 15 分鐘，下面就來看看 DoubleTree Suites by Hilton Hotel Anaheim Resort - Convention Center 的介紹吧

## 飯店外觀＆大廳
Hilton DoubleTree Anaheim LA 並不高，總樓層只有七樓，但舒適度來說，我認為是樂勝 Downtown 的 DoubleTree
有興趣的可參考這篇：[開箱！洛杉磯希爾頓 DoubleTree 飯店（DoubleTree by Hilton Hotel Los Angeles Downtown）](https://ycjhuo.gitlab.io/blogs/Hotel-LA-Downtown-Hilton-DoubleTree.html)

![Hilton DoubleTree Anaheim LA 外觀](../images/116-01.jpg)

大廳很大，仔細看的話，還能發現左後方有隻米奇（上面有迪士尼樂園的資訊），而右邊則是 Check in 櫃檯
![Hilton DoubleTree Anaheim LA 大廳](../images/116-02.jpg)
![Hilton DoubleTree Anaheim LA Check in 櫃檯](../images/116-03.jpg)

## 房間
可能是淡季的關係，很幸運地被升級到 Suite 房型，且房間內還有冰箱及微波爐
![Hilton DoubleTree Anaheim LA Suite 房型](../images/116-04.jpg)
房間為 2 Queen bed 房型
![Hilton DoubleTree Anaheim LA 房間](../images/116-06.jpg)
除了客廳有電視外，房間內也有電視，可惜的是只能看固定的頻道，無法觀看 Netflix 及 Youtube 等
![Hilton DoubleTree Anaheim LA 房間](../images/116-07.jpg)

## 浴室
房間左邊則是化妝間，化妝間的左邊是廁所，右邊連接著客廳
![Hilton DoubleTree Anaheim LA 浴室](../images/116-08.jpg)

浴室有配備浴缸，牆壁上還有扶手方便起身，盥洗用具則是提供知名護膚品牌瑰柏翠（Crabtree & Evelyn）
![Hilton DoubleTree Anaheim LA 浴室](../images/116-09.jpg)
![Hilton DoubleTree Downtown LA 盥洗用具](../images/112-06.jpg)

## 早餐
因疫情關係，目前美國境內的 Hilton 都不提供早餐 <br />
替代方案則是一天給 $24 credit（黃金會員），可用在飯店內的 咖啡店 ＆ Lounge Bar <br />
咖啡店可選擇的品項很多，除了星巴克外，也有星冰樂可以選擇 <br />
![Hilton DoubleTree Anaheim LA Cafe](../images/116-10.jpg)

下面是我其中一天用 $24 credit 換的早餐（想不到要換什麼，就用 $5 換了水）
![Hilton DoubleTree Anaheim LA 早餐](../images/116-11.jpg)

咖啡店的菜單如下
![Hilton DoubleTree Anaheim LA Menu](../images/116-12.jpg)

## 後記
這間飯店整體還算不錯，因為鄰近迪士尼樂園，附近都是其他飯店，隔壁是茹絲葵牛排（Ruth's Chris Steakhouse），附近治安相比 LA Downtown 上升很多，雖然晚上還是有零星幾個流浪漢，但至少沒有像 Downtown 一樣整條街都被帳篷佔據

我們在迪士尼樂園玩到晚上 10:30 才走回飯店，路上因為也有其他遊客，所以這麼晚，走了 15 分鐘還是蠻安全的 <br />
以 33k 點數（現金含稅價 $133.64）來說其實蠻划算的（2021/09/26），而且還意外的被升級到 Suite 房型，下次若再來迪士尼樂園仍會選擇這家飯店 <br />


點擊 [飯店開箱](https://ycjhuo.gitlab.io/tag/Hotel/)，看更多飯店開箱 <br />
或 [Los Angeles](https://ycjhuo.gitlab.io/tag/Los%20Angeles/)，看 Los Angeles 景點介紹 <br />
