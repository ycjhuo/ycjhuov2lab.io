---
title: 如何知道 Table 內有幾個欄位及欄位資訊（MSSQL）
description: '查 Table 欄位 | MSSQL 欄位資料 | MSSQL 幾個欄位 | Table 有幾個欄位'
date: 2021-04-01 23:00:11
tag: [Python, MSSQL]
category: Programming
---

有時候資料庫的 Table 內會有很多個欄位，要如何知道 Table 內有多少個欄位呢？<br />

要是欄位很多的話，一個個數的話，不僅容易算錯，很也費時間 <br />

這時，就能利用下面的 script 直接知道 Table 內有多少個欄位

## 找出 Table 中有幾個欄位

```sql
SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
WHERE table_catalog = 'WRCT' -- Database name
AND table_name = 'FFCDB_From201907' -- Table name
```

![74-01](../images/74-01.png)<br/>

這個範例中， WRCT 是資料庫的名稱，而 FFCDB_From201907 為 Table 名稱，我們就可得知，在 這個 Table 內，共有 136 個欄位 <br />

## 找出 Table 中個欄位的資訊
而在得出 Table 中有幾個欄位後，若我們想知道每個欄位的資訊的話呢？可用這個 script 來取得：<br />
（這裡要注意：須在該 Table 的 DB 中執行才可以得到結果）

```sql
SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'FFCDB_From201907'
```

![74-02](../images/74-02.png)<br/>

若我們只需要欄位名稱，而不需要其他資訊的話，則只要將 Select 後面 的 * 改為 COLUMN_NAME 即可

```sql
SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'FFCDB_From201907'
```

![74-03](../images/74-03.png)<br/>
