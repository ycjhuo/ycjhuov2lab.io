---
title: Meta 2022 Q3 財報，股價已從高點崩跌 76%
description: 'Meta FB 2022 Q3 第三季財報｜Facebook FB 2022 Q3 第三季財報｜'
date: 2022-11-06
tag: [Stock, Meta]
category: Investment
---

社交龍頭 Meta 在 10/26 公佈了 2022 第三季的財報，在 Q2 財報 - Q3 財報之間（7/27 - 10/26），三個月的時間， Meta 股價從 $169.58 → $129.82，表現為 -23.45%（同時期 S&P 500 為 -4.8%）

而在財報公布後一星期的現在，股價更是繼續下跌了 30%，來到 $90.79

在已經跌了這麼多的現在，是否能進場加碼了呢？下面就來看看 Meta 2022 Q3 財報

## Meta 2022 Q3 財報
[資料來源](https://s21.q4cdn.com/399680738/files/doc_financials/2021/q3/FB-09.30.2021-Exhibit-99.1.pdf)
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">27,908</td>
    <td align="center">28,822</td>
    <td align="center">27,714</td>
    <td align="center">3.28%</td>
    <td align="center">-3.84%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">8,908</td>
    <td align="center">8,186</td>
    <td align="center">5,576</td>
    <td align="center">-8.11%</td>
    <td align="center">-31.88%</td>
  </tr>
  <tr>
    <td align="center">廣告營利率</td>
    <td align="center">42.2%</td>
    <td align="center">39.35%</td>
    <td align="center">34.04%</td>
    <td align="center">-2.85%</td>
    <td align="center">-5.31%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">30.54%</td>
    <td align="center">29%</td>
    <td align="center">20.44%</td>
    <td align="center">-1.54%</td>
    <td align="center">-8.56%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">2.74</td>
    <td align="center">2.47</td>
    <td align="center">1.64</td>
    <td align="center">-9.85%</td>
    <td align="center">-33.6%</td>
  </tr>
</table>

- 營收：為 277 億，季增率 -4%，與去年同期相比，也是相差 -4% <br />
目前全力發展的虛擬實境業務（Reality Labs）營收為 2.85 億，僅佔了總營收 1%（約 2.85 億），但從 2022 Q1 開始，每季營收約穩定衰退 35%（Q1 營收約 6.95 億）
- 稅前淨利：約 55.8 億，季增率 -32%，與去年同期相比，則是 -47%，主因為虛擬實境業務的虧損（36.7 億）較上季增加了 31%
- 廣告營利率：為 34%，自 Q1 開始就穩定下降，表示在排除虛擬實境業務後，廣告業務也在 iOS 大改隱私權的設定後，難以維持往日的榮景
- 營業利益率：為 20.44%，與 Q1 的 30.5% 相比，已衰退了 10%，若排除虛擬實境業務後，則以行政費用增幅最高（13%）
- EPS：為 1.64，較上季衰退約 34%，較去年同期衰退 49%

Meta 的營收在 2021 Q4 達到最高點後，就逐漸下降（337 億 → 277 億），至今已衰退了 17%，但支出卻逐漸升高 <br />
連帶影響淨利衰退了 57%，降至約 44 億，EPS 則衰退了 55%

![Meta 2022 Q3 營收](../images/196-01.png)

## Meta 財報重點
- 用戶數：Facebook 的每日活躍用戶（daily active users, DAUs）及旗下的 Instagram ＆ What's app 的每日使用者人數仍保持小幅上升 <br />
目前 Facebook 每日使用人數為 19.8 億，加計 Instagram ＆ What's app 則增加到 29.3 億

![Meta 2022 Q3 使用者活躍數](../images/196-02.png)
![Meta 2022 Q3 每位使用者價值](../images/196-03.png)

- 廣告業務：廣告的顯示次數比去年增加了 17%，但每次顯示廣告的價格卻較去年下降了 18%
- 庫藏股：本季投入了 65.5 億買回庫藏股，而今年至今已投入了 211 億買回庫藏股，並還有 178 億的額度未使用
目前 Meta 手上的約當現金仍有 417.8 億（長期債務為 99.2 億）


CFO 對 Q4 的預期：總營收將落在 300-325 億（假設匯率不變），Q4 總花費則會在 230-250 億 <br />
若與本季相比，營收在 Q4 約成長 8-17%，花費則會成長 4-13%


## 後記
Meta 距離 2021/09 的高點（$378.69），股價已崩跌了 76%，而本益比也從當初的 26 到現在的 8.7，但由於營收＆EPS 無法維持現在水準，因此本益比也就喪失了評斷是否買入的意義

財報預期在明年度 2023，總花費會比今年再增加 13%，落在 960-1010 億，主要會集中在基礎設施＆虛擬實境業務（Reality Labs）的虧損上

這些現象可看出，在蘋果調整 iOS 的隱私權政策後，使 Facebook 難以像過往一樣投放精準的廣告給用戶，在主力業務受到不可避免的限制後，Meta 只好轉而切入「元宇宙」，試圖將「硬體」＆「軟體」都掌握在自己手中

- 但因虛擬實境的遊戲體驗＆流暢度皆極度倚賴硬體設施，因此 Meta 只能繼續加大自己在基礎設施上的投資，這點在 Meta 的今年資本支出比去年多了 66.5%，來到了 228 萬億即可看出

- 而軟體方面，在缺乏殺手級應用＆尚未實現普及前，也只能繼續埋首發展，而為了減緩燒錢的速度，Meta 也暫停了員工的招募，力求將員工數控制在目前的數量（Meta 目前總員工數為 8.7 萬人，較去年成長 28%）

值得注意的是，Meta 的自由現金流（Free Cash Flow）在本季也來到最低點的 1.7 億，僅是以前的 2%

現階段投資 Meta 已從之前的穩健成長股，轉變為對於「元宇宙」有期待的「本夢比」股了，雖已帶給我不小的虧損，但仍不得不佩服 Zuckerberg 冒著巨大的虧損＆眾人的不諒解，仍致力積極將公司轉型，不讓公司在被蘋果的限制中，逐漸走向衰退

::: warning
點擊觀看更多：
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::