---
title: 紐約必去 Outlet，如何從紐約搭乘大眾運輸到 Woodbury Outlets
description: '紐約景點｜紐約購物｜紐約 Outlet｜紐約必去景點｜New York 購物｜New York Outlet'
date: 2023-01-19
tag: [New York]
category: Travel
---

Woodbury Outlets 是許多來紐約玩的人必去的景點之一，裡面共有超過 220 家的商店，同時也是世界上最大的 Outlet 之一

從紐約曼哈頓到 Woodbury Outlets 若自行開車的話，約 80 分鐘車程 <br />
但若是來觀光，身上沒有駕照、或是不想租車的話，其實也可選擇搭乘大眾交通工具 Coach USA <br />
這篇就來介紹如何搭乘 Coach USA 從紐約曼哈頓到 Woodbury Outlets 以及 Woodbury Outlets 的省錢技巧

## Woodbury Outlets Coach USA 購票
購買 Coach USA 車票有二種方法：
1. 到 [Groupon Woodbury Outlets](https://www.groupon.com/deals/shortline-coach-usa-2) 購買，來回車票為 $40/人（常出現更低的價格）
![Groupon Woodbury New York Outlets Coach USA](../images/206-01.jpg)

2. 到 [Coach USA 官網](https://www.coachusa.com/) 購買，來回車票為 $35.4/人（也可購買單程）
![Coach USA New York Woodbury Outlets](../images/206-02.jpg)


這二種購票方式，搭的都是同一種車，但 Groupon 常常會有促銷活動，很容易出現比 Coach USA 官網的價格還低的現象 <br />
在 Coach USA 官網 / Groupon 買的票都是「購買日的 365 天內」可使用，所以不須擔心要搭乘的日期 & 時間 <br />

## Woodbury Outlets Coach USA 搭乘
那買完票後，該如何使用，且要去哪邊搭乘呢？
1. 到 Port Authority Bus Terminal (625 8th Ave, New York, NY 10018) 的一樓，一進去後走到底，可看到下圖的換票處，找到 Coach USA 的櫃檯，將我們從 Groupon 上買的票給櫃檯看，她就會給我們紙本的票了

::: tip
這步驟只有在 Groupon 買票的需要做，在 Coach USA 官網買票不需要換票
:::
![Groupon Woodbury Outlets New York Coach USA 換票](../images/206-03.jpg)

2. 接著往入口處往回走一點，搭乘手扶梯到 4F，Coach USA 的發車位置在 401 - 407 不等，到了那邊會有工作人員指揮我們該去哪個窗口搭車，上車時，票務人員會檢查票：
- 在 Groupon 購買的人，出示剛剛換到的紙本車票
- 在 Coach USA 官網購買的人，出示 App / email 裡面的 QR code 即可

發車時間，最早是 8:30 發車，接著 9:20、10:00、10:30 都有車，但其實發車時間僅供參考，車不會準時在那個時間點來 <br />
所以到了等車地點後，直接排隊，等到車來，工作人員即會安排我們上車（通常排最多人的那條就是去 Woodbury Outlets 的） <br />
曼哈頓到 Woodbury Outlets，車程約 70 分鐘，但因 Woodbury Outlets 裡面的店都是 11:00 才開門，因此建議 9:00 左右到 Port Authority 搭車即可 <br />

![Woodbury Outlets Coach USA 搭車](../images/206-04.jpg)

3. 到達後 Woodbury 後，下車的地點也是我們回程的等車地點，回程同樣沒有固定的時間，因此買完後，再到一樣的地方排隊即可（建議 18:00 左右即可過去等車）<br />

![Woodbury Outlets Coach USA 下車 等車 地點](../images/206-05.jpg)

## Woodbury Outlets 後記
Woodbury Outlets 佔地遼闊，商店數也很多，為了方便消費者找到想逛的商店，Outlet 中的商店用屋頂顏色分為四個區域（如下圖）<br />
一到達 Woodbury Outlets 後，我們可以到 Market Hall 裡面領取 Outlet 紙本地圖（在廁所旁邊）<br />
地圖背面有各個商店的號碼，方便我們對照地圖正面來找，而地圖的下方也會有特定商店的折價券可以用 <br />
最常見的是 Columbia 品牌的買 $100 折 $20 <br />

![Woodbury Outlets map 地圖](../images/206-06.jpg)
![Woodbury Outlets](../images/206-07.jpg)
![Woodbury Outlets](../images/206-08.jpg)


而若是我們想購物的商店，在紙本地圖上沒有折價券的話，也可用手機下載「SIMON - MALLLs, Mills ＆ Outlet」 這個 App <br />
裡面我們可以針對自己喜歡的品牌，看看有沒有折價券可以用，若有的話，只要在結帳時給店員看手機的螢幕截圖即可


::: warning
點擊觀看更多： <br />
- [紐約必去景點 & 必辦證件](https://ycjhuo.gitlab.io/tag/20York/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
