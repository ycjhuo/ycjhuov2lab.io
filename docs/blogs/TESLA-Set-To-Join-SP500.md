---
title: 特斯拉入選 S&P 500 指數後，股價還能再漲多少？
description: '特斯拉 標普|TSLA S&P500|特斯拉 指數|特斯拉 大盤|TSLA S&P500|TSLA Index'
date: 2020-11-22 17:34:20
tag: [Stock, TSLA]
category: Investment
---

標普指數公司（S&P Dow Jones Indices）於上週一（ 11/16 ）盤後，宣布了特斯拉將於 12/21 被納入 S&P 500 的消息。隔天開盤後，特斯拉股價直接拉升 12.8%，而截至上週五 11/20 收盤，短短四天，特斯拉股價共上漲了 19.98%。同期間以科技股為主的納斯達克指數（Nasdaq Index）表現為 -0.63%。
<table class="center">
  <tr>
    <th align="center">時間</th>
    <th align="center">股價</th>
    <th align="center">市值</th>
  </tr>
  <tr>
    <td align="center">11/16（一） 消息公佈前收盤價</td>
    <td align="center">$408.09</td>
    <td align="center">3868.29億</td>
  </tr>
  <tr>
    <td align="center">11/20（五） 截至上週五收盤價</td>
    <td align="center">$489.61</td>
    <td align="center">4641.02億</td>
  </tr>
</table>

對比九月時特斯拉落選 S&P 500 那週的表現，入選 S&P 500 的消息讓特斯拉直接收復了當時所跌掉的市值。
::: tip
本週（09/08 - 09/11）特斯拉（TSLA）表現為 -10.9％，科技股為主的納斯達克指數（Nasdaq Index）則是 -4.04%。
:::
取自：[未如預期入選 S&P 500 指數成分股的特斯拉（TSLA）](https://ycjhuo.gitlab.io/blogs/TESLA-Left-Out-Of-The-SP500.html)

### 特斯拉在 S&P 500 中的權重
特斯拉在週一收盤時的市值（3868.29 億）約等於在 S&P 500 中，權重排名第八的嬌生（Johnson & Johnson 目前市值 3852.99 億）。而目前嬌生在 S&P 500 中的權重為 1.3%。但特斯拉的市值隨著上週的上漲，已來到 4641.02 億，若維持這個市值加入到 S&P 500，權重約為 1.42%。

<table class="center">
  <tr>
    <th align="center">排名</th>
    <th align="center">公司（Company）</th>
    <th align="center">股票代號（Symbol）</th>
    <th align="center">市值（Market Cap）</th>
    <th align="center">權重（Weight）</th>
  </tr>
  <tr>
    <td align="center">1</td>
    <td align="center">蘋果（Apple Inc.）</td>
    <td align="center">AAPL</td>
    <td align="center">1.99兆</td>
    <td align="center">6.42%</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td align="center">微軟（Microsoft Corporation）</td>
    <td align="center">MSFT</td>
    <td align="center">1.59兆</td>
    <td align="center">5.41%</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td align="center">亞馬遜（Amazon.com Inc）</td>
    <td align="center">AMZN</td>
    <td align="center">1.56兆</td>
    <td align="center">4.47%</td>
  </tr>
  <tr>
    <td align="center">4</td>
    <td align="center">Facebook（Facebook Inc.）</td>
    <td align="center">FB</td>
    <td align="center">7681.84億</td>
    <td align="center">2.21%</td>
  </tr>
  <tr>
    <td align="center">5</td>
    <td align="center">Google 母公司（Alphabet Inc Class A）</td>
    <td align="center">GOOGL</td>
    <td align="center">1.18兆</td>
    <td align="center">1.78%</td>
  </tr>
  <tr>
    <td align="center">6</td>
    <td align="center">Google 母公司（Alphabet Inc Class C）</td>
    <td align="center">GOOG</td>
    <td align="center">1.18兆</td>
    <td align="center">1.74%</td>
  </tr>
  <tr>
    <td align="center">7</td>
    <td align="center">波克夏（Berkshire Hathaway Inc. Class B）</td>
    <td align="center">BRK.B</td>
    <td align="center">5329.27億</td>
    <td align="center">1.53%</td>
  </tr>
  <tr>
    <td align="center">8</td>
    <td align="center">嬌生（Johnson & Johnson）</td>
    <td align="center">JNJ</td>
    <td align="center">3852.99億</td>
    <td align="center">1.3%</td>
  </tr>
</table>

### 加入 S&P 500 後的表現
在 09/04 時，特斯拉並沒成功被納入 S&P 500，而當時有三家公司獲選進入 S&P 500。分別是：ETSY（手做品電商公司，代號：ETSY），Teradyne（半導體測試設備製造商，代號：TER） 及 Catalent（製藥公司，代號：CTLT）。

這三家剛進入 S&P 500 指數的公司，市值都相差無幾（170 - 176 億），而他們在消息公布到現在約三個月的時間，股價漲幅在 23% - 35% 之間，而目前特斯拉才公布四天，股價就已上漲了約 20%。

<table class="center">
  <tr>
    <th align="center">公司</th>
    <th align="center">09/04（五） 消息公佈前股價</th>
    <th align="center">11/20（五） 上週五股價</th>
    <th align="center">目前市值</th>
    <th align="center">佔 S&P 500 權重</th>
    <th align="center">股價增減</th>
  </tr>
  <tr>
    <td align="center">ETSY</td>
    <td align="center">$112.04</td>
    <td align="center">$140.06</td>
    <td align="center">176.60億</td>
    <td align="center">0.054</td>
    <td align="center">+25%</td>
  </tr>
  <tr>
    <td align="center">Teradyne</td>
    <td align="center">$78.6</td>
    <td align="center">$106.33</td>
    <td align="center">176.57億</td>
    <td align="center">0.06</td>
    <td align="center">+35.28%</td>
  </tr>
  <tr>
    <td align="center">Catalent</td>
    <td align="center">$83.95</td>
    <td align="center">$103.33</td>
    <td align="center">170.18億</td>
    <td align="center">0.058</td>
    <td align="center">+23.09%</td>
  </tr>
</table>

<br/>
S&P 500 成分股已經超過七年沒有市值這麼高的企業加入了，上一個是 Facebook（目前市值約 7682 億，於 2013/12/23 加入，當時市值仍小於 3000 億）。

接著我們來看看若是市值較大的企業，在被納入 S&P 500 指數後，會不會帶來更明顯的漲幅。

以下我挑選了在這二年內（2019-2020）被納入 S&P 500 指數，且市值最大的二家企業：T-mobile（美國第三大電信公司，代號 TMUS）與 ServiceNow（雲端服務平台，代號 NOW）。

這二家企業（T-mobile, ServiceNow）在消息公布前的市值約是前面三家（ETSY, Teradyne, Catalent）的 5 倍。一樣挑選 T-mobile 及 ServiceNow 在消息公布前，到三個月後這段時間的數值來比較。
<table class="center">
  <tr>
    <th align="center">公司</th>
    <th align="center">消息公佈日</th>
    <th align="center">當時市值</th>
    <th align="center">公佈後三個月市值</th>
    <th align="center">佔 S&P 500 權重</th>
    <th align="center">股價增減</th>
  </tr>
  <tr>
    <td align="center">T-mobile</td>
    <td align="center">2019/07/09</td>
    <td align="center">642.1億</td>
    <td align="center">664億</td>
    <td align="center">0.25</td>
    <td align="center">+34%</td>
  </tr>
  <tr>
    <td align="center">ServiceNow</td>
    <td align="center">2019/11/18</td>
    <td align="center">523.8億</td>
    <td align="center">678.5億</td>
    <td align="center">0.33</td>
    <td align="center">+30%</td>
  </tr>
</table>

從上表可以看到，就算市值大了五倍，但漲幅一樣落在前者的 23% - 35% 之間。因此我們可以大膽假設特斯拉在納入 S&P 500 後，可能會帶來 30% - 35% 的漲幅。而目前特斯拉從公佈後的漲幅約 20%。表示仍有 10 - 15% 左右的上漲幅度。

### 後記
在 S&P 500 中的權重多寡，是依照企業的市值來決定，而權重越大，表示追蹤 S&P 500 指數的基金們就要買入越多數量的特斯拉股價，來讓基金可以確實反應 S&P 500 指數的漲幅。

而上面我們比較了權重約佔 0.06% 的三家小型企業，以及權重約佔 0.3% 的二家較大型企業，他們在從被納入 S&P 500 的消息公佈到三個月後的漲幅約在 25 - 35% 之間。

而特斯拉在加入後，可能佔的權重為 1.42，雖然市值遠遠大於我們這次拿來比較的五家企業。但我認為特斯拉的漲幅應該還是會落在 25 - 35% 之間。考慮到上週特斯拉就已經漲了 20% 左右，這週的股價波動應該就不會像上週一樣的劇烈了。