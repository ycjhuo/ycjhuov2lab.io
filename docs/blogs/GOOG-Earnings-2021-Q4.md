---
title: Google（GOOG）2021 Q4 季報，宣布拆股後仍否再漲 18%
description: '美股投資 Google GOOG|Google 拆股 |Google GOOG 2021 Q4 第四季財報分析|拆股股價上漲|美股 拆股 影響'
date: 2022-02-13
tag: [Stock, GOOG]
category: Investment
---

線上廣告龍頭 Google（GOOG）在 02/01 公佈了 2021 Q4 的財報，從 2021 Q3 季報公佈後（10/26 盤後），到公佈 Q4 季報，三個月的時間，股價從 $2793.44 → 2757.57，下跌了 1.28%，同期間，S&P 500 指數也小跌 0.62%，顯示這季度，市場沒有太大的變化 <br />

但 Google 這次除了公布財報後，也宣布了即將拆股的消息，回到 2020 年，蘋果＆特斯拉也曾拆股過，而股價也漲了不少，而 Google 能否也能重演呢？就讓我們從 Google 的 2021 Q4 財報來分析

---

Google 2021 Q3 財報回顧：[Google（GOOG）2021 Q3 季報，成長動能依然不減](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q3.html) <br />


## Google 2021 Q4 財報
[來源](https://abc.xyz/investor/static/pdf/2021Q4_alphabet_earnings_release.pdf?cache=d72fc76)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">61,880</td>
    <td align="center">65,118</td>
    <td align="center">75,325</td>
    <td align="center">5.23%</td>
    <td align="center">15.67%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">21,985</td>
    <td align="center">23,064</td>
    <td align="center">24,402</td>
    <td align="center">4.91%</td>
    <td align="center">5.80%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">57.62</td>
    <td align="center">57.58%</td>
    <td align="center">56.21%</td>
    <td align="center">-0.03%</td>
    <td align="center">-1.38%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">31.29%</td>
    <td align="center">32.2%</td>
    <td align="center">29.05%</td>
    <td align="center">1.01%</td>
    <td align="center">-3.24%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">27.26</td>
    <td align="center">36.38</td>
    <td align="center">30.69</td>
    <td align="center">33.46%</td>
    <td align="center">-15.64%</td>
  </tr>
</table>

- 總營收：與 Q3 相比，成長了 15.67%，其中成長最顯著的業務為 YouTube 廣告，與上季相比成長了近 20%，目前已佔總營收 11%
- 稅前淨利：與 Q3 相比，成長了 5.8%，主要是因銷售成本＆業務行銷的費用增長所致
- 毛利＆營業利益率：分別為 56.21% & 29.05%，與 Q3 相比，分別降低了 1.38% & 3.24%
- EPS：Q4 的 EPS 為 30.69，為 2021 的第二高，與 Q3 成長了 33.46% 相比，Q4 降低了 15.64%


## 拆股＆庫藏股
董事會宣布將針對旗下的股票 Class A, B, C 實施拆股 1 股拆成 20 股，拆股日為 2020/07/01，而在 7/15 收盤後，就會收到額外發放的 19 股 <br />

這表示從現在到 07/01 之間，持有 GOOGL（Class A）與 GOOG（Class C）的人，均可以在 7/15 收盤後，得到多的 19 股，但股價也會在 07/02 除以 20  

若對拆股不熟悉的，可參考這篇：[半年漲了六倍的特斯拉（Tesla），是否該考慮賣出](https://ycjhuo.gitlab.io/blogs/How-Tesla-Soar-600-in-6-month.html)


庫藏股部分，而 Google 在 Q4 買回自家股票的金額來到了近 135 億，比 Q3 上升了 6.84% <br />
Google 在 Q4 的稅前利潤為 206 億來說，而庫藏股金額就佔了稅前利潤的 65%，可看出 Google 十分力挺自己的股票

## 後記
Google 的股價在財報發佈後（2/2）上漲了 7.37%，來到了今年以來的最高點，顯示出投資人對於拆股都持樂觀的看法，可惜受到最近股市表現不好的影響，目前（2/11）股價已跌回財報發布前的水準

在目前投資人普遍看跌的氛圍下，就算是在去年表現最好的科技龍頭 Google 也還未搭著拆股的消息來推動股價

若以 2020 年蘋果的拆股表現來說，拆股讓蘋果的股價在一個月上漲了近 18% <br />

現在 Google 股價若以本益比來說，算是處於相對低點，若加上拆股後效應以及 Google 一直以來都有在實施的庫藏股機制，其實 Google 要跑贏大盤的機率其實相當容易，也就表示著不管市場怎麼變化，買入 Google 仍是相對安全，同時也不錯的好選擇

::: tip
蘋果從 2020/07/30 公佈要拆股的消息 → 8/31 完成拆股 <br />
這一個月的時間，蘋果股價上升了 17.46%，而同期間 S&P500 上漲了 7.24%
:::
