---
title: Square（SQ）公佈 2021 Q1 財報，比特幣收入暴增一倍 
description: 'Square SQ 2021 Q1 第一季財報|美股 Square SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報'
date: 2021-05-10
tag: [Stock, SQ]
category: Investment
---

Square（股票代號：SQ），從 02/23 公佈 2020 Q4 財報，到 2021 Q1 財報發布（05/06）期間，股價下跌了 16% <br />
而在 2021 Q1 財報公佈後的這二天，SQ 股價上漲了 7.99%

讓我們從 Square 2021 第一季財報中找出這二天大漲的原因

對 Square 2020 Q4 財報有興趣的，可參考這篇：<br />
[公佈第四季及 2020 全年財報，比特幣概念股 SQ 表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-SQ.html)

---

## 財務指標
[來源](https://s27.q4cdn.com/311240100/files/doc_financials/2021/q1/Q1-2021-Shareholder-Letter.pdf)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1 vs. 2020 Q4</th>
    <th align="center">Growth (2021 Q1 - 2020 Q4)</th>
    <th align="center">Growth (2020 Q4 - 2020 Q3)</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">50.58 億 vs. 31.6 億</td>
    <td align="center">60.09%</td>
    <td align="center">3.96%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">19.05% vs. 25.44%</td>
    <td align="center">-6.39%</td>
    <td align="center">-0.74%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">1.34% vs. 1.43%</td>
    <td align="center">-0.09%</td>
    <td align="center">-0.20%</td>
  </tr>
  <tr>
    <td align="center">EPS</td>
    <td align="center">0.09 vs. 0.65</td>
    <td align="center">-86.15%</td>
    <td align="center">87.69%</td>
  </tr>
</table>

- 營收方面：與 2020 Q4 相比，Square 在這季成長了 60%（2020 Q4 僅比 Q3 成長了 3.96%）
- 毛利率方面：與 2020 Q4 相比，下降了 6.39%（表示這些新增的營收，毛利率並不高）
- 營業利潤率：相比 2020 Q4 僅下降了 0.09%，顯示了增加的這些營收，並沒有提高太多營運費用
- EPS：這次 EPS 下降了 86.15%，是因為提撥了一筆 2,750 萬的其他費用 <br />
（DoorDash 投資損失及 Bitcoin 費用，若排除這二個因素，EPS 為 0.19）

::: tip
2020 Q4 則是多了一筆 2.7 億的 Doordash 及其他投資收入，若排除這項，則 EPS 為 0.04 <br />
若排除這些因素，本季 EPS 與 2020 Q4 相比，成長了 475%
:::

---

## 營收來源
Square 的收入由下面這四個項目所組成，下表是各項收入佔總營收的比例：
<table style="width:100%">
  <tr>
    <th align="center">營收組成</th>
    <th align="center">2021 Q1 vs. 2020 Q4</th>
    <th align="center">Growth (2021 Q1 - 2020 Q4</th>
    <th align="center">Growth (2020 Q4 - 2020 Q3)</th>
    <th align="center">Gross Margin (2021 Q1)</th>
    <th align="center">Gross Margin (2020 Q4)</th>
  </tr>
  <tr>
    <td align="center">交易收入</td>
    <td align="center">18.98% vs. 29.41%</td>
    <td align="center">-10.43%</td>
    <td align="center">-1.09%</td>
    <td align="center">45.37%</td>
    <td align="center">42.38%</td>
  </tr>
  <tr>
    <td align="center">訂閱收入</td>
    <td align="center">11.03% vs. 14.23%</td>
    <td align="center">-3.2%</td>
    <td align="center">-0.53%</td>
    <td align="center">84.12%</td>
    <td align="center">85.53%</td>
  </tr>
    <tr>
    <td align="center">硬體收入</td>
    <td align="center">0.57% vs. 0.77%</td>
    <td align="center">-0.2%</td>
    <td align="center">-0.13%</td>
    <td align="center">-40.62%</td>
    <td align="center">-47.74%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin 收入</td>
    <td align="center">69.43% vs. 55.59%</td>
    <td align="center">13.84%</td>
    <td align="center">1.74%</td>
    <td align="center">2.13%</td>
    <td align="center">2.32%</td>
  </tr>
</table>

表中可看出，Bitcoin 收入佔總營收的比重越來越高 <br />
從 2020 Q4 的 55.59% 上升到現在的 69.43%；但 Bitcoin 的毛利在四個收入來源中卻是偏低的（僅為 2.13%，在 2020 Q4 時為 2.32%）<br />

毛利最高的訂閱收入（毛利率 85.53%），在這一季僅占總營收的 11%（2020 Q4 為 14.23%）<br />

::: tip
訂閱收入雖然佔總營收的比例有所下降，但整體數字仍比 2020 Q4 成長了 24.1%<br />
和比特幣相比下仍遜色很多（成長 99.92%）
:::

---

## Seller vs. Cash App ecosystem
接著我們將 Square 分為 B2B 業務（Seller）以及 B2C 業務（Cash App），來比較哪個業務對於 Square 的獲利更為重要 <br />
（假設 Seller 跟 Cash App 在同樣項目的毛利率一樣）

<table style="width:100%">
  <tr>
    <th align="center">營收組成</th>
    <th align="center">2021 Q1 vs. 2020 Q4</th>
    <th align="center">Diff (2021 Q1 - 2020 Q4)</th>
    <th align="center">Diff (2020 Q4 - Q3)</th>
  </tr>
  <tr>
    <td align="center">交易收入（Seller）</td>
    <td align="center">17.16% vs. 27.14%</td>
    <td align="center">-9.98%</td>
    <td align="center">-0.69%</td>
  </tr>
  <tr>
    <td align="center">訂閱收入（Seller）</td>
    <td align="center">2.39% vs. 3.33%</td>
    <td align="center">-0.94%</td>
    <td align="center">0.25%</td>
  </tr>
  <tr>
    <td align="center">硬體收入（Seller）</td>
    <td align="center">0.57% vs. 0.77%</td>
    <td align="center">-0.2%</td>
    <td align="center">-0.13%</td>
  </tr>
  <tr>
    <td align="center">交易收入（Cash App）</td>
    <td align="center">1.82% vs. 2.26%</td>
    <td align="center">-0.45%</td>
    <td align="center">0.87%</td>
  </tr>
  <tr>
    <td align="center">訂閱收入（Cash App）</td>
    <td align="center">8.63% vs. 10.89%</td>
    <td align="center">-2.26%</td>
    <td align="center">0.94%</td>
  </tr>
  <tr>
    <td align="center">Bitcoin 收入（Cash App）</td>
    <td align="center">69.43% vs. 55.59%</td>
    <td align="center">13.83%</td>
    <td align="center">36.83%</td>
  </tr>
</table>

本季是第一次 Cash App 的毛利（Gross profit）贏過 Seller（51.43% vs. 48.57%）；毛利率則分別為 45.99%（Seller）與  12.27%（Cash App）<br />

對 Square 淨利影響最大的仍然為 Seller 的交易收入 與 Cash App 的訂閱收入（二者皆各佔淨利的約 40%，Bitcoin 僅佔約 8%）

::: tip
在 2020 Q4 財報中，對 Square 淨利影響最大的一樣為 Seller 的交易收入 與 Cash App 的訂閱收入：
- Seller 的交易收入約佔淨利的約 45%，Cash App 的訂閱收入約為 35%，Bitcoin 為 5%
- 毛利率方面，在 2020 Q4，Seller 為 43.26%；Cash App 為 17.34%
:::

---

## 後記
本次季報最大的亮點就是 Bitcoin 收入，從 2020 Q2 的 8.75 億到 2020 Q4 的 17.56 億，整整成長了 200% <br /> 
而在基數已經這麼大的情況下，比特幣營收居然還可以再成長 99.2%，可見比特幣的風潮仍未減弱

如同上面提到的，```對 Square 淨利影響最大的仍然為 Seller 的交易收入``` <br />

我的看法仍跟上篇一樣 [公佈第四季及 2020 全年財報，比特幣概念股 SQ 表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-SQ.html)，隨著已接種疫苗人數的增加，以及經濟活動的逐漸開放，Square 將靠著 Seller 業務的幫助，大幅度的提升獲利能力 <br />

而我也趁著 Square 這段時間的下跌，小幅加碼了 58 股的 SQ
![SQ 2021-Q1 交易紀錄](../images/SQ-Transactions-In-2021-Q1.png)