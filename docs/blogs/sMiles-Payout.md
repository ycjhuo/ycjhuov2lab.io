---
title: sMiles 出金教學，轉出 Bitcoin 到 Wallet of Satoshi
description: 'sMiles 介紹|sMiles 出金教學|Wallet of Satoshi 錢包 介紹|Wallet of Satoshi 閃電網路 錢包|Wallet of Satoshi Bitcoin BTC Lightning Network 推薦錢包|Wallet of Satoshi 錢包 轉出 教學|Wallet of Satoshi 轉出到 Cash App|'
date: 2023-04-11
tag: [sMiles, Walk2Earn, Passive Income, Crypto]
category: Investment
---

[sMiles](https://join.smilesbitcoin.com/YCJHUO551) 是一款走路就能賺比特幣的 App，在 iOS ＆ Android 都能下載，每天在走路這個項目能賺取的比特幣上限為 21 sats，除了走路，還有抽獎等其它賺取比特幣的方式


::: tip
1 個 sat 等於 0.00000001 個比特幣（Bitcoin），所以 1 千萬個 sat 等於 1 個 比特幣 <br />
依當前比特幣價值為 $28,000 美金來看，1 sat 等於 $0.00028 
:::

若單以走路來算，若每天都走一萬步，可得到 21 sats，一個月則是 630 個 sats，依目前的比特幣現值，每個月最多可賺取 $0.1764

下面就來介紹如何將 sMiles 所賺到的 sats 轉到 Wallet of Satoshi（官方預設的出金錢包）

## 註冊 Wallet of Satoshi
Wallet of Satoshi 是一個支持比特幣（Bitcoin）「閃電網路」（Lightning Network）的錢包，跟正常比特幣轉帳的差別就是手續費極低，僅收取轉帳總額的 0.3%

在這邊是作為接收 sMiles 出金之用的錢包，在將 Bitcoin 轉到這裡後，可再轉到有支援「閃電網路」的交易所完成出金

1. 先來下載 Wallet of Satoshi，下載完後開啟 App 則可看到跟下圖一樣的畫面
2. 點擊右上角的三個點，選擇 Log in to Back Up，之後填入自己的 email
3. 填完後，系統會寄出驗證信到剛剛填的 email

![註冊 Wallet of Satoshi](../images/204-01.png)

4. 收到信後，將信中的那 2 個單字填回 Wallet of Satoshi 的驗證畫面
5. 驗證完後，再點擊 Wallet of Satoshi 右上角的三個點，若看到下方原本紅色的地方轉為綠色，且有自己的 email 則表示註冊完成

![Wallet of Satoshi Bitcoin 閃電網路](../images/204-02.png)

下載＆設定完 Wallet of Satoshi 後，就可回到 sMiles 準備將我們賺到的比特幣轉到 Wallet of Satoshi 囉

## 轉出 Bitcoin 到 Wallet of Satoshi
1. 選擇 Balance 後，點擊 Send sats
2. 點擊 Send to default Wallet
3. 這裡可以選擇我們要轉出多少 sat 到 Wallet of Satoshi 錢包，我選擇轉出 2,000 個 sats，按下 Send 2,000 sats

![sMiles 轉出 Sats 到 Wallet of Satoshi Bitcoin 介紹](../images/204-03.png)

4. 按下 Send 後，手機會跳出我們下載的 Wallet of Satoshi，點擊開啟
5. 之後畫面會跳到 Wallet of Satoshi，再選擇 OK
6. 即可看到交易正在處理中的綠色畫面

![sMiles 轉出 Sats 到 Wallet of Satoshi Bitcoin 介紹](../images/204-04.png)

7. 之後馬上就可看到我們的 Wallet of Satoshi 錢包已確實收到 2,000 個 sats
8. 再回到 sMiles 後，點擊 Balance，可在 History 看到我們轉出的紀錄：扣了 2,004 個 sats，表示在轉出 sats 時，會被收取 4 個 sats 的手續費

![How to withdraw sats to sMiles Wallet of Satoshi Bitcoin](../images/204-05.png)

到這邊，我們就已成功將 sMiles 裡面的 sats 轉到 Wallet of Satoshi 錢包了 <br />
若是想將 Wallet of Satoshi 裡面的 sats 轉到其它交易所轉成現金，則只要選擇有支援「閃電網路」（Lightning Network）的交易所即可

::: warning
點擊觀看更多： <br />
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
:::