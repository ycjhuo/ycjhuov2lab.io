---
title: 特斯拉（TSLA）2022 Q4 季報，近一個月股價暴漲 70%
description: '特斯拉 TSLA 財報|特斯拉 TSLA 2022 Q4 第四季財報|特斯拉 TSLA 生產量 交車量 2022 Q4|特斯拉 2023 大降價|金融業 科技業 大裁員'
date: 2023-01-28
tag: [Stock, TSLA]
category: Investment
---

特斯拉在 1/25（三）公佈了 2022 的第四季財報  <br/>
在 2022 Q3 - 2022 Q4 財報期間（10/19 - 1/25），股價從 $222.04 → $144.43，下跌了 35%（同期間 S&P 500 上漲 9%） <br/>

而股價最低時為 $101.81，而在 2022 的表現為 -69%（S&P 500 為 -20%），但這一切都隨著 [特斯拉公佈 2022 第四季交車量](https://ycjhuo.gitlab.io/blogs/TSLA-P&D-2022-Q4.html)  <br/>

在 1/2 公佈了 2022 Q4 生產＆交車量後，不到一個月，股價至今上漲了 70%

特斯拉是否已開啟回漲之路，將在 2023 年實現大反轉呢？下面來看看特斯拉 2022 第四季財報

## 特斯拉 2022 Q4 財報
[來源](https://tesla-cdn.thron.com/static/GZR0GS_TSLA_Q4_2022_Update_PVPJAG.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22b7871185-dd6a-4d79-9c3b-19b497227f2a.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">2022 Q4</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">電動車營收（百萬）</td>
    <td align="center">14,602</td>
    <td align="center">18,692</td>
    <td align="center">21,307</td>
    <td align="center">-15.47%</td>
    <td align="center">21.88%</td>
    <td align="center">12.27%</td>
  </tr>
  <tr>
    <td align="center">電動車利潤（百萬）</td>
    <td align="center">4,081</td>
    <td align="center">5,212</td>
    <td align="center">5,522</td>
    <td align="center">-35.73%</td>
    <td align="center">21.7%</td>
    <td align="center">5.61%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">27.9%</td>
    <td align="center">27.9%</td>
    <td align="center">25.9%</td>
    <td align="center">-5%</td>
    <td align="center">--</td>
    <td align="center">-2%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">14.6%</td>
    <td align="center">17.2%</td>
    <td align="center">16%</td>
    <td align="center">-4.6%</td>
    <td align="center">2.6%</td>
    <td align="center">-1.2%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">0.65</td>
    <td align="center">0.95</td>
    <td align="center">1.07</td>
    <td align="center">-46.67%</td>
    <td align="center">31.58%</td>
    <td align="center">11.2%</td>
  </tr>
</table>

- 電動車營收：在 2022 達到了 715 億，其中 Q4 為 213 億，佔了整年度的 30%，季成長則由 22% → 12%
- 電動車利潤：2022 全年約達到 204 億，而 Q4 為 55 億，佔了整年度的 27%，季成長則由 22% → 6%
- 毛利率：2022 平均毛利率為 28.5%，除了 Q1 （32.9%）超過平均外，其餘三季均遜於平均，而本季則是 2022 中最低的 25.9%
- 營業利益率：2022 平均營利率為 16.8%，本季為 16%，季衰退的幅度 < 毛利率，顯示出特斯拉擅於整合內部資源的能力
- EPS：Q4 的 EPS 為 1.07，佔了 2022 （3.62）的 30%，更是 2021 （1.63）的 66%

電動車營收的季成長仍保持在雙位數，但利潤的季成長卻由於毛利率的下滑，衰退至 6%  <br/>
而受益於較低的營業費用，營業利益率則在本季下降的幅度略少於毛利率  <br/>

而由於特斯拉在今年一月在全球各市場大降價，降幅約 10-20% 不等，因此特斯拉在 2023 Q1 的財報，將無法保持現在 26% 的毛利率＆ 16% 的營利率

## 財報重點
特斯拉達成了：總營收年成長 51%，淨利潤年成長 128%  <br/>

從 2017 - 2022，成功的將營利率從 -14% 提升至 17%，有助於讓特斯拉獲得再次降價的空間，以實現人人都能買得起電動車的目標  <br/>

近期也釋出了 FSD Beta 版給北美的購買客戶（約 40 萬人），幫助特斯拉取得更多自動駕駛的資料，並期望 FSD 可成為另一個營收主力  <br/>

面對目前升息的市場下，特斯拉重申仍會持續加強有效率的生產方式，以便再次降低車子的售價，減輕車主的負擔（車貸） <br/>

而目前 Model 3/Y 的年產量已到達 180 萬輛（同時也是 2023 的目標），且在 2022 年底也交付了第一台的 Semi  <br/>

## 後記
雖說這次的財報讓特斯拉再次證明了自己的價值，同時也反應在股價上（一個月內漲了 40％），但由於：
- 一月份特斯拉全球大降價：美國降 15% ； 歐洲降 11% ； 中國降 11%  
- 近期科技業＆金融業的大裁員，如：Google 裁員 6 %，約 12,000人 ； 微軟裁員 5%，約 11,000 人  <br/>
Amazon 裁員 18,000 人 ； Meta 裁員 11,000 人 ； 高盛 裁員 6.5%，約 3,200 人

這些跡象顯示了特斯拉在 2023 Q1 的財報將無法維持現有的獲利能力（除非銷量大暴漲） <br/>
但在裁員潮剛起，且仍會有第二波的情況下，大幅削減著消費者的買車意願，以及是否要將手中股票變現，以渡過失業期間的生活開銷  <br/>

讓我在這個月的暴漲行情下（不到一個月上漲 70%），保持著觀望的態度，僅在[上篇 2022 Q4 生產及交付量發佈](https://ycjhuo.gitlab.io/blogs/TSLA-P&D-2022-Q4.html)時購買 10 股而已

![TSLA 特斯拉 2023 抄底](../images/210-01.png)


::: warning
點擊觀看更多：  <br/>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::