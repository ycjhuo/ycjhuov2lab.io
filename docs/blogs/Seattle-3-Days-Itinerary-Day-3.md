---
title: 疫情間的西雅圖三天二夜（Day 3）華盛頓大學 University Village
description: '西雅圖 景點|西雅圖 推薦|西雅圖 介紹|西雅圖 華盛頓大學|Seattle 景點|Seattle 推薦|Seattle 介紹|Seattle 華盛頓大學 介紹|西雅圖 三天二夜|Seattle 三天二夜|西雅圖 必去|Seattle 必去'
date: 2021-04-01 00:45:54
tag: [Seattle, Covid]
category: Travel
---

接續前二篇：<br />
[疫情間的西雅圖三天二夜（Day 1）派克市場 星巴克創始店 西雅圖藝術博物館 西雅圖中央圖書館 西雅圖摩天輪](https://ycjhuo.gitlab.io/blogs/Seattle-3-Days-Itinerary-Day-1.html)<br />

[疫情間的西雅圖三天二夜（Day 2）瑞尼爾山 The Spheres 太空針塔 凱利公園](https://ycjhuo.gitlab.io/blogs/Seattle-3-Days-Itinerary-Day-2.html)<br />

## 景點規劃
第三天的行程是：<br />
西雅圖市區（Seattle Downtown） ➔ 華盛頓大學 ➔ University Village ➔ 機場（SEA Airport）<br />

前二天已經把西雅圖市區逛的差不多了，接下來我們就可以搭乘 light rail 到稍微遠一點的華盛頓大學（若從市區的 Westlake 站出發的話，僅二站距離，車程約 10-15 分）

![Link light rail station Map](../images/71-01.jpg)

## 華盛頓大學（University of Washington）
華盛頓大學最著名的就是櫻花季時的櫻花以及噴泉（Drumheller Fountain），天氣好時還能從噴泉這邊看到瑞尼爾山，可惜我們去的時候不是櫻花季（四月），天氣也不好，噴泉更沒在運作，不過校園還是蠻漂亮的<br />

附近也有二家著名的咖啡廳：Ugly Mug Cafe 以及 Cafe Allegro，可惜因為疫情關係均指提供外帶服務 <br />
![University of Washington](../images/73-01.jpg)
![University of Washington](../images/73-02.jpg)
![University of Washington](../images/73-03.jpg)

## University Village
逛完校園後，可以往 University Village 走路，一路上都是下坡，路程約 20 分 <br />
University Village 是一個購物中心，但每個品牌都有自己的建築，而不是集中在一棟商場裡面 <br />

裡面有著名的冰淇淋店（Molly Moon’s Homemade Ice Cream，Kerry Park 附近也有一家），鼎泰豐（市區也有一家）<br />
各類服飾店，亞馬遜書店，特斯拉展示店 <br />

![University Village Bookstore](../images/73-04.jpg)
![University Village Tesla](../images/73-05.jpg)

## 返回機場（SEA Airport）
接著就可以直接搭 Uber 回 light rail 站，或是直接搭 Uber 回飯店拿行李，就可以準備前往機場了<br />
也可以直接從 University of Washington 的 light rail 站，搭 15 站就到機場（SeaTac / Airport），車程約 50 - 60 分<br />

若還有時間的話，也可以到華盛頓大學左邊的藝術小鎮 （Fremont）

到了機場站（SeaTac / Airport）下車後，走到機場約 10 分左右，沿途也有小型接駁車直接載你到機場，最後就可以悠閒地在機場貴賓室等候上機囉

---

對於貴賓室有興趣的可參考：<br />
[開箱！美國運通百夫長貴賓室（西雅圖 塔科馬國際機場 SEA Airport）](https://ycjhuo.gitlab.io/blogs/SEA-Centurion-Lounge.html)
