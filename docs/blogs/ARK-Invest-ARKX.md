---
title: ARKX（太空探索 ETF），方舟投資（ARK Invest）新成立的主動型 ETF
description: '女股神 Cathy Wood|方舟基金介紹|Ark Invest|ARKX 太空 ETF|方舟基金 ETF|ARKX 介紹比較|ARKX 手續費報酬率|太空概念股'
date: 2021-04-03 00:58:10
tag: [Stock, ETF, ARK, ARKX]
category: Investment
---

ARKX（ARK SPACE EXPLORATION AND INNOVATION ETF），著重於太空探索的 ETF 於 2021/03/30 成立 <br />

直至這星期結束，在三個交易日上漲了 2% <br />

同期間追蹤納斯達克指數的 QQQ 上漲 3.4%，而 ARKK 則上漲 9.86%，顯示了 ARKX 並未如預期的一推出就大漲 <br />

先來把新出的 ARKX 與其他 5 支 ARK 的主動型 ETF 來做個對比

## 方舟投資 ETF
下表看目前 ARK 旗下的六支主動型 ETF（資料日期：截至 2021/03/26）：
<table style="width:100%">
  <tr>
    <th align="center">ARK ETF</th>
    <th align="center">ARKK（破壞式創新）</th>
    <th align="center">ARKQ（自動化及機器人）</th>
    <th align="center">ARKW（新一代網路）</th>
    <th align="center">ARKG（基因改良及醫療）</th>
    <th align="center">ARKF（金融創新）</th>
    <th align="center">ARKX（太空探索）</th>
  </tr>
  <tr>
    <td align="center">成立時間</td>
    <td align="center">2014/10/31</td>
    <td align="center">2014/09/30</td>
    <td align="center">2014/09/30</td>
    <td align="center">2014/10/31</td>
    <td align="center">2019/02/04</td>
    <td align="center">2020/03/30</td>
  </tr>
  <tr>
    <td align="center">資產規模</td>
    <td align="center">176.8 億</td>
    <td align="center">17 億</td>
    <td align="center">52.7 億</td>
    <td align="center">76.7 億</td>
    <td align="center">19.6 億</td>
    <td align="center">N/A</td>
  </tr>
  <tr>
    <td align="center">管理費</td>
    <td align="center">0.75%</td>
    <td align="center">0.75%</td>
    <td align="center">0.79%</td>
    <td align="center">0.75%</td>
    <td align="center">0.75%</td>
    <td align="center">0.75%</td>
  </tr>
  <tr>
    <td align="center">持有企業（支）</td>
    <td align="center">48</td>
    <td align="center">42</td>
    <td align="center">55</td>
    <td align="center">52</td>
    <td align="center">47</td>
    <td align="center">41</td>
  </tr>
</table>

之前出的 ETF 中，除了 ARKF（金融創新 ETF）僅成立了 2 年，其餘 ETF 皆已成立了 6.5 年左右（ARKF 雖然成立時間較晚，但資產規模卻已超過 ARKQ，是否表示為相較於自動化設備，投資人更為青睞 Fintech 領域）

管理費的話，除了 ARKW 稍微高了 0.05% 左右，其它 ETF 皆為 0.75%（ARKW 同時也是持有最多企業的 ETF，不確定是否為這個原因，導致管理費較高）

接著來看每支 ETF 的前三大持股為何，讓我們從中了解 ARK 的選股邏輯

--- 

## 各 ETF 前三大持股
下面列出了 6 支 ETF 的各前三大持股（資料日期：截至 2021/04/01）
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">ARKK（破壞式創新）</th>
    <th align="center">ARKQ（自動化及機器人）</th>
    <th align="center">ARKW（新一代網路）</th>
    <th align="center">ARKG（基因改良及醫療）</th>
    <th align="center">ARKF（金融創新）</th>
    <th align="center">ARKX（太空探索）</th>
  </tr>
  <tr>
    <td align="center">第一大持股（權重）</td>
    <td align="center">TSLA（11.03%）</td>
    <td align="center">TSLA（10.65%）</td>
    <td align="center">TSLA（10.98%）</td>
    <td align="center">TDOC（7.17%）</td>
    <td align="center">SQ（10.51%）</td>
    <td align="center">TRMB（8.60%）</td>
  </tr>
  <tr>
    <td align="center">第二大持股（權重）</td>
    <td align="center">SQ（6.51%）</td>
    <td align="center">TRMB（5.62%）</td>
    <td align="center">GBTC（5.78%）</td>
    <td align="center">EXAS（5.11%）</td>
    <td align="center">SI（4.72%）</td>
    <td align="center">PRNT（6.08%）</td>
  </tr>
  <tr>
    <td align="center">第三大持股（權重）</td>
    <td align="center">TDOC（6.30%）</td>
    <td align="center">BIDU（5.14%）</td>
    <td align="center">SQ（5.71%）</td>
    <td align="center">PACB（5.01%）</td>
    <td align="center">PYPL（4.60%）</td>
    <td align="center">KTOS（5.75%）</td>
  </tr>
  <tr>
    <td align="center">總計權重</td>
    <td align="center">23.84%</td>
    <td align="center">21.41%</td>
    <td align="center">22.47%</td>
    <td align="center">17.29%</td>
    <td align="center">19.83%</td>
    <td align="center">20.43%</td>
  </tr>
</table>

上表可看出，在 6 支 ETF 中，其中 3 支 ETF 的最大持股皆為特斯拉，且權重都超過 10%（看來 ARK 真的是特斯拉的超級支持者） <br />

接著是 Square（SQ），也在 3 支 ETF 中，佔有不少的權重；與此同時，ARKX 的最大持股 TRMB，也是 ARQK（自動化及機器人）ETF 的第二大持股 <br />

大部分的 ETF，光是前三大持股，就佔了整支 ETF 權重的 20% 以上

---

## ARKX（太空探索）ETF 持股
因為前三大持股的表現，幾乎可以左右整支 ETF 的表現，下面是 ARKX 前三大持股的簡介：<br />

- TRMB（8.6%）：業務範圍遍及農業，建築，地理測量等，近五年股價漲幅為 228.73%（近一年為 183.68%）
- PRNT（6.08%）：ARK 基金旗下的 3D 列印行業的被動式 ETF，近五年股價漲幅為 100.96%（近一年為 154.44%）
- KTOS（5.75%）：專精在無人系統，衛星通訊等領域，近五年股價漲幅為 473.83%（近一年為 108.01%）

這前三大持股，這五天漲幅分別為 10.8%（TRMB），5.7%（PRNT），12.71%（KTOS）

另外，在 ARKX 中，其他知名的持股有：<br />
京東（JD，權重 4.92%），波音（BA，3.58%），輝達（NVDA，3.36%），亞馬遜（AMZN，2.99%）<br />
Google（GOOG，2.8%），Garmin（GRMN，2.02%），維京銀河（SPCE，1.97%），空中巴士（AIR，1.58%）<br />
阿里巴巴（BABA，1.56%），台積電（TSM，1.01%），騰訊（TCEHY，0.97%）

## 後記
先前說到太空行業，大家不外乎會想到 Musk 的 SpaceX，Bazos 的 Blue Origin，以及 Branson 的 Virgin Galactic <br />
但前二家目前皆無上市，因此一開始知道 ARK 要發行太空探索的 ETF 時，以為 Virgin Galactic 肯定會在 ETF 中佔有不低的比重<br />

但以這持股名單來看，ARK 挑選的方向更為接近太空產業的基礎建設，例如：地圖測量，衛星通訊，3D 列印等非直接聯想到的相關產業，而真正提供太空飛行的維京銀河卻在 ETF 中僅佔了 1.97% 的權重 <br />

不確定這是否代表著，在太空探索方面，相較於仍在燒錢的飛行運輸產業，ARK 更為偏向目前已有獲利能力的相關間接產業（但在 ARK 的其它支 ETF 中，仍在虧損的企業仍佔了多數）<br />

抑或是，等到 SpaceX 或 Blue Origin 上市後，ARK 就會迅速重倉這二支股票，使之成為前幾大權重的股票之一

---

來源：<br />
[ARKX 官網介紹](https://ark-funds.com/arkx)<br />
[ARKX 持股](https://ark-funds.com/wp-content/fundsiteliterature/holdings/ARK_SPACE_EXPLORATION_&_INNOVATION_ETF_ARKX_HOLDINGS.pdf)<br />

延伸閱讀：<br />
[特斯拉（Tesla）公布 Q4 季報，成長速度是否減弱](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-Q4-2020.html)<br />
[公佈第四季及 2020 全年財報，比特幣概念股 SQ 表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-SQ.html)<br />
