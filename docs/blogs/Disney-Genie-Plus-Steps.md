---
title: 如何購買迪士尼 Disney Genie Plus（Disney Genie+）
description: '迪士尼快速通關|快速通關 Fast Pass 介紹|迪士尼 Genie Plus 介紹|什麼是 Genie Plus|Disney Genie+ 介紹|Disney Genie+ 推薦|Disney Genie+ 教學'
date: 2021-12-23
tag: [Orlando, Disney]
category: Travel
---

上篇 [開箱！迪士尼 Disney Genie Plus（Disney Genie+）介紹](https://ycjhuo.gitlab.io/blogs/Disney-Genie-Plus.html) 介紹了迪士尼新推出的快速通關 Disney Genie+

這篇來介紹該如何使用 Disney Genie+

## 選擇日期
1. 一點開迪士尼的 App （My Disney Experience - Walt Disney World），點擊右上角的 My Day
![如何購買 Disney Genie+](../images/143-01.png)

2. 之後點擊 Change Day，選擇我們已經購買門票的那天（這裡可以看到我買了 18 & 19 這二天的票）
![如何購買 Disney Genie+](../images/143-02.png)

3. 先選擇 12/18 這天，就可以看到下方已經顯示了我們的票（12/18）以及要去的園區（Animal Kingdom）
![如何購買 Disney Genie+](../images/143-03.png)

4. 之後下滑 App 到最底下，看到紫色部分的 Genie+，點擊 Get Dsiney Genie+ for Today
![如何購買 Disney Genie+](../images/143-04.png)

## 購買 Disney Genie+
1. 來到 Disney Genie+ 的頁面後，點擊 Get Dsiney Genie+ Service <br />
![購買 Disney Genie+ 教學](../images/143-05.png)

2. 接著就會來到付款頁面，確認日期沒錯後，系統會偵測我們買了幾張門票，並自動帶入，二個人含稅的價格是 31.94 <br />
![購買 Disney Genie+ 教學](../images/143-06.png)

3. 接著滑到下方點擊確認後，就成功購買到 Disney Genie+ Service 了
![購買 Disney Genie+ 教學](../images/143-07.png)

## 使用心得
購買了 Disney Genie+ Service 後，就可以在當天早上開始預約想玩的設施了 <br />
我在當天早上 7:00 就在飯店預約好 Lighting Lane 了，不須等到入園後再預約 <br />

而早上 7:00 時，在 Epcot 園區內，最熱門的 Test Track 就已經剩下晚上 8:30 的位置了，為了不想在這個設施上花 90 - 120 分鐘排隊，只好先預約了這個，而在 11:30 左右才可以再預約第二個 Lighting Lane（當天是 10:20 左右入園的）

總體而言，Disney Genie+ 值不值得購買，還是看個人，以 $15 的價格來說，我認為就算只用到 2 個設施，節省了 2 個小時也是很划算的

即使我們用了 Disney Genie+，但仍無法在一天內玩完所有的設施，若真要在一天內玩完全部的話，勢必要再加購 Individual Lightning Lane （一人約 $7 - $14）<br />

但這個很快就會賣完，因此想買的話，也須早點起來搶，而不是想買就買得到的


若想看更多迪士尼文章，可點選文章上方的 Disney Tag，或是 [Disney](https://ycjhuo.gitlab.io/tag/Disney/) 


::: warning
點擊觀看更多： <br />
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
:::