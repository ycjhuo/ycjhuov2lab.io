---
title: Block（SQ）2023 Q1 財報，下季將實現轉虧為盈
description: 'Block SQ 2023 Q1 第一季財報|美股 Block SQ 財報|美股 Fintech 龍頭|美股 Fintech 財報|Block 收購 Afterpay|Block 收購先買後付平台|Block buy now pay later” (BNPL)'
date: 2023-05-21
tag: [Stock, SQ]
category: Investment
---

Block（股票代號：SQ），從 2022 Q4 - 2023 Q1 財報發布期間（2/23 → 5/4），股價下跌了 18.5%（$74.15 → $60.43），同期間 S&P 500 指數上漲 2%

Block 股價自 2021/08 達到高峰後（$275），就一路走下坡，更別說是在 3/23 又被著名空頭公司興登堡（Hindenburg Research）發佈了看空報吿，當時股價為 $61.88，今年至今的報酬率為 -9%

下面就來看看 Block（SQ）在 2023 Q1 的財報，了解 Block 目前的狀況

## Block 財務指標
[來源](https://s29.q4cdn.com/628966176/files/doc_financials/2023/q1/Shareholder-Letter_Block_1Q23.pdf)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q4</th>
    <th align="center">2023 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2023 Q1 vs. 2022 Q4</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">46.5 億</td>
    <td align="center">49.9 億</td>
    <td align="center">2.5%</td>
    <td align="center">3%</td>
    <td align="center">7.3%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">35.7%</td>
    <td align="center">34.4%</td>
    <td align="center">-0.7%</td>
    <td align="center">1%</td>
    <td align="center">-1.3%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-2.9%</td>
    <td align="center">-0.1%</td>
    <td align="center">3.8%</td>
    <td align="center">-1.8%</td>
    <td align="center">2.8%</td>
  </tr>
  <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">-0.19</td>
    <td align="center">-0.03</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 營收：已達成連續 4 次的季成長，本季營收為 49.9 億（季增 7%），主要受惠於「比特幣」業務的推動（季增 18%），目前佔總營收的 43%
- 毛利率：季減 1%，目前毛利 34%，與 2022 的平均毛利率一致
- 營業利潤率：因營運支出的減少，本季營利率從 -2.9% → -0.1%，對比 2022 的 -3.6%，有著大幅的進步，下個季度將可實現轉虧為盈
- EPS：由於營利率的大幅改善，EPS 由上季的 -0.19 → -0.03，下個季度將會是睽違 7 個季度的正 EPS


本季的營收若扣除「比特幣業務」，其實與 2022 Q4 相差無幾，毛利率也差不多，主要讓公司降低虧損的原因在於行銷＆行政的支出有著顯著的降低（季減 8% & 4%），如此簡單的改變，就能讓公司幾乎實現獲利

這代表著 Block 的商業模式是可行的，僅須降低自身支出，即可帶給投資人滿意的成果 <br />
但因 Block 仍處在成長期，因此短期內不須追求轉虧為盈，相反的，將賺來的錢再投入公司，用以發展出更多盈利的業務，才是對於投資人最好的選擇


## Block 各項表現
![33-01](../images/219-01.png)

上圖從左至右看，毛利的年成長率從上季的 40% → 32%，而 Cash App ＆ Square 分別佔了 54% ＆ 45%，年成長率則分別為 49% ＆ 16%

可看出 Cash App 不僅是主要的毛利來源，成長速度也遠超 Square，也難怪在興登堡的做空報告中，攻擊的對象全是 Cash App

右半部則可看到，由於營利率的上升，各項獲利指標都有明顯的轉虧為盈，下一季度將可實現正收益

::: tip
目前 Cash App 月活用戶為 2,000 萬，年成長 34%，作為對比 <br /> 
Facebook 北美的月活用戶為 2.69 億，年成長 2.3%，Cash App 的成長空間仍很巨大
:::

下表為 Block 的各項營收來源＆季成長率

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q4</th>
    <th align="center">2023 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2023 Q1 vs. 2022 Q4</th>
  </tr>
  <tr>
    <td align="center">Square 交易營收</td>
    <td align="center">13.5 億</td>
    <td align="center">12.9 億</td>
    <td align="center">3%</td>
    <td align="center">-3%</td>
    <td align="center">-5%</td>
  </tr>
  <tr>
    <td align="center">Square 訂閱營收</td>
    <td align="center">3.7 億</td>
    <td align="center">3.4 億</td>
    <td align="center">4%</td>
    <td align="center">11%</td>
    <td align="center">-7%</td>
  </tr>
    <tr>
    <td align="center">Square 硬體營收</td>
    <td align="center">0.35 億</td>
    <td align="center">0.37 億</td>
    <td align="center">-10%</td>
    <td align="center">-18%</td>
    <td align="center">5%</td>
  </tr>
  <tr>
    <td align="center">Cash App 交易營收</td>
    <td align="center">1.2 億</td>
    <td align="center">1.3 億</td>
    <td align="center">2%</td>
    <td align="center">3%</td>
    <td align="center">10%</td>
  </tr>
  <tr>
    <td align="center">Cash App 訂閱營收</td>
    <td align="center">9 億</td>
    <td align="center">9.7 億</td>
    <td align="center">12%</td>
    <td align="center">12%</td>
    <td align="center">8%</td>
  </tr>
    <tr>
    <td align="center">Cash App 比特幣營收</td>
    <td align="center">18 億</td>
    <td align="center">22 億</td>
    <td align="center">-1%</td>
    <td align="center">4%</td>
    <td align="center">18%</td>
  </tr>
</table>

Cash App 不僅在各項營收的成長率都高於 Square，也貢獻了比 Square 更多的利潤
若將時間拉長到年成長率的話：
- Cash App 在本季帶來了 32.7 億的營收 ＆ 9.3 億的利潤，年增長 33% ＆ 49%，若排除比特幣營收，則營收年增長為更高的 52%
- Square 在本季則是帶來 16.7 億的營收 ＆ 7.7 億的利潤，年增長 15% ＆ 16%


## 後記
本季 Block 由於營運支出的縮減，使營業利潤率來到 6 個季度以來的高點，股價也是雖從那個季度開始大幅下滑，至今已跌了近 80%（$275 → $59），但若保持這個趨勢，Block 將可在下個季度實現正收益

同時，Block 最近向 Intel 購買了一批晶片，用於研發比特幣挖礦的晶片，並計畫在明年初推出該晶片＆相關硬體，這將使 Block 更加深了與比特幣業務的連結

在 Block 在收購 AfterPay 進軍「先買後付」領域後，現在又將踏足「比特幣挖礦」的領域，這否能成為推升 Block 股價的關鍵，仍
得看比特幣的走向而定

目前比特幣已從 2021/11 的高點 $65,467 跌至 $27,173，跌幅高達 60%，但今年以來，比特幣已回升到 $27,173，大漲了 63%

我也在最近開始加碼 Block，是今年的第一次買入，希望 Block 能在財報好轉＆踏入新領域的加持下，擺脫股價下滑的趨勢

::: warning
點擊觀看更多： <br />
- [Block / Square （SQ）分析文](https://ycjhuo.gitlab.io/tag/SQ/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
