---
title: '如何在 VuePress 中利用 Valine, Vssue 加入評論功能( 以 RECO 主題為例 )'
description: 'VuePress 介紹|VuePress 教學|VuePress 留言|VuePress 評論|Valine 介紹|Valine 教學|Valine 留言|Valine 評論|Vssue 評論|Vssue 介紹|Vssue 教學|Vssue 留言|Vssue 評論|Valine Vssue 哪個好|VuePress 推薦|Valine 推薦|Vssue 推薦|'
date: 2020-12-06 21:39:53
tag: [VuePress, Valine, Vssue, Blog]
category: Blog
---

在 VuePress 的 RECO 主題中提供了二個添加評論的套件（Valine 及 Vssue），如果依設定的難易度來說的話，Valine 是比較適合的選擇。且 Valine 可以讓使用者不須註冊帳號就可直接留言。相反的，Vssue 則須要有 Gitlab / GitHub 等帳號才可留言。

---

## 如何在 VuePress 加入 Valine
Valine 是基於 LeanCloud 的服務。LeanCloud 除了能讓 Blog 有留言的功能外，也可以用來追蹤每篇文章的閱讀次數。
要使用 Valine 前，須先到 LeanCloud 上註冊一個帳號，取得 appId 及 appKey 後。就可以在 VuePress 中來加入 Valine 了。

### 註冊 LeanCloud
LeanCloud 分為中國版及國際版，若使用中國版的話，則須提供身分證字號來通過實名認證才可以使用。因此對於沒有中國身分證字號的人來說就只能使用國際版的了（二種版本的帳號是各自獨立的，因此若要換版本的話則須另外再註冊一個帳號）。

在註冊完帳號後，可在左下角的設置 -> 應用 Keys 找到自己的 appId 及 appKey。

![40-1](../images/40-1.png)

### 安裝 Valine 套件
用以下指定來安裝 Valine（二選一），安裝完後會在 package.json 檔案中看到 Valine 已經被加入到 dependencies 中

```powershell
# 用 npm 安裝
$ npm install valine --save

# 用 yarn 安裝
$ yarn add Valine
```
```javascript
"dependencies": {
    "valine": "^1.4.14",
  }
```

### 將 Valine 加到 config.js
之後再到 config.js 裡面的 themeConfig 中，加進剛剛註冊的 LeanCloud 拿到的 appId 及 appKey。
```javascript

"themeConfig": {
    "valineConfig": {
      "appId": "XXXXXX",
      "appKey": "XXXXXX"
    },
}
```

設定好後，就可以在每篇文章的下方看到 Valine 功能已經被加上囉。
![40-2](../images/40-2.png)

另外，因為 Valine 本身也具備追蹤文章的閱讀次數功能，因此，我們可以在文章的標題下面，看到文章的閱讀次數。
如下圖：

![40-3](../images/40-3.png)

---

## 如何在 VuePress 加進 Vssue

Vssue 是藉由利用像 Github, GitLab 等平台 所提供的 OAuth API 衍生出來的一個評論功能，可以讓 Blog 這種無後端的網站也能具備讓訪客留言的功能。相較 Valine 是透過 LeanCloud 來管理留言， Vssue 則是支援 Github, GitLab 等。

換句話說，要用 Vssue，得先去 Github / GitLab 註冊帳號，且留言的人也要用 Github / GitLab 帳號來登入，才能留言。
這裡用 Github 為例。

目前 Gitlab 會因為無法自動辨識網域下的其他網址，而在帳號跳轉驗證時會出現 The redirect URI included is not valid.

例如我的網域是： https://ycjhuo.gitlab.io

這篇文章網址是：https://ycjhuo.gitlab.io/Using-Valine-Vssue-In-Vuepress.html

在按下右下角的 Login with GitLab 時就會出現這個錯誤訊息：The redirect URI included is not valid.
如下圖：
![40-4](../images/40-4.png)

因此，若要使用 Vssue 的話，推薦使用 Github，且因為 有 Github 帳號的人也比較多，所以是比 Gitlab 合適的。


### 建立 Github 的 OAuth Apps
登入 Github 後，須要先建立一個 repository（名字隨便打，這裡我用 vssue），裡面之後會拿來放留言的資料，現在空的就可以了。
之後到 Settigns 裡面，左側有一個 Developer settigns。進去右邊選擇 OAuth Apps，接著選擇 new OAth App 

進入到 new OAth App 後，我們需要填入 Application name （可以自己取名，我一樣用 vssue），Homepage URL 與 Authorization callback URL 則是填入部落格的網址（我填：https://http://ycjhuo.gitlab.io/）
若只想先在本機端測試就填 http://localhost:8080

按下確定後，就會看到已經產生了 Client ID，然後我們再按下 Generate a new client serect。

### 將 Vssue 加進 config.js
接著到 config.js 裡面的 themeConfig 中，填上剛剛拿到的 Client ID 及 Client Secrets。

owner 是 github 帳號，repo 則是 repository 名稱。

```javascript
"themeConfig": { 
     "vssueConfig": {
      platform: 'github',
      owner: 'leon78113',
      repo: 'vssue',
      clientId: '498603059d3278e9e740',
      clientSecret: '1c98175c5f55406a3fe1da95ef0bb029f6647491'
    },
}
```

![40-5](../images/40-5.png)

這樣就建立好 Vssue 了，回到 Blog 後，就可以看到下方已經出現留言框及 Login With GitHub 的登入框，點擊登入框後，輸入自己的 GitHub 帳號密碼，就會出現如下圖的授權申請：
![40-6](../images/40-6.png)

在通過驗證後，就可以留言囉。