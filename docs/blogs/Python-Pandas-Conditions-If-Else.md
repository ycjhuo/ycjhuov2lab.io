---
title: Pandas 中如何實現 if else 條件式（conditions）
description: 'Python if else 教學 | Pandas if else 教學 | Pandas loc 介紹 | Pandas Dataframe 欄位相減'
date: 2021-01-18 10:44:31
tag: [Python, Pandas]
category: Programming
---

if else 這個條件式算是程式語言中的必備的語法，在 pandas 中若我們要進行條件的篩選時，通常會直接對 dataframe 設定篩選條件。

![45-01](../images/45-01.png)

以上面這個例子來說，若我們要篩選出身份為（Identity）HomeStaff 的人，會直接在 dataframe 中包著篩選資料來得到我們想要的資料。如：
```deductTimeOffDf[deductTimeOffDf['Identity'] == 'HomeStaff']```
:::tip
deductTimeOffDf 為 dataframe 的名稱，上圖最右邊的 DeductTimeOff 欄位之所以顯示 NaT，是因為我們還沒針對這個欄位做運算
:::

## 符合條件的欄位相減
接著我們來算出 DeductTimeOff 這個欄位的結果，公式為：上班時數（Total Hours） - 請假時數（HomeStaff Time Off）。而因為我們這個資料表內有不同身份的員工，這裡要只計算身份為 HomeStaff 的話，可用下面這段程式碼來取得。

若要計算身份為 local 的話，則可用下面的 line 5 來取得：<br/>
欄位及公式為：上班時數（Total Hours） - 請假時數（local Time Off）

```python
# Home Staff: total hours - time off hours
deductTimeOffDf.loc[(deductTimeOffDf['Identity'] == 'HomeStaff'), 'DeductTimeOff'] = deductTimeOffDf['Total Hours'] - deductTimeOffDf['HomeStaff Time Off']

# Local: total hours - time off hours
deductTimeOffDf.loc[(deductTimeOffDf['Identity'] == 'Local'), 'DeductTimeOff'] = deductTimeOffDf['Total Hours'] - deductTimeOffDf['Local Time Off']
```

由以上程式碼可以看出， if else 的使用方法為：<br/>
```dataframe.loc[(dataframe['A'] == 篩選條件), 'B'] = condition```
:::tip
公式內的 A, B 為欄位名稱，公式運行的邏輯為：先用 dataframe.loc 將要判斷的 dataframe 中的 A 欄位包起來，若 A 欄位符合我們設定的條件，則去改變 B 欄位的資料。而 B 欄位要依據什麼邏輯去改變資料，則寫在 = 的右方
:::

![45-02](../images/45-02.png)

上圖可以看到，我們已得出了每位同事的 DeductTimeOff（扣除請假時間後的上班時數），但在 index 4 同事的 DeductTimeOff 卻顯示了 - days。

這是因為他當天雖然請假了 7 小時，但還是有上班。所以在二者相減後，DeductTimeOff 就會變成負的。
而扣除請假時間後上班時數應該只會被扣到 0 小時。因此我們需要 DeductTimeOff 欄位中，將所有 - days 的資料改為 0。

## 符合條件的改變欄位資料
一樣可以用 if else 的判斷來做：
```python
# 請假時 上班時數為 0，減掉請假時數後 總時數會變成 -1 days 需轉換成 0:00:00
deductTimeOffDf.loc[(deductTimeOffDf['DeductTimeOff'] < timedelta(seconds=0)), 'DeductTimeOff'] = timedelta(0)
```

:::tip
公式邏輯為：當 dataframe（deductTimeOffDf）中的 DeductTimeOff 欄位，數值小於 0:00:00 時（也就是上面說到的 - days），就直接將 DeductTimeOff 的數值改為  0:00:00。 
:::

若想了解 timedelta 這個時間格式的話，可以參考我的這篇文章：[Pandas 的時間格式轉換及如何在 Excel 中正確顯示時間](https://ycjhuo.gitlab.io/blogs/Python-Pandas-Time-Format.html)