---
title: Coinbase（COIN）2022 Q3 季報，加密市場是否已見底
description: '美股投資 Coinbase COIN 介紹|Coinbase COIN 2022 Q3 第三季財報分析|第一家上市的加密貨幣 Crypto 交易所|加密貨幣 Crypto 大跌|加密貨幣 Crypto 美股'
date: 2022-12-02
tag: [Stock, COIN, Crypto]
category: Investment
---
從上篇[Coinbase（COIN）公布 2021 Q3 季報，揭秘加密貨幣的趨勢走向](https://ycjhuo.gitlab.io/blogs/COIN-Earnings-2021-Q3.html) <br />

到現在過了一年，Coinbase 的股價已隨著加密貨幣市場的低迷，每天都在挑戰歷史新低 <br />

今年以來，股價從 $251 → $40，跌幅達到 84% <br />

在遭逢 FTX 交易所破產，而再次重挫的加密貨幣產業，身為最早上市的交易所 Coinbase 對於加密貨幣的前景又是如何看待的呢？<br />
讓我們從 Coinbase Q3 的財報來一探究竟


## Coinbase 2022 Q3 財務指標
[資料來源](https://s27.q4cdn.com/397450999/files/doc_financials/2022/q3/Q32022-Shareholder-Letter.pdf)

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q3</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">與上季相比</th>
    <th align="center">與去年同期相比</th>
  </tr>
  <tr>
    <td align="center">總營收（千）</td>
    <td align="center">1,311,908</td>
    <td align="center">808,325</td>
    <td align="center">590,339</td>
    <td align="center">-39.25%</td>
    <td align="center">-53.32%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">84.96%</td>
    <td align="center">79.32%</td>
    <td align="center">82.74%</td>
    <td align="center">3.43%</td>
    <td align="center">-2.22%</td>
  </tr>
  <tr>
    <td align="center">營業利潤（千）</td>
    <td align="center">291,808</td>
    <td align="center">-1,044,389</td>
    <td align="center">-556,484</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>  
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">22.24%</td>
    <td align="center">-129.2%</td>
    <td align="center">-94.27%</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">1.62</td>
    <td align="center">-4.98</td>
    <td align="center">-2.43</td>
    <td align="center">N/A</td>
    <td align="center">N/A</td>
  </tr>
</table>

- 總營收：在 2021 Q2 達到高點的 22.3 億後，隨後開始急劇下滑，本季營收僅為當時的 26.5%
- 毛利率：經過前二季的短暫下滑後，本季毛利率回升到 83%，2021 時皆在 85% 左右
- 營業利潤：本季虧損了 5.6 億，比上季虧損的 10.4 億減少了約一半，但離實現獲利仍有段距離，目前已連續三個季度虧損
- 營業利潤率：本季營業利潤率為 -94%，上季好了很多，除了研發之外，其它方面的支出，都有顯著的降低
- EPS：受限於營業利潤的虧損，本季的 EPS 為 -2.43，2022 至今 EPS 為 -9.39

Coinbase 的營收從 2021 Q2 後就開始一路萎縮，而比特幣價格則是在 2021 Q3 到達高點 $64,400，隨後就開始一路下跌，這點跟 Coinbase 的股價如出一徹 <br />

下圖為 Coinbase 股價＆比特幣（BTC）近一年的走勢圖，可看到二者的連動性相當的高
![Coinbase vs. Bitcoin 近一年走勢圖](../images/201-01.png)

::: tip
跟 Q2 比較，本季 BTC 平均價格下降了 35%，而 ETH 則下跌 32%
:::

## Coinbase Q3 財報重點
Coinbase 在財報中不只一次提及對 Q4 以及明年的市場更保守的看法 <br />
並認為加密貨幣市場在明年仍會繼續萎縮，且萎縮的市場將會讓 Coinbase 面臨更艱難的處境 <br />
下表取自 Coinbase Q3 財報：<br />

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2022 Q3</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">月交易用戶數（百萬）</td>
    <td align="center">9.2</td>
    <td align="center">9</td>
    <td align="center">8.5</td>
    <td align="center">-2.2%</td>
    <td align="center">-5.6%</td>
  </tr>
  <tr>
    <td align="center">散戶交易量（十億）</td>
    <td align="center">74</td>
    <td align="center">46</td>
    <td align="center">26</td>
    <td align="center">-37.8%</td>
    <td align="center">-43.5%</td>
  </tr>
  <tr>
    <td align="center">機構交易量（十億）</td>
    <td align="center">235</td>
    <td align="center">171</td>
    <td align="center">133</td>
    <td align="center">-27.2%</td>
    <td align="center">-22.2%</td>
  </tr>  
</table>

可看出，不論是散戶或是機構，交易量衰退的幅度都相當的大 <br />
散戶交易量占總交易的比重更是從 2021 Q1 的 56%，下降到現在的 19.5%，散戶大軍影響市場的情況經已不復見 <br />
而這也驗證了 Coinbase 在財報中說的：散戶的行為已從上季的「交易導向」，在這季轉為「持有導向」<br />

除了這些壞消息，好消息的部分是，Coinbase 持續強化與企業的合作，藉此擺脫對於交易手續費為主要營收來源的依賴
- Coinbase 在 十月與 Google 建立戰略合作，Google Cloud 的客戶將可選擇加密貨幣為付款方式
- 世界上最大的資產管理公司黑石集團（BlackRock）達成合作，導入 Coinbase 的企業方案，為黑石的客戶提供加密貨幣的各種服務
- 推出 Coinbase One 訂閱服務，期望為公司帶來穩定現金流

## 後記
Coinbase 預計 Q4 的各項營業費用也會比這季稍高，也交易量仍會持續下降，但「月交易用戶數」可維持當前水準 <br />
因此可推算，在收入將減少的情況下，營業費用又無法降低的情況下，Coinbase 在下一季的虧損將會比這季更高 <br />
也難怪目前 Coinbase 的股價已來到歷史新低，加上目前處於美聯儲加息的市場下，Coinbase 的股價仍會繼續走低 <br />

若仍看好加密貨幣市場的投資者，在前景仍未見曙光的情況下，建議選擇分批買入的方式進場，來攤平買入成本

::: warning
點擊觀看更多： <br />
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
