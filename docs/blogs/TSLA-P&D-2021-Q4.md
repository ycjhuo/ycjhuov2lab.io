---
title: 特斯拉（TSLA）發布 2021 Q4 生產及交付量，股價大漲 13%
description: '美股特斯拉|特斯拉 TSLA|特斯拉 2021 Q4 財報|TSLA 2021 Q4 財報'
date: 2022-01-03
tag: [Stock, TSLA]
category: Investment
---

特斯拉在 01/02（日）公佈了第四季的生產 & 交車數量，在發佈後的隔天，也就是 2022 的第一個交易日（01/03），特斯拉股價跳漲 13.53%

在 Q3 財報發布（10/2）到 公佈 Q4 交車數量（1/2）的期間內，特斯拉股價上漲了 36.32%，而同期間 S&P 500 指數上升了 9.39%，也表示特斯拉在這段期間的漲幅約是 S&P 500 的 3.86 倍  <br/>

## 特斯拉 2021 Q4 生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">8,941</td>
    <td align="center">13,109</td>
    <td align="center">282.09%</td>
    <td align="center">46.62%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">228,882</td>
    <td align="center">292,731</td>
    <td align="center">12.15%</td>
    <td align="center">27.9%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">9,275</td>
    <td align="center">11,750</td>
    <td align="center">390.18%</td>
    <td align="center">26.49%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">232,102</td>
    <td align="center">296,850</td>
    <td align="center">16.39%</td>
    <td align="center">27.9%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">237,823</td>
    <td align="center">305,804</td>
    <td align="center">15.21%</td>
    <td align="center">28.6%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">241,391</td>
    <td align="center">308,660</td>
    <td align="center">19.91%</td>
    <td align="center">27.84%</td>
  </tr>
</table>

- Model S/X：身為特斯拉最高級的車款，在第二季末完成改款，且回歸生產線後，本季的生產量為 13,109，與 Q3 相比，成長了 46.62%；交付量則為 11,750，季成長 26.49%

- Model 3/Y：目前全球最熱銷的電動車，Q4 生產量已逼近 30 萬輛（292,731），季成長 27.9%（上季為 12.15%）；交付量為 296,850，季成長 27.9%

回想 Model 3/Y 在 Q2 時生產＆交付量才剛突破 20 萬，僅經過半年，在本季均已雙雙來到了 30 萬；若與年初的（Q1）的生產量比較，成長了 43%


## 特斯拉 2021 全年生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2020</th>
    <th align="center">2021</th>
    <th align="center">Chg（%）</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">54,805</td>
    <td align="center">24,390</td>
    <td align="center">-55.5%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">454,932</td>
    <td align="center">906,032</td>
    <td align="center">99.16%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">57,085</td>
    <td align="center">24,964</td>
    <td align="center">-56.27%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">442,562</td>
    <td align="center">911,208</td>
    <td align="center">182,847</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">509,737</td>
    <td align="center">930,422</td>
    <td align="center">82.53%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">499,647</td>
    <td align="center">936,172</td>
    <td align="center">87.37%</td>
  </tr>
</table>

- Model S/X：因為改款的原因，今年留給 Model S/X 生產的時間實際上僅有半年，交車＆交付量約為 25,000（實際數量為 24,390 & 24,964），導致年成長與去年相比，下降了 55.5%

- Model 3/Y：今年生產量為 906,032，與去年相比，成長 99.16%，交付量更是達到了 105.89%；2019 + 2020 二年的交付量加起來也僅有 74 萬

## 後記
本季度的交車量季成長分別為 46.62%（Model S/X） & 27.90%（Model 3/Y），連續三個季度的雙位數成長，多數人可能會想說這成長速度不可能延續，下一季成長率馬上就會降到個位數，但真的是這樣嗎？

Model 3/Y 在今年前二季成長分別為 13.17%	＆ 12.15%，當時我們可能以為成長率已趨於平穩，但這季仍能成長 27.9%（是 Q3 的二倍）

特斯拉在 Q3 財報揭露了位於歐洲的柏林工廠會在 2021 年底取得生產許可，若日期沒改變的話，今年 Model 3/Y 的生產量將很有可能再增加 100% 以上，實現年生產 200 萬輛的目標（而不只有柏林廠，德州廠也在建造中）

隨著 Musk 於 11 月初在推特宣布要賣股，特斯拉股價也在 12 月底來到近期最低的 $886，但過了三天馬上又重回 $1,000；儘管Musk 也提到 2022 & 2023 將會出現經濟衰退，但我認為有著強大生產能力為後盾的的特斯拉並不會受到太大影響