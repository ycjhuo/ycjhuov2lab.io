---
title: Coinbase（COIN）公布 2021 Q3 季報，揭秘加密貨幣的趨勢走向
description: '美股投資 Coinbase COIN 介紹|Coinbase COIN 2021 Q3 第三季財報分析|第一家上市的加密貨幣 Crypto 交易所|加密貨幣 Crypto 大跌|加密貨幣 Crypto 美股'
date: 2022-01-23
tag: [Stock, COIN, Crypto]
category: Investment
---

「挖金礦不如賣鏟子」，加密貨幣交易所 Coinbase（COIN）在 11/09 公佈了 2021 第三季的財報，從 2021 Q2 季報公佈（08/10 盤後），到公佈 Q3 季報（11/09 盤後），三個月的時間，股價從 $269.67 → $357.39，上漲了 32.53%（同期間，S&P 500 指數上漲 5.6%）<br />

可看出，在 Q3 期間， Coinbase 的股價遙遙領先大盤，下面就來分析 COIN 第三季財報，看看現在是否為買進的好機會

::: tip
截至 2021/09/30，Coinbase 平台上的總資產為 2550 億，與 Q2 （1800 億）相比，上升了 41.67% <br />
同時 Coinbase 平台上的加密貨幣也佔了整個市場的 12.2%（Q2 時為 11.2%）
:::

## Coinbase 2021 Q3 季報
[資料來源](https://investor.coinbase.com/financials/quarterly-results/default.aspx)

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">總營收（千）</td>
    <td align="center">1,801,112</td>
    <td align="center">2,227,962</td>
    <td align="center">1,311,908</td>
    <td align="center">23.7%</td>
    <td align="center">-41.12%</td>
  </tr>
  <tr>
    <td align="center">營業利潤（千）</td>
    <td align="center">987,713</td>
    <td align="center">874,725</td>
    <td align="center">291,808</td>
    <td align="center">-11.44%</td>
    <td align="center">-66.64%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">88.67%</td>
    <td align="center">91.25%</td>
    <td align="center">94.12%</td>
    <td align="center">2.58%</td>
    <td align="center">2.87%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">54.84%</td>
    <td align="center">39.26%</td>
    <td align="center">22.24%</td>
    <td align="center">-15.58%</td>
    <td align="center">-17.02%</td>
  </tr>
    <tr>
    <td align="center">每股淨收益（Diluted）</td>
    <td align="center">3.05</td>
    <td align="center">7.77</td>
    <td align="center">1.92</td>
    <td align="center">104.47%</td>
    <td align="center">-75.29%</td>
  </tr>
</table>

- 總營收：本季不僅無法重現 Q2 時（22.7 億）的高成長性（23.7%），Q3 營收（13 億）甚至只是 Q1 （18 億）的 7 成
- 營業利潤：從 9.88 億（Q1）→ 8.7 億（Q2） → 2.9 億（Q3），Q3 整體利潤僅為 Q1 的 3 成
- 毛利率：雖然營收大幅衰退，但毛利率卻是逐季提升，在 Q3 達到了驚人的 94.12%
- 營業利潤率：與毛利率相反，營業利潤率以每季 16% 的速度往下滑，從 Q1 的 55% → Q3 的 22%
- 每股淨收益：受限於營業利潤大幅下滑的因素，Q3 的每股淨收益約是 Q1 的一半

本季營收下降這麼多的原因，可歸類到因加密貨幣價格均處在低檔，投資人傾向保守且持有，造成交易熱度降低，以至於 Coinbase 喪失成長動能

而營業成本雖然比 Q2 減少了 25%（主要成本仍在於研發，佔了總成本的 35%），但因營收削減太多，造成營業利潤在本季下降了 67%

比特幣價格如下：
![152-01](../images/152-01.png)

接著來看看本季的營收＆利潤下降是否真的是受到比特幣＆市場影響呢？

## Coinbase Q3 關鍵指標

![Coinbase 2021 Q3 平台數據](../images/152-02.png)

- 交易量：與 Q2 相比，散戶在本季交易量（930 億）減少了 36%；機構（2340 億）則是減少了 26%，合計（3270 億）共少了 30%，與營收下降 40% 符合

- 交易種類：在 Q3，比特幣佔了 19%，以太幣為 22%，其它為 59%，可看出資金持續從主流的「比特幣/以太幣」流出，轉進其它加密貨幣

- 營收來源：與交易量總類一致，在本季，其他加密貨幣所帶來的收入已超過「比特幣＆以太幣」的總和

- 資產分佈：雖然本季的交易量大幅下滑，但散戶/機構在 Coinbase 上的資產卻有著 32%/51% 的增長，證明了在市場處於低檔時，投資人均採取持有，而非賣出的策略

- 資產種類：「比特幣」已連續四季減少，而這減少的份額均被其它加密貨幣所取代，顯示了在區塊鏈在快速的發展過程中，誕生出許多更為方便＆有效率的加密貨幣，並且也逐漸受到投資人的青睞，使得「比特幣」這個加密貨幣龍頭的光環正逐漸黯淡

## 後記
本篇在寫的時候，由於聚焦在財報的表現，僅考慮 Q2 - Q3 期間股價的變化，而截至今日（01/22），Coinbase 股價已從 Q3 財報發佈後，再度下跌 46.29%

目前股價（$191.97）也是 Coinbase 從 2020/04/14 IPO 以來的最低點（當時股價為 $328.28），表示若在 IPO 當天買入的投資人，目前報酬率為 -42%

但就公司體質看來，卻是越來越好：
- 交易量：從 2020 的 1,931 億，到目前的 1.12 兆（Q1 - Q3），暴增了 580%
- 資產規模：從 2020 年的 903 億，大幅上升到 2550 億，不到二年的時間，成長了 280%
- 淨收入：從 2020 的 3.22 億，到 2021 Q3 單季就為 4.06 億（ Q1 - Q3 總和為 27.8 億），增加了 863%

身為目前唯一上市的加密貨幣交易所，且平台上的加密貨幣佔據了整個市場的 12.2%，我認為若是想跟上加密貨幣風潮，但又不知該從哪種幣下手的話，Coinbase 無疑是最好的選擇