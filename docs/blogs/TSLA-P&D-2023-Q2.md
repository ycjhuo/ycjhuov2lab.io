---
title: 特斯拉（TSLA）發布 2023 Q2 生產及交付量，成長動能恢復，第一台 Cybertruck 成功生產
description: '美股特斯拉|特斯拉 TSLA|特斯拉生產交車量 2023 Q2|特斯拉 Model 3 生產交車量 2023 Q2 第二季|特斯拉 Model Y 生產交車量 2023 Q2 第二季'
date: 2023-07-17
tag: [Stock, TSLA]
category: Investment
---

特斯拉在 07/02（日）公佈了 2023 第二季的生產 & 交車數量

在 Q1 財報發布 → 公佈 2023 Q2 交車數量期間（4/2 → 7/2），特斯拉的股價上漲了 45%，而 S&P 500 上漲 7%  <br/>

而從財報公佈 → 現在（截至上週五），特斯拉股價繼續上漲了 7%，S&P 500 上漲 1%  <br/>
顯示了投資人在看到特斯拉 Q2 的交車量後，都對於即將公布的 Q2 財報有信心

在財報公佈的前三天，是否要搶先買入特斯拉，提前佈局呢？下面來看看特斯拉的 2023 Q2 生產及交付量

## 特斯拉 2023 Q2 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2023 Q1</th>
    <th align="center">2023 Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2023 Q1 vs. Q4</th>
    <th align="center">Q2 vs. Q1</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">19,437</td>
    <td align="center">19,489</td>
    <td align="center">3%</td>
    <td align="center">-6%</td>
    <td align="center">--</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">421,371</td>
    <td align="center">460,211</td>
    <td align="center">21%</td>
    <td align="center">1%</td>
    <td align="center">9%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">10,695</td>
    <td align="center">19,225</td>
    <td align="center">-24%</td>
    <td align="center">-24%</td>
    <td align="center">80%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">412,180</td>
    <td align="center">446,915</td>
    <td align="center">19%</td>
    <td align="center">6%</td>
    <td align="center">8%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">440,808</td>
    <td align="center">479,700</td>
    <td align="center">20%</td>
    <td align="center">--</td>
    <td align="center">9%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">422,875</td>
    <td align="center">466,140</td>
    <td align="center">17%</td>
    <td align="center">5%</td>
    <td align="center">10%</td>
  </tr>
</table>

2023 Q2 生產/交付量：
- Model S/X：產量與上季持平，約 1.9 萬輛
- Model S/X：本季約交付 1.9 萬輛，季成長達到驚人的 80%，創下史上最高的單季交付紀錄
- Model 3/Y：產量約 46 萬輛，季成長為 9%，相比上季幾近停滯的成長率，本季又恢復以往的成長動能
- Model 3/Y：交付量約 44.7 萬輛，季成長 8%，比上季的 6% 有著小幅度的成長

本季總產量約 48 萬輛車，比上季多 5 萬輛，季成長從上季的 0% 回升到 9%

總交付量方面，本季為 46.6 萬輛，季成長為 10%（上季為 5%），表示消費者對於特斯拉的需求仍很強勁

本以為特斯拉的產能已接近滿載，即將進入平穩發展的階段，沒想到本季又恢復了成長動能，帶來了接近 10% 的成長速度

## 特斯拉最大產能與毛利率
[上篇](https://ycjhuo.gitlab.io/blogs/TSLA-P&D-2023-Q1.html)提到：特斯拉每季度的產能為 47.5 萬輛，與本季相當，顯示出各個工廠都的產能都已到達滿載，那接下來要再提升產量，是不是就沒有這麼容易了呢？

所幸，2019 年發表的電動皮卡 Cybertruck，終於在今天（2023/7/16）生產出了第一輛車，預計在明年進入量產階段  <br/>

也就代表著特斯拉的產量將會在明年藉著 Cybertruck ，再次進入爆發期，並不會就此趨於平緩

在確認特斯拉在產能上，仍保有未來的成長性後，另一個重點就是毛利率了

特斯拉的毛利率一直維持著不錯的水準，2022 的平均毛利率為 29%，平均利潤為 52 億

但今年 Q1 時，由於特斯拉的降價策略，導致毛利率降到 19%，利潤也隨之減少到 45 億，
與去年的平均相比，毛利率＆利潤分別衰退了 10% & 16%

而這也將會是特斯拉在財報公布後，股價漲/大跌的關鍵

## 後記
特斯拉在 Q2 的生產/交車數量上表現良好，但我傾向於在財報公布後再買入，原因為：
- 若毛利率比 Q1 低，股價下跌的機會很高，有機會用比現在低的價格買進
- 若毛利率比 Q1 高，因為股價在近期已經漲了很多（近一個月上漲 10%，近三個月漲了 70%），再往上漲的幅度有限，就算到時再買入，成本也不會比現在進場高很多

特斯拉一直保持在正確的軌道上（搶攻市占率/開源充電標準），未來的發展潛力也很大（Cybertruck/Semi/自動駕駛），買進股票，並長期持有，要虧錢其實也不容易

但短期還是希望股價能因為削價競爭的關係，下跌一些，好讓投資人可用更低的成本買入

::: warning
點擊觀看更多：  <br/>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::