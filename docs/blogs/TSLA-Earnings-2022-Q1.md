---
title: 特斯拉（Tesla）2022 Q1 季報，連續成長五季的 Model 3/Y 交車量，首見衰退
description: '特斯拉 TSLA 財報|特斯拉 TSLA 2022 Q1 第一季財報|特斯拉 TSLA 生產量 交車量 2022 Q1|TSLA 特斯拉 拆股'
date: 2022-04-24
tag: [Stock, TSLA]
category: Investment
---

特斯拉在 04/20（三）公佈了 2022 第一季財報  <br/>
在 2021 Q4 財報發布（01/26）到 公佈 2022 Q1 財報（04/20）的期間內，特斯拉股價上漲了 4.24%，而同時 S&P 500 指數僅上漲 2.52%，特斯拉的股價已連續三個季度超越大盤

而在財報公佈後隔天，股價跳漲 10%，可惜後來受到聯準會加息的消息影響，將漲幅吐了回去，但這仍不影響我持續加碼特斯拉的決心

同時我持有特斯拉也正式邁入第六年，過程中從沒賣過任何一股，可惜由於將券商從 TD 轉到嘉信，以前的買入紀錄已找不到，所幸在嘉信上仍有買到特斯拉拆股前的紀錄

![特斯拉拆股](../images/173-01.png)

::: tip
更換券商的原因請見：[投資美股，最推薦的美股券商嘉信理財（Charles Schwab）](https://ycjhuo.gitlab.io/blogs/Brokerage-Account-Charles-Schwab.html)
:::

## 特斯拉 2022 Q1 財報
[來源](https://tesla-cdn.thron.com/static/IOSHZZ_TSLA_Q1_2022_Update_G9MOZE.pdf?xseo=&response-content-disposition=inline%3Bfilename%3D%22TSLA-Q1-2022-Update.pdf%22)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4</th>
    <th align="center">2022 Q1</th>
    <th align="center">2021 Q4 vs. 2021 Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
  </tr>
  <tr>
    <td align="center">電動車營收（百萬）</td>
    <td align="center">12,057</td>
    <td align="center">15,967</td>
    <td align="center">16,861</td>
    <td align="center">24.49%</td>
    <td align="center">5.3%</td>
  </tr>
  <tr>
    <td align="center">電動車利潤（百萬）</td>
    <td align="center">3,673</td>
    <td align="center">4,882</td>
    <td align="center">5,539</td>
    <td align="center">24.76%</td>
    <td align="center">11.86%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">30.5%</td>
    <td align="center">30.6%</td>
    <td align="center">32.9%</td>
    <td align="center">0.1%</td>
    <td align="center">2.3%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">14.6%</td>
    <td align="center">14.7%</td>
    <td align="center">19.2%</td>
    <td align="center">0.1%</td>
    <td align="center">4.5%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">1.44</td>
    <td align="center">2.05</td>
    <td align="center">2.86</td>
    <td align="center">29.76%</td>
    <td align="center">28.32%</td>
  </tr>
</table>


- 電動車營收：與去年 Q4 相比，僅上漲了 5.3%，結束了連續三個季度的雙位數成長率（上季為 24.49%） <br/>
原因是身為特斯拉主力車型的 Model 3/Y 交車量，對比上季小幅降低了 0.5%，在本季之前，Model 3/Y 的交車量約逐季成長 16%
- 電動車利潤：從本季的利潤的成長幅度（11.86%） > 營收（5.3%），可看出特斯拉在毛利上有著一定幅度的上升，才能在總交車量略低於上季的情況下，將利潤持續拉高
- 毛利率：在本季來到歷史上最高的 32.9%，已連續三季保持在 30% 以上；提升的關鍵在於，身為高價車款的 Model S/X 在前二季，僅佔總交車量的 3.85% 左右，但本季 Model S/X 卻上升到 4.75%
- 營業利益率：同樣來到歷史新高的 19.2%，相比前二季，扣掉毛利率上升的 2.3%，以及本季特斯拉由於銷售＆行政費用的大幅縮減（約 5 億美元），讓營業利益率一舉再上升 4.5%
- EPS：因前幾項指標帶來的好成績，本季 EPS 為 2.86，比上個季度成長了 28.32%


現在的特斯拉，每次季報一公佈都是在創下新的紀錄，一季比一季更好，而這季的好成績可歸類為二點：
1. 毛利率方面，由於高價車款的 Model S/X 在總交車量中的佔比，在前幾季中都是保持一定的比例，但這季在 Model 3/Y 交車量變化不大的情況下，Model S/X 的交車量大幅成長了 25%，導致了 Model S/X 的佔比上升，較高的售價也將毛利率也一起拉高了

2. 營益率方面，由於上季支出予 CEO 的股權協議影響下，造成銷售＆行政費用相比前幾季大幅上升了 33.5%，但這個費用在本季又回歸到原本的數值（約 1 億），在少了這個因素，以及毛利率上升 2% 情況下，推升了營益率在本季上升 4.5%，其實已不足為奇

分析完特斯拉亮眼的財報後，再來看看特斯拉 2022 Q1 的生產/交付量



## 特斯拉 2022 Q1 生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q4</th>
    <th align="center">2022 Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">13,109</td>
    <td align="center">14,218</td>
    <td align="center">282.09%</td>
    <td align="center">46.62%</td>
    <td align="center">8.46%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">292,731</td>
    <td align="center">291,189</td>
    <td align="center">12.15%</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">11,750</td>
    <td align="center">14,724</td>
    <td align="center">390.18%</td>
    <td align="center">26.49%</td>
    <td align="center">25.14%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">296,850</td>
    <td align="center">295,324</td>
    <td align="center">16.39%</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">305,804</td>
    <td align="center">305,407</td>
    <td align="center">15.21%</td>
    <td align="center">28.6%</td>
    <td align="center">-0.14%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">308,660</td>
    <td align="center">310,048</td>
    <td align="center">19.91%</td>
    <td align="center">27.84%</td>
    <td align="center">0.45%</td>
  </tr>
</table>

本季度的生產/交付量：

- Model S/X：身為特斯拉的高價車款， Model S/X 也是本季生產＆交車量唯一成長的車型，在總交車數量一直保持接近 4% 的比重，在本季一舉增加了 1%，來到了快 5%，未來若能佔據更大比重，特斯拉的毛利則能再繼續提升

- Model 3/Y：受限於原物料＆供應商均不足的情況下，本季生產量小幅降低 0.5%，對比前幾季最低成長 10% 的成長率來看，是否代表著 Model 3/Y 的生產量已到達上限？

本季財報揭露了，德國柏林工廠＆德州工廠均已開幕，正式進入了生產狀態，可以預見在這二個工廠的幫助下，Model Y 的產量將在下季恢復先前的成長動能，而上海工廠因疫情因素，而小幅停工的影響也將被完全掃除


## 後記
除了上面介紹的電動車業務外，特斯拉在本次財報中也提到了另外二件事：
1. 服務業務的毛利率已從 2018 年的 -35%，逐漸上升到現在的 -0.6%，將在特斯拉使用者持續增長的情況下達到損益二平
2. 保險業務在本季又在三個州推出了，目前已有 8 個州可以使用特斯拉的保險業務，已佔了全美 50 州的 16%

雖說特斯拉的股價在最近聯準會的加息下，受到市場情況下跌的影響，在今年表現不佳（截至目前仍下跌 16%），但我仍認為即使是在市場前景不佳的情況下，仍是繼續買進特斯拉的好機會，並趁著股價尚未再次一飛衝天時，持續累積手上的特斯拉股數


::: warning
點擊觀看更多：  <br/>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
