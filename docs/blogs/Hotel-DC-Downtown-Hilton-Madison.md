---
title: 開箱！華盛頓 DC-希爾頓麥迪遜飯店（The Madison Washington DC）
description: '華盛頓 DC 飯店介紹|華盛頓 DC 飯店推薦|華盛頓 DC 飯店|華盛頓 DC 飯店 CP 值|華盛頓 DC 希爾頓 Hilton 介紹'
date: 2022-01-17
tag: [Hotel, Hilton, Washington DC]
category: Travel
---
::: tip
入住時間：2022/01/15 - 01/17 <br />
房型：1 King Bed Deluxe Room（但入住後升級成 Executive Room 1 King Bed）<br />
每晚價格（稅後）：$194.27 / 41,000 點 <br />
Google 評價：4.2 顆星（1,670 評價）
:::

自從 Hilton 從 2021/10 開始會針對金卡、鑽卡會員在入住的 72 小時內發送升級通知後，一直很期待什麼時候可以體驗到 <br />
沒想到這個機會來的這麼快，在入住的前三天，收到了主旨為：We're upgrading your upcoming stay 的信（但信上並不會說升到哪個房型） <br />

這次是透過美國運通 THC（Amex Travel The Hotel Collection）平台訂了華盛頓 DC 的 Hilton Madison（在 2020/12 訂的，剛好把白金卡 2020 年的 $200 Hotel Credit 用掉）

![Hilton 升級通知信](../images/150-00.png)

這次入住的 The Madison Washington DC，位於華盛頓 DC 的 Downtown，走到白宮約 10 分鐘，之後再往南走 15 分鐘可到華盛頓紀念碑（Washington Monument），距離各景點的位置不算近，若跟我一樣是冬天來的話，就會覺得 Madison 的地點不是很好，但若夏天來的話，則還可以接受
![Hilton Madison 位置](../images/150-01.png)

下面就來看看華盛頓 DC-希爾頓麥迪遜飯店（The Madison Washington DC） 的介紹吧

## 大廳
The Madison Washington DC 共有 15 樓，每層約 40 個房間，大樓算新，裡面有星巴克、餐廳、酒吧 <br />
進門後，左邊是電梯，右邊是 Check-In 櫃檯，在更右邊則是星巴克/零食櫃、以及餐廳/酒吧

![The Madison Washington DC Lobby](../images/150-02.jpg)
![The Madison Washington DC Lobby](../images/150-03.jpg)

## 房間
這次入住位於 12 樓的 Executive Room，聽到名字後，想說肯定是個不錯的房型，但開門後只能說現實是殘酷的 <br />
房間超小，除了一張床，僅有一張單人沙發＆辦公桌 <br />
![The Madison Washington DC Executive Room](../images/150-04.jpg)
![The Madison Washington DC Executive Room](../images/150-05.jpg)
![The Madison Washington DC Executive Room](../images/150-06.jpg)

房間沒有微波爐＆小冰箱，取而代之的是「動到就要付費」的小飲料櫃＆點心櫃
![The Madison Washington DC Executive Room](../images/150-07.jpg)
![The Madison Washington DC Executive Room](../images/150-08.jpg)


## 衛浴
浴室也很小，馬桶與門之間幾乎沒有縫隙，但因化妝台的鏡子會發光，讓浴室變得很明亮 <br />
備品用的是與 DoubleTree 品牌一樣的知名護膚品牌瑰柏翠（Crabtree & Evelyn）
![The Madison Washington DC Bathroom](../images/150-09.jpg)
![The Madison Washington DC Bathroom](../images/150-10.jpg)
![The Madison Washington DC Bathroom](../images/150-11.jpg)

## 餐廳
在 Check-In 櫃檯右邊有星巴克/零食櫃（Marketplace），下圖因為非營業時間，因此有用門將星巴克隔開 <br />
這裡除了一般的洋芋片之外，也有沙拉、三明治、飲料等等（金卡會員送的 $30 Credit 可以用在這邊＆星巴克）<br />
早上也有 $14.9（未稅）的 Buffet 可以吃，但雖說是 Buffet，但只有炒蛋、培根、馬鈴薯三種而已
![The Madison Washington DC Restaurant](../images/150-12.jpg)
![The Madison Washington DC Restaurant](../images/150-13.jpg)
![The Madison Washington DC Restaurant](../images/150-14.jpg)

飯店內的餐廳（Lady Madison）營業時間從 16:00 - 23:00，價位不算高，主餐約在 $25- $30左右，酒類一杯約 $12 - $16
這次因為是透過 THC 訂的，飯店還送了 $100 的 Dining credit，我們就把它用在這邊
![The Madison Washington DC Restaurant](../images/150-15.jpg)
![The Madison Washington DC Restaurant](../images/150-16.jpg)
![The Madison Washington DC Restaurant](../images/150-17.png)

從左到右分別為：Short Ribs、Classic American Cheeseburger、Maryland Crab Soup、Margherita Flatbread
![The Madison Washington DC Restaurant](../images/150-18.png)

## 後記
The Madison Washington DC 雖然房型偏小，但建築本身蠻新的，且 Madison 屬於在 THC 平台上可以訂到的最便宜飯店（在 DC）<br />

二晚 $389（稅後）的房價，扣掉 $100 的 Dining Credit ＆每晚的 $30 早餐退額，實際支出僅剩 $229 <br />
最後再扣掉 AMEX 白金卡的每年 $200 Hotel Credit，只花了 $29 就可以在 DC 玩二天，可以說是玩得相當值得 <br />

但也可能是這次的入住日期（1/15 - 1/17）算是淡季，若是將日期改為三月底 - 四月初的櫻花季，這間 The Madison Washington DC 一晚的房價就會從現在的 $189 上漲到 $ 408，整整上漲了二倍

但若沒透過 THC 平台訂房＆這些福利的話，同樣價位我會選擇萬豪的萬麗酒店「Renaissance Washington, DC Downtown Hotel」，主要原因是：距離各景點＆商業區較近，入夜後，附近也有很多商店可逛