---
title: 洛杉磯 Downtown 天使鐵路（Angels Flight Railway）- La La Land（樂來越愛你）拍攝景點
description: 'LA 景點|LA 推薦|洛杉磯 推薦|洛杉磯 景點|天使鐵路 介紹|La La Land 景點|LA La La Land|樂來樂愛你 景點|Angels Flight Railway 介紹'
date: 2021-10-29
tag: [Los Angeles]
category: Travel
---

上篇 [洛杉磯 Downtown 一日遊 天使鐵路 中央市場（Grand Central Market）最後一間書店（The Last Bookstore）](https://ycjhuo.gitlab.io/blogs/Tour-LA-downtown.html) 介紹到「天使鐵路」（Angels Flight Railway），這個因電影 La La Land（樂來越愛你）而一砲而紅的景點 <br />

世界上最短，全長僅 96 公尺的天使鐵路，因為行駛的路段是上坡，所以其實有點像在搭纜車 <br />
共有二個鐵軌 ＋ 二台火車（纜車）來回跑，他們還有各自的名字，分別為：Olivet & Sinai <br />
票價為 $1 美元（單程）（若刷卡的話則要 $1.5），因此若要搭上去，再搭下來的話，則要 $2（都在上面那端收），因此一開始要搭乘時可直接進去 <br />

下圖是要搭乘天使鐵路上去前：
![洛杉磯 LA 天使鐵路](../images/130-02.jpg)

車廂內部，最多可容納 33 人
![洛杉磯 LA 天使鐵路](../images/131-01.jpg)

二台列車，上下來回行駛中
![洛杉磯 LA 天使鐵路](../images/131-02.jpg)

搭上來後，出站後可看到的車站外觀
![洛杉磯 LA 天使鐵路](../images/131-03.jpg)

搭上來後有一個大廣場，但其實就是一般的商辦＆住宅區
![洛杉磯 LA 天使鐵路](../images/131-04.jpg)


延伸閱讀：<br />
[洛杉磯 Downtown 一日遊 天使鐵路（Angels Flight Railway） 中央市場（Grand Central Market）最後一間書店（The Last Bookstore）](https://ycjhuo.gitlab.io/blogs/Tour-LA-downtown.html)