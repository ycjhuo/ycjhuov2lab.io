---
title: '如何在 VuePress 加入 Google AdSense ( 以 RECO 主題為例 )'
description: 'VuePress AdSense|VuePress RECO|RECO AdSense|VuePress Google 廣告|RECO Google 廣告|VuePress 加入 AdSense|VuePress AdSense 教學'
date: 2021-11-12
tag: [VuePress, Google AdSense]
category: Blog
---

## 安裝套件
用 npm 或 yarn 安裝 Google AdSense 套件，安裝完後在 package.json 可以看到 Google AdSense 已被加進 devDependencies

```powershell
# 用 npm 安裝
$ npm i vuepress-plugin-google-adsense -D

# 用 yarn 安裝
$ yarn add vuepress-plugin-google-adsense
```

```javascript
"devDependencies": {
    "vuepress-plugin-google-adsense": "^0.2.1",
  },
```

## 建立 AdSense Vue
建立一個 AdSense.vue 放在 ```.vuepress/theme/components/AdSense.vue``` 如下：

``` vue
<template>
  <div class="adsense-content">
    <div style="padding: 0 1.5rem;">
      <Adsense
        data-ad-client="ca-pub-XXXXXXXXXXXXXXXX"
        data-ad-slot="XXXXXXXXXX">
      </Adsense>
    </div>
  </div>
</template>

<script>
export default {
  name: 'AdSense'
}

</script>
<style scoped>
  .adsense-content {
    max-width: 740px;
    margin: 0 auto;
  }
  .adsense-title {
    color: #999;
    transition: color 0.15s ease;
    cursor: pointer;
    font-size: 1.1em;
    font-weight: bold;
    padding: 0 1.5rem;
    margin-top: 0;
    margin-bottom: 1rem;
  }
</style>
```

- data-ad-client：就是 Google AdSende -> Account Information 裡面的 Publisher ID
- data-ad-slot：在 Google AdSende 左側的 Ads -> By ad unit 建立一個 In-article ads，即可得到一組 10 位數的數字

![Google AdSense In-article ads](../images/134-01.png) <br/>

## 在頁面中插入 AdSense Vue
需要加入 AdSense Vue 的有二個檔案：```enhanceApp.js``` & ```Page.vue```

### enhanceApp.js
位於 ```.vuepress/theme/enhanceApp.js```

```js{7,18-20}
/* eslint-disable no-proto */
import postMixin from '@theme/mixins/posts'
import localMixin from '@theme/mixins/locales'
import { addLinkToHead, addScriptToHead } from '@theme/helpers/utils'
import { registerCodeThemeCss, interceptRouterError, fixRouterError404 } from '@theme/helpers/other'
import { install } from 'vue-demi'
import Ads from 'vue-google-adsense'
export default ({
  Vue,
  siteData,
  isServer,
  router
}) => {
  install(Vue)
  Vue.mixin(postMixin)
  Vue.mixin(localMixin)
  Vue.use(require('vue-script2'))
  Vue.use(Ads.Adsense)
  Vue.use(Ads.InArticleAdsense)
  Vue.use(Ads.InFeedAdsense)

  if (!isServer) {
    addLinkToHead('//at.alicdn.com/t/font_1030519_2ciwdtb4x65.css')
    addScriptToHead('//kit.fontawesome.com/51b01de608.js')
    registerCodeThemeCss(siteData.themeConfig.codeTheme)
  }

  interceptRouterError(router)
  fixRouterError404(router)
}
```
在 Google AdSense 中，文章插入廣告的方式有很多種，我們雖然插入了 Adsense，In-Article-Adsense，In-Feed-Adsense 三種  <br/>
但我們並未在 AdSense 中註冊 In-Feed-Adsense，所以第 20 行可加可不加

### Page.vue
位於 ```.vuepress/theme/components/Page.vue```，我們在 ```template``` 中加入了一段顯示 AdSense 的 ```ModuleTransition```  <br/>

同時，在 script 區塊中，也須在 ```import``` 與 ```export``` 中加入 ```AdSense```

```vue {2-6,14,17}
<template>
<ModuleTransition class="comments-wrapper">
      <div>
         <AdSense />
      </div>
    </ModuleTransition>
</template>    
<script>
import { defineComponent, computed, getCurrentInstance, toRefs } from 'vue-demi'
import PageInfo from '@theme/components/PageInfo'
import { resolvePage, outboundRE, endingSlashRE } from '@theme/helpers/utils'
import { ModuleTransition } from '@vuepress-reco/core/lib/components'
import SubSidebar from '@theme/components/SubSidebar'
import AdSense from '@theme/components/AdSense'

export default defineComponent({
  components: { PageInfo, ModuleTransition, SubSidebar, AdSense },

})
</script>
```
## 後記
之後就可以在文章下方看到廣告囉  <br/>
![Google AdSense In-article ads](../images/134-02.png)  <br/>

延伸閱讀： <br/>
[如何在 VuePress 加入 Disqus 及 LikeCoin Button ( 以 RECO 主題為例 )](https://ycjhuo.gitlab.io/blogs/VuePress-Add-Disqus-LikeCoin.html)