---
title: 加州一定要租車嗎？洛杉磯自由行只靠 Uber、Lyft 要多少錢
description: '加州 租車|洛杉磯 租車|LA 租車|LA 景點|LA 推薦|洛杉磯 推薦|洛杉磯 景點|LA Uber Lyft|加州 Uber Lyft|洛杉磯 Uber Lyft'
date: 2021-12-24
tag: [Los Angeles]
category: Travel
---

這次在洛杉磯五天四夜，行程從洛杉磯國際機場（LAX Airport）開始，涵蓋洛杉磯各個知名景點 <br />
如：Downtown、蓋蔕博物館（The Getty）、天文台（Griffith Observatory）、好萊塢星光大道（Hollywood Walk of Fame）、Disney（Anaheim）等

洛杉磯（LA）並不像紐約的大眾運輸工具一樣發達，且景點又都很分散，因此若沒有車，若僅靠大眾交通工具的話，會耗費許多時間

這篇就來比較租車與 Rideshare（ Lyft / Uber ） 哪個划算吧，下圖是這次在 LA 自由行去的景點

![LA 自由行景點](../images/144-01.png)

## 洛杉磯（LA）Uber 費用
下表是這次搭乘 Uber 的費用，總共 12 趟，總計為 $415.4 <br />
雖然說是五天四夜，但其實只有其中二天比較常搭，有一天在迪士尼玩，從飯店走到迪士尼只要 15 分鐘

<table style="width:100%">
  <tr>
    <th align="center">Date</th>
    <th align="center">Time</th>
    <th align="center">Payment</th>
    <th align="center">From</th>
    <th align="center">To</th>
    <th align="center">RideShare</th>
  </tr>
  <tr>
    <td align="center">2021/09/24</td>
    <td align="center">18:17</td>
    <td align="center">$96.95</td>
    <td align="center">LAX Airport</td>
    <td align="center">Downtown DoubleTree</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/25</td>
    <td align="center">13:57</td>
    <td align="center">$53.96</td>
    <td align="center">Downtown DoubleTree</td>
    <td align="center">The Getty</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/25</td>
    <td align="center">16:41</td>
    <td align="center">$27.9</td>
    <td align="center">The Getty</td>
    <td align="center">The Original Farmers Market</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/25</td>
    <td align="center">18:50</td>
    <td align="center">$19.98</td>
    <td align="center">The Original Farmers Market</td>
    <td align="center">Public Art "Urban Light"</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/25</td>
    <td align="center">19:50</td>
    <td align="center">$27.92</td>
    <td align="center">Public Art "Urban Light"</td>
    <td align="center">Downtown DoubleTree</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/26</td>
    <td align="center">10:25</td>
    <td align="center">$32.95</td>
    <td align="center">Downtown DoubleTree</td>
    <td align="center">Griffith Observatory</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/26</td>
    <td align="center">13:30</td>
    <td align="center">$25.91</td>
    <td align="center">4520 North Virgil Avenue</td>
    <td align="center">Hollywood Walk of Fame</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/26</td>
    <td align="center">15:29</td>
    <td align="center">$19.74</td>
    <td align="center">In n out ( Hollywood Walk )</td>
    <td align="center">Downtown DoubleTree</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/26</td>
    <td align="center">16:46</td>
    <td align="center">$43.21</td>
    <td align="center">Downtown DoubleTree</td>
    <td align="center">Anaheim DoubleTree</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/27</td>
    <td align="center">10:17</td>
    <td align="center">$14.47</td>
    <td align="center">Anaheim DoubleTree</td>
    <td align="center">Knott's Berry Farm</td>
    <td align="center">Uber</td>
  </tr>
  <tr>
    <td align="center">2021/09/27</td>
    <td align="center">16:38</td>
    <td align="center">$12.51</td>
    <td align="center">Knott's Berry Farm</td>
    <td align="center">Anaheim DoubleTree</td>
    <td align="center">Lyft</td>
  </tr>
  <tr>
    <td align="center">2021/09/27</td>
    <td align="center">11:13</td>
    <td align="center">$39.9</td>
    <td align="center">Anaheim DoubleTree</td>
    <td align="center">LAX Airpot</td>
    <td align="center">Lyft（這時 Uber 是 $56.96）</td>
  </tr>
</table>

表中可以看到，後面幾次開始改用 Lyft，這是因為在比較之後，發現 Lyft 在洛杉磯真的比 Uber 便宜，若是再早點發現的話，總體價格應該可以再減少 20% - 30% 左右，也就是 $290 - 330 左右

::: tip
若是在短期間有多次搭乘 Lyft 的需求，也可以在 App 內升級成 Lyft Pink 會員，一個月的費用僅要 $19.99 <br />
但卻可以在每趟行程都享有 15% 的車費折扣

若還沒有 Lyft 會員的人，可以用我的 [推薦連結](https://ride.lyft.com/invite/YC26966?utm_medium=p2pa_iacc)，註冊後可以得到 $15 的 Lyft Credit
:::

## 洛杉磯（LA）租車費用
下圖是我在赫茲（Hertz）租車（五天四夜）的費用，可以看到費用總共為 $679.05，車型是 4 人座的馬自達（Mazda 3）

![LA 租車費用](../images/144-02.png)

租車的費用跟搭 Uber 比起來，多了約 63% <br />
而因為我們其實只有二天比較常搭 Uber，因此若是再將租車的天數扣掉二天的費用（$180.52），租車仍是貴了 Uber 20% 左右 <br />

以上這些租車的費用還沒加上停車費＆油錢

## 後記
雖然單純以費用的角度來看，Lyft & Uber 是真的比租車便宜，但租車的話，其實還可以少了叫車＆等車的時間（但也要再加上取車＆還車的時間）

在 LA，叫車的時間不像紐約只要 10 分鐘以內即可，以我這次叫車的經驗，從按下叫車，約要等候 5 - 10 分鐘，才會叫到車，而再等車開過來，還須再等 10 - 20 分鐘不等（在 The Getty 時則是要 30 分鐘）

不過儘管有這些缺點，但若讓我再選一次，我仍會選擇 Rideshare 作為往返各景點的選擇


若想看更多洛杉磯的介紹，可點選文章上方的 Los Angeles Tag， 或是 [Los Angeles](https://ycjhuo.gitlab.io/tag/Los%20Angeles/) 