---
title: Noise.Cash 連接到 Bitcoin.com 錢包教學
description: 'Noise.Cash 介紹|Noise.Cash 教學|Noise.Cash 錢包|BCH 錢包|Bitcoin Cash 錢包|Noise.Cash 推薦錢包|Noise.Cash 轉出|Noise.Cash 出金|Bitcoin Cash 教學|使用 Bitcoin Cash 錢包'
date: 2022-03-09
tag: [Noise.Cash, SocialFi, Crypto]
category: Investment
---

隨著加密貨幣的風潮，新型態的社群平台 SocialFi 也逐漸興起 <br />
什麼是 SocialFi 呢？簡單的說與我們普遍在使用的 Facebook，Twitter 等社交平台一樣 <br />
唯一的差別就是 SocialFi，也就是 Social Finance ，可以讓使用者們在平台上賺取加密貨幣

賺取加密貨幣？怎麼賺呢？使用者在 SocialFi 平台上只要透過 發文、回文等動作，平台就會自動發送加密貨幣給你

這篇來介紹如何把在 Noise.Cash 這個 SocialFi 平台上賺到的 BCH ( Bitcoin Cash ) 轉到 Bitcoin.com 錢包吧

## 註冊 Bitcoin.com
1. 在 Apple Store 或 Google Play 商店下載好 Bitcoin.com 錢包後，開啟 Bitcoin.com App

![Noise.Cash](../images/165-0.png)

2. 點選右下方的齒輪圖示，再點擊上方的 Secure your funds ，就可以用 Google 帳號來註冊一個 Bitcoin.com 的帳號

3. 接著設定一組密碼，這個密碼可以跟 Google 帳號不同，是 Bitcoin.com 專用的密碼

![Noise.Cash](../images/165-1.png)

## 在 Bitcoin.com 上創建 BCH 錢包地址
1. 註冊完後，點選左下角的首頁按鈕，再點擊 RECEIVE
2. 在 RECEIVE 頁面，我們選擇 BCH 來取得我們在 Bitcoin.com 錢包的 BCH 地址
3. 接著出現的 QR code 以及下方的一串代碼就是我們在 Bitcoin.com 錢包上的 BCH 地址，點擊 Copy 來複製這個地址

![Noise.Cash](../images/165-2.png)

# 從 Noise.Cash 傳送 BCH 到 Bitcoin.com
1. 打開 Noise.Cash 的 Wallet 頁面，點擊 Pay 跳轉到付款頁面
2. 在付款頁面的 Where To 那邊，貼上剛剛複製的那串 Bitcoin.com 錢包的 BCH 地址
3. 輸入要傳送的金額，再點擊 Pay 即可將 Noise.Cash 上的 BCH 傳送到 Bitcoin.com 錢包上

![Noise.Cash](../images/165-3.png)

4. 回到 Bitcoin.com 錢包，就可以看到剛剛傳送的交易已成功

![Noise.Cash](../images/165-4.png)


## 後記
在上面的步驟中，我從 Noise.Cash 傳送了 0.06664095 的 BCH 到 Bitcoin.com 錢包，之後在 Bitcoin.com 錢包上，也看了我的 BCH 是 0.06664095 <br />
這證明了從 Noise.Cash 出金到 Bitcoin.com 錢包是不用手續費的，而把 BCH 送到了 Bitcoin.com 後，我們就可以選擇有支援 BCH 的交易所把 BCH 轉成 USD 然後匯出到我們的銀行戶頭囉

若想知道怎麼把 BCH 從 Bitcoin.com 轉出到交易所，可參考這篇 [從 Bitcoin.com 錢包轉出 BCH 到 Gemini 交易所](https://ycjhuo.gitlab.io/blogs/Bitcoin-Wallet-To-Gemini.html)