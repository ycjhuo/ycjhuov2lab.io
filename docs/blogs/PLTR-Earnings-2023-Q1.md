---
title: Palantir（PLTR）2023 Q1 財報｜股價飆漲 100%，交出史上最好的成績單
description: '美股投資 Palantir PLTR 2023 Q1 財報分析|AI 大數據分析的顧問公司|2023 AI 戰爭概念股|Palantir PLTR 買點｜Palantir PLTR 買入｜Palantir PLTR IPO 價格'
date: 2023-05-26
tag: [Stock, PLTR]
category: Investment
---

Palantir（PLTR）在 5/8 公布了 2023 Q1 的財報，在 2022 Q4 → 2023 Q1 這段期間（2/13 → 5/8），Palantir 股價上漲了 1.7%，而 S&P 500 則是持平

乍看之下差不多，但若是時間拉長到今年以來的表現，截至今日（5/25），PLTR 上漲了 101%，而 S&P 500 僅上漲 9%

而其實，PLTR 主要上漲的時間點，是在本季財報公布後，也就是在 5/9 → 今日（5/25），PLTR 股價從 $7.74 → $12.84，短短 13 個交易日，股價飆漲了 66%

下面就來看看 PLTR 2023 Q1 的財報，找出大漲的原因！

## Palantir 2023 Q1 財務指標
[來源](https://investors.palantir.com/files/Palantir%20Q1%202023%20Business%20Update.pdf)：

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q4</th>
    <th align="center">2023 Q1</th>
    <th align="center">2023 Q1 季成長</th>
    <th align="center">2022 Q4 年成長</th>
  </tr>
  <tr>
    <td align="center">營收</td>
    <td align="center">5.1 億</td>
    <td align="center">5.3 億</td>
    <td align="center">3%</td>
    <td align="center">24%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">79.5%</td>
    <td align="center">79.5%</td>
    <td align="center">--</td>
    <td align="center">0.6%</td>
  </tr>
    <tr>
    <td align="center">營業利潤率</td>
    <td align="center">-3.5%</td>
    <td align="center">1%</td>
    <td align="center">4.3%</td>
    <td align="center">18.2%</td>
  </tr>
  <tr>
    <td align="center">EPS（diluted）</td>
    <td align="center">0.01</td>
    <td align="center">0.01</td>
    <td align="center">--</td>
    <td align="center">-33%</td>
  </tr>
</table>

- 營收：2023 Q1 的營收為 5.25 億，季增 3%，而 2022 的總營收為 19 億，年增 24%
- 毛利率：2023 Q1 的毛利率為 79.5%，與上季持平，比 2022 平均毛利率高出 1%
- 營業利潤率：2023 Q1 營利率為 1%，是公司首次實現營運獲利，而 2022 的平均營利率僅為 -8.5%
- EPS：本季 EPS 為 0.01，與上個季度持平，為連續二季度的正收益，2022 全年 EPS 為 -0.18

Palantir 在本季接續上個季度的好成績，本季 EPS 一樣保持在 0.01，不同的是，上個季度是依靠額外收入來達成正收益，而本季是在本身的營運模式下實現正收益（營業利潤率 1%）

Palantir 從 2021 到現在，毛利率皆維持在 78% - 80%，而最近二季實現轉虧為盈的關鍵是營收增長速度（年增 23%） > 營運開銷（年增 3%）

而 2023 Q1 除了研發費用季增 10% 之外，行銷＆行政費用分別季減 2% ＆ 9%，總體營運費用季減 2%

::: tip
Palantir 的各項營運費用占比為：行銷 45%、行政 33%、研發 22%
:::

## Palantir 2023 Q1 財報重點
Palantir 在本季達成連續二季的正 EPS，並且是公司首次達到營運面獲利，且 CEO 在財報揭露今年，Palantir 會保持季季獲利的表現

Palantir 本季的營收達到 5.3 億（年增率為 18%），其中美國市場佔了 64%（年增 23%），客戶群細分如下：
- 商業客戶佔了 45%（年增 15%），其中美國市場佔了 45%（年增 26%）
- 政府單位佔了 55%（年增 20%），其中美國市場佔了 80%（年增 22%）

目前政府單位仍是佔了營收的一半以上，且又以美國政府為主，雖說要擴展他國的政府單位業務，在推廣上會比商業客戶來得難，但政府單位有著較為穩定的優點，一但成為客戶，就能成為 Palantir 穩定的營收來源

本季的營收年成長為 18%，財報預估 Q2 營收約為 5.28 - 5.32 億，季增 1%（本季為 3%）<br />
而 2023 全年的營收可達到 22 億，年增 15%

::: tip
目前 Palantir 的商業客戶總數為 280，政府單位為 111，總客戶數年增 41%，季增 7%
:::

## 後記
Palantir 在本季交出了史上最好的財報，不僅各項指標都呈現正成長，且還轉虧為盈 <br />
也難怪股價會在財報發佈後大暴漲，短短二個星期就漲了 66%，相比 2022 整年的報酬率 -61%，可說是絕地反攻 <br />

那這樣是否代表現在 Palantir 可以安心買進了呢？ <br />
由於 Palantir 的薪酬制度，讓股票數量稀釋的很快，在 2020 ＆ 2021 分別增加了 70% ＆ 96%，但好在這個狀況在 2022 已減緩到 7% <br />

目前 Palantir 的股價若要回到 2021/01 時的高點（$35.18），還須上漲 274%，但考慮到今年 Palantir 的表現（已上漲了 101%），在財報季季獲利的加持 ＆ 現在 AI 的熱潮下，要突破之前的高點，似乎也不無可能

::: warning
點擊觀看更多： <br />
- [Palantir（PLTR）分析文](https://ycjhuo.gitlab.io/tag/PLTR/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
