---
title: 疫情間的西雅圖三天二夜（Day 2）瑞尼爾山 The Spheres 太空針塔 凱利公園
description: '西雅圖 景點|西雅圖 推薦|西雅圖 介紹|西雅圖 瑞尼爾山|Seattle 景點|Seattle 推薦|Seattle 介紹|Seattle 三天二夜|西雅圖 三天二夜|西雅圖 Mount Rainier|Mount Rainier 推薦|西雅圖 瑞尼爾 推薦|西雅圖 Seattle 必去|西雅圖 Seattle 夜景'
date: 2021-03-31 02:13:21
tag: [Seattle, Covid]
category: Travel
---
接續上篇：[疫情間的西雅圖三天二夜（Day 1）派克市場 星巴克創始店 西雅圖藝術博物館 西雅圖中央圖書館 西雅圖摩天輪](https://ycjhuo.gitlab.io/blogs/Seattle-3-Days-Itinerary-Day-1.html)

## 景點規劃
第二天的行程是：<br />
瑞尼爾山（Mount Rainier National Park）➔ The Spheres ➔ 太空針塔（Space Needle）➔ 凱利公園（Kerry Park）

## 瑞尼爾山（Mount Rainier National Park）
到了西雅圖，當然就要去著名的瑞尼爾國家公園，由於沒有交通工具，我選擇了[玩哪兒的瑞尼爾一日遊](https://www.wannar.com/seattle-mount-rainier-1-day-tour-2685.html#reviews)

可能是疫情關係，這次旅行團只有我們加上司機而已，07:45 在 Chinatown 集合，由司機開小巴直接前往瑞尼爾山（中途要停休息站的話，可與司機溝通）

進入瑞尼爾山後，司機會帶我們到三個景點 <br />
分別是：克里斯汀瀑布（Christine Falls），仙境之路（Skyline Loop Trail）以及倒影湖（Reflection Lake）

到達遊客中心後，司機會給我們 2-3 小時的時間自由活動 <br />
仙境之路有很多條路線，但由於當天沒有穿雪靴，為了避免滑倒，只能選擇最容易的路線，且路面太滑，也無法走到太上面

但即使如此，我仍認為這是瑞尼爾山最好玩也是最值得的景點 <br />
倒影湖則是天氣好的時候，可以看到瑞尼爾山整個倒映在湖面上
### 遊客中心
![Mount Rainier National Park Skyline Loop Trail](../images/72-05.jpg)

### 仙境之路（Skyline Loop Trail）
![Mount Rainier National Park Skyline Loop Trail](../images/72-02.jpg)
![Mount Rainier National Park Skyline Loop Trail](../images/72-04.jpg)
### 倒影湖（Reflection Lake）
![Mount Rainier National Park Skyline Loop Trail](../images/72-03.jpg)

結束這三個景點後，司機就會將我們載回 Chinatown，時間大約為 15:30-16:30 左右 <br />
下車後，這邊有家韓國熱狗店（Chung Chun Rice Dog），最近在紐約也很紅 <br />

---

因為 Chinatown 就位於市區的南端，因此若是昨天沒去舊城區的拓荒者廣場（Pioneer Square），沈船停車場（Sinking Ship），史密斯塔（Smith Tower），這三個景點的話，也可以趁現在走過去 <br />

回到市區後，可以接續昨天的路線，開始進行今天的市中心路線，探索西雅圖市區的北邊（接下來的景點適合傍晚去）<br />
The Spheres ➔ 太空針塔（Space Needle）➔ 凱利公園（Kerry Park）<br />

建議搭 light Rail 到 Westlake Station 這站，再開始跟著上面的景點走 <br />

![Seattle Night Attractions](../images/72-01.png)

## The Spheres
2018 年才蓋好的 Amazon Spheres，奇特的球狀造型，裡面酷似植物園，晚上燈光點亮後更讓人驚艷（旁邊還有 Amazon 無人商店）
![The Spheres](../images/72-06.jpg)

## 太空針塔（Space Needle）
西雅圖地標之一的太空針塔，可買票乘坐電梯上去觀看夜景，但因排隊人數過多，就沒上去了（旁邊有個琉璃博物館）
![Space Needle](../images/72-07.jpg)
![Space Needle](../images/72-08.jpg)

在太空針塔附近，有間人氣早午餐店（Tilikum Place Cafe），晚上雖然也有開，但由於早晚菜單不同，只好隔天早上再跑一趟 <br />
多數人推薦的 Dutch baby 只有早上有賣，隔天早上去的時候，因為沒事先訂位，要等半小時以上（還不一定會有），就外帶回飯店吃了 <br />
![Tilikum Place Cafe](../images/72-12.jpg)
![Tilikum Place Cafe Dutch baby](../images/72-11.jpg)

## 凱利公園（Kerry Park）
最後來到今天的最後一站，可以俯瞰西雅圖市區夜景的凱利公園，個人認為可以不用特別上去史密斯塔以及太空針塔，直接在這邊看夜景就很夠了

![Kerry Park](../images/72-09.jpg)
![Kerry Park](../images/72-10.jpg)


對飯店有興趣的可參考：<br />
[開箱！西雅圖希爾頓 The Charter 飯店（ The Charter Hotel Seattle, Curio Collection by Hilton）](https://ycjhuo.gitlab.io/blogs/The-Charter-Hotel-Seattle.html) <br />

[開箱！西雅圖萬豪 Renaissance 飯店（ Marriott Renaissance Seattle Hotel ](https://ycjhuo.gitlab.io/blogs/Renaissance-Seattle-Hotel.html) <br />
