---
title: Facebook（FB）公布 2021 Q1 季報，交出季成長翻倍的成績單
description: '美股投資 Facebook FB|FB 2021 Q1 第一季財報|Facebook 成長翻倍|Facebook 庫藏股'
date: 2021-04-29
tag: [Stock, FB]
category: Investment
---

在[特斯拉](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q1.html)，[Google](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q1.htm) 接連公布 2021 Q1 季報後，社交龍頭 Facebook 也在今天盤後公佈了 2021 Q1 季報 <br />

這次季報直接讓 Facebook 在盤後上漲了 6.8%，下面來看看 Facebook 為什麼會大漲的原因

## Facebook 2021 Q1 季報
[來源](https://s21.q4cdn.com/399680738/files/doc_financials/2021/FB-03.31.2021-Exhibit-99.1_Final.pdf)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q1</th>
    <th align="center">2020 Q4</th>
    <th align="center">2021 Q1 vs. 2020 Q4</th>
    <th align="center">2021 Q1 vs. 2020 Q1</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">26,171</td>
    <td align="center">28,072</td>
    <td align="center">-6.77%</td>
    <td align="center">47.55%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">11,503</td>
    <td align="center">13,055</td>
    <td align="center">-11.89%</td>
    <td align="center">96.28%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">80.39%</td>
    <td align="center">81.44%</td>
    <td align="center">-1.05%</td>
    <td align="center">-0.10%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">43.48%</td>
    <td align="center">45.51%</td>
    <td align="center">-2.03%</td>
    <td align="center">10.25%</td>
  </tr>
    <tr>
    <td align="center">EPS</td>
    <td align="center">3.34</td>
    <td align="center">3.94</td>
    <td align="center">-15.23%</td>
    <td align="center">94.19%</td>
  </tr>
</table>

本次季報，若與上季（2020 Q4）相比，各項數值都是小幅衰退的 <br />
但若是與去年第一季相比（2020 Q1）比的話，情況就大大不同了，營收了成長近 50%，而稅前淨利及 EPS 更是爆增了近一倍 <br />

下圖是 Facebook 近二年的營收，可以看出 FB 在前二季（Q1, Q2）營收都是較少的 <br />
加上去年因受疫情影響，使得 Facebook 營收的成長減緩；但隨著疫情逐漸穩定後，Facebook 在這季達成了 EPS 成長近翻倍好成績 <br />

![Facebook 近二年營收](../images/FB-Revenue-2019-2021.png)<br/>

## 使用者活躍數
Facebook 雖已身為社交龍頭，但旗下產品的使用者活躍數，仍持續上升中 <br />
而家族產品的活躍數成長率又高於 Facebook，表示出 Instagram 及 What's App 的市場仍有足夠的增長空間 <br /> 

::: tip
- Facebook 每日活動用戶 DAUs（daily active users）：與上季相比，成長了 2.17%，目前人數為 18.8 億 <br />
- 整個家族（包含 Facebook, Instagram, What's app）的日活動用戶：與上季相比，成長了 4.62%，目前人數為 27.2 億 <br />
- Facebook 月活用戶：相比上季成長了 1.79%，人數為 28.5 億<br />
- 家族的月活用戶：相比上季成長了 4.55%，人數為 34.5 億<br />
:::

![Facebook 每日使用者活躍數 2021 Q1](../images/FB-DAU-2021Q1.png)<br/>

## 後記
Facebook 在財報中指出，由於去年第二季也受到疫情影響不小，因此 Q2 的季報仍會有不錯的增長率，但到了 Q3, Q4 成長幅度就會逐漸減緩 <br />

下圖是 Facebook 將各地區的營收分別列出，可以看出，北美仍是 Facebook 的主要營收來源（歐洲 + 亞太區都仍未超過北美）
![Facebook 各地區營收 2021 Q1](../images/FB-Revenue-geography-2021Q1.png)<br/>

若再將營收 / 使用者人數的話，則可得出：每位使用者平均貢獻了多少營收

::: tip
北美約 $48.03；歐洲 $15.49；亞太 $3.94；其他地區 $2.64；平均為 $9.27
:::

平均一位北美使用者的貢獻度約等於 12 名亞太區的使用者，在目前北美及歐洲的使用者增長幾乎已停滯的情況下，未來 Facebook 要提升營收只能寄望在亞太區及其他市場 <br />

但在滲透率已如此高的現在，想辦法提高每位使用者的貢獻度更能有效地讓營收再上一層樓，但這也不是件容易的事 <br />

---

根據[維基百科的人均總收入資料（2019 年）](https://en.wikipedia.org/wiki/List_of_countries_by_GNI_(nominal)_per_capita)：<br />
北美區：美國人均 $65,760；加拿大 $46,370 <br />
亞太區：新加玻 $59,590；日本 $41,690；台灣 $26,594；中國 $10,410 (雖然 Facebook 難以開拓這個市場)；印度 $2,130<br />

---

另外，在 2020 Q4 期間，Facebook 投入了 19 億來回購股票（股價在這段時間上漲 2.45%；S&P 500 上漲 11.1%），而在這一季加大了購買力度，約投入 39 億來回購（股價在這段時間上漲 9.5%；S&P 500 上漲 7.36%）

---

看更多圖表：<br />
[Facebook Q1 圖表](https://s21.q4cdn.com/399680738/files/doc_financials/2021/FB-Earnings-Presentation-Q1-2021.pdf) <br />

延伸閱讀：<br />
[公佈第四季及 2020 全年財報，在企業縮減開支的一年下，Facebook 表現如何？](https://ycjhuo.gitlab.io/blogs/2020-Full-Year-Earnings-FB.html)<br />

[Google（GOOG）公布 2021 Q1 季報，近半年中表現最好的權值股](https://ycjhuo.gitlab.io/blogs/GOOG-Earnings-2021-Q1.htm)<br />

[特斯拉（Tesla）2021 Q1 季報，缺席了高價車的財報依舊強勢](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q1.html)<br />