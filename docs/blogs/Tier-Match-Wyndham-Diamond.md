---
title: Tier Match 會籍匹配，如何免費取得溫德姆鑽石會員（Wyndham Diamond）
description: '飯店會籍匹配|飯店 Match|溫德姆 Match|Wyndham Match|Wyndham Caesars|溫德姆 凱薩 Match|溫德姆 鑽石 到 凱薩|會籍 Match'
date: 2021-12-06
tag: [Hotel, Wyndham, Tier Match]
category: Travel
---
飯店會籍匹配流程：
::: tip
[Wyndham Diamond]() → Caesars Diamond → MGM Gold → Hyatt Explorist
:::

萬事達卡（Mastercard）最近推出一個活動：只要提供亞太地區銀行，所核發的世界之極（World Elite Mastercard）信用卡卡號的前 6 碼，即可直接得到溫德姆集團的鑽石會員等級（Wyndham Diamond）<br />

信用卡的前 6 碼只能得出發卡組織（也就是萬事達卡），以及發卡銀行，因此不用擔心會因此洩漏個人資料

該活動只到 2021/12/30 截止，有興趣的可以點擊[活動網頁](https://specials.priceless.com/en-ap/offers/Wyndham_Hotels___Resorts?Oid=201912180093)


::: tip
若想知道自己的信用卡是什麼等級，可到 [BIN Codes](https://www.bincodes.com/bin-checker/)輸入前 6 碼即可得知
:::

## 溫德姆集團鑽石（Wyndham Diamond）好處
[溫德姆會籍福利表](https://www.wyndhamhotels.com/wyndham-rewards/member-levels)

這次活動可以匹配到的鑽石會員是溫德姆會籍中的最高等會員，但即使已是最高等會員，仍沒有其他飯店集團會提供的免費早餐福利 <br />

比較常見的福利僅有：Early Check-in / Late Check-out 以及房間升等而已，而因為溫德姆集團的定位多以僅有一種房型為主，因此要能用到房間升等的機會其實不高

那這樣的話，我們還須要參加萬事達卡的匹配活動來拿到溫德姆集團的鑽石會員嗎？

溫德姆集團雖然看起來不好用，但一但成為了溫德姆的鑽石會員，即可用這個會員去匹配凱薩集團（Caesars）的鑽石會員（Diamond）<br />

凱薩集團（Caesars）的鑽石會員享有：免 Resort fee、免停車費（在大多數飯店）以及每年 $100 的餐飲費用折抵（適用於凱薩酒店旗下的餐廳）

之後更可以用凱薩集團的鑽石會員去匹配 MGM 集團的金卡會員，最後再用 MGM 的金卡會員匹配 Hyatt 的 Explorist 會員

P.S: 凱薩集團（Caesars）鑽石會員沒有 Early Check-in / Late Check-out、房間升等、免費早餐這些常用福利

## 如何申請溫德姆鑽石會員（Wyndham Diamond）
上面說了這麼多好處，那該怎麼參與這個活動呢？

只須自行 E-mail 到：WyndhamRewardsUpgrade@wyndham.com <br />
再附上自己的溫德姆會員號碼以及 Nastercard World Elite 等級的信用卡前 6 碼即可 <br />

範本如下：<br />
主旨為：Mastercard complimentary / fast track offer
```
Hi, 
I would like to participate in the Wyndham status match, here's my info below:

Name: XXXX
Member Number: XXXXXXXXXX
First 6 digits of the MasterCard number: 528911
```

之後大約二個星期後，再打開自己的 App 就可以看到自己的 Wyndham 會員已被升級成 Diamond 囉

## 飯店會籍匹配系列文
下篇：<br />
[Tier Match 會籍匹配，溫德姆鑽石會員（Wyndham Diamond）→ 凱薩鑽石會員（Caesars Diamond）](https://ycjhuo.gitlab.io/blogs/Tier-Match-Wyndham-To-Caesars.html)
