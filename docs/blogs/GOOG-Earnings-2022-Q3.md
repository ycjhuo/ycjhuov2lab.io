---
title: Google（GOOG）2022 Q3 季報，股價下跌 33%，本益比低於疫情
description: '美股投資 Google GOOG|Google 拆股 |拆股股價上漲|美股 拆股 影響'
date: 2022-11-19
tag: [Stock, GOOG]
category: Investment
---
Google（股票代號：GOOG），從 Q2 - Q3 財報發布期間（7/26 → 10/25），股價表現持平，僅下跌 0.48%（$105.44 → $104.93），而同期間 S&P 500 下跌 -1.58%，二者差距不大 <br />

而在 Q3 財報發佈後，Google 股價下跌 9.6%，到了 $94.82，最低時跌到了 $83.5，之後緩慢回升 <br />
截至今天，股價距離財報發佈日的表現為 -6.8% <br />

Google 股價今年以來的報酬率為 -32.6%，現在本益比更是比疫情時還低，為 20.04 <br />
現在是否為買進 Google 的最佳時機呢？接著來瞭解 Google 第三季財報 <br />

::: tip
疫情爆發時（2020/03/16），Google 本益比最低來到 22.47
:::


## Google 2022 Q3 財務指標
[來源](https://abc.xyz/investor/static/pdf/2022Q3_alphabet_earnings_release.pdf?cache=4156e7f)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4 vs. Q1</th>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
  </tr>
  <tr>
    <td align="center">總營收（百萬）</td>
    <td align="center">69,955</td>
    <td align="center">69,092</td>
    <td align="center">-9.71%</td>
    <td align="center">2.86%</td>
    <td align="center">-1.23%</td>
  </tr>
  <tr>
    <td align="center">毛利率</td>
    <td align="center">56.97%</td>
    <td align="center">54.9%</td>
    <td align="center">0.27%</td>
    <td align="center">0.49%</td>
    <td align="center">-2.06%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">27.81%</td>
    <td align="center">24.8%</td>
    <td align="center">0.49%</td>
    <td align="center">-1.74%</td>
    <td align="center">-3.01%</td>
  </tr>
  <tr>
    <td align="center">稅前淨利（百萬）</td>
    <td align="center">19,284</td>
    <td align="center">16,233</td>
    <td align="center">-22.41%</td>
    <td align="center">1.85%</td>
    <td align="center">-15.82%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">1.21</td>
    <td align="center">1.06</td>
    <td align="center">-19.61%</td>
    <td align="center">-1.63%</td>
    <td align="center">-12.40%</td>
  </tr>
</table>


- 總營收：約 691 億，較上季小幅衰退 1.23%，營收來源仍以搜尋業務為主力，佔了 39%
- 毛利率：為 54.9%，較上季衰退 2.06%，前四季皆保持在 56.5 - 57% 之間
- 營業利潤率：為 24.8%，較上季衰退 3%，已連續二季衰退
- 稅前淨利：較上季衰退了 16%，因營業利潤率的下滑，再加上匯率的影響，嚴重打擊了 Google 的獲利能力
- EPS：相較上季衰退了約 12%，目前已連續四季衰退

Google 在這個季度，營收在匯率影響下小幅下滑，但由於毛利率、人力成本的提高，使得稅前淨利＆EPS 有著較大幅度的下滑 <br />
而由於強勢美元的影響，也持續受限了 Q4 的盈利能力 <br />

且目前在科技業大舉裁員的風潮下，Google 並未有此計畫，因此在下一季度財報，仍會看到人力成本繼續提高，而繼續拉低營業利潤率來影響獲利能力 <br />

## 財報重點
財報中提及，未來會著重在短影音 Short 上，而最快將在 2023 Q1 財報揭露 Short 的盈利狀況 <br />
目前短影音 Short 每個月的使用者為 15 億（約是 Facebook 的一半），而每天觀看量則達到 300 億 <br />

且根據 Nielsen 報告指出，YouTube 在今年九月，在收視率上，首次稱霸了美國串流電視的市場 <br />
Google 也承諾會隨著影音創作者的腳步，持續開發更多有利於創作者的工具，例如影片導購、影片購物等等 <br />


接著在雲服務 Cloud 方面，本季營收較上季增加了 9%，也收穫了更多新客戶 <br />
如：汽車業巨頭 Toyota、金融機構 Prudential、加密貨幣交易所 Coinbase、夏威夷州政府以及澳洲證交所 Australia Securities Exchange 等等 <br />

由於雲服務有著長期合作的特性，因此這些客戶在往後也能成為 Google Cloud 可持續的營收來源

::: tip
現階段 Google Cloud 佔總營收比例為 10%
:::

## 後記
Google 股價雖在今年表現不盡理想（-33%），且在拆股後，股價仍繼續走低，跟今日（11/18）股價相比，股價表現為 -11% <br />
但好處是，股價從拆股前的 $2,000 一股，已縮減到現在的不到 $100，更加適合資金不多的投資人買入 <br />

從財報中可看出，Google 十分知道自己發展方向，在 Youtube 已成為不可或缺的營收來源後，新推出的短影音 Short <br />也有著不錯的成績，甚至在明年就可在財報中單獨看到 Short 帶給 Google 的幫助 <br />

在目前短影音當道的時刻，TikTok、Reels、Short 都在爭奪使用者的眼球 <br />
Google 靠著從 Youtube 累積的資源，能有效的將本來就有一眾粉絲的 Youtuber 帶到 Short <br />
讓 Youtuber 也多了一種盈利管道，達成雙贏的局面 <br />

而 Google Cloud 也持續的在增加重量級客戶，且不僅涵蓋了不同的產業，也從商業界跨足了公部門，可謂是後勢看好 <br />
在股價已變得容易入手的現在，且本益比又低於疫情最嚴重的時候，我認為是加碼 Google 的時候了

::: warning
點擊觀看更多： <br />
- [Google 分析文](https://ycjhuo.gitlab.io/tag/GOOG/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::