---
title: 如何在 Pandas 使用 Excel 的 Match 函數
description: 'Python Excel 教學 | Python Excel Match 用法 | Python Excel Match 範例 | Python Pandas 合併 Excel | Python Pandas 合併報表'
date: 2021-07-14
tag: [Python, Pandas, Excel, Match]
category: Programming
---

因疫情關係，到辦公室的人都須事先填寫問券，在搭配門禁系統的刷卡紀錄，就可比對出哪些人有到辦公室，卻沒有填寫問券 <br />
要達成這個目的，利用 Excel 的 Match 函數，在二個資料表中比對，就可完成 <br />

那在 Python 的 Pandas 中，該如何做到跟 Excel 的 Match 函數一樣的效果呢 <br />

不熟悉 Match 函數 的可參考這篇：<br />
[如何在 Excel 中跨分頁查找所需資料 (Match 函數介紹)](https://ycjhuo.gitlab.io/blogs/How-To-Search-Data-What-You-Need-in-Excel-Introduce-MATCH-Function.html)

![95-01](../images/95-01.png)

上面這二個資料表 (左邊是問券資料，右邊是門禁系統資料) 

Pandas 中有個 ```pd.merge()``` 方法，可以合併二個資料表

## 讀取 Excel
```python
import pandas as pd
import os

# Get desktop path
desktopPath = os.path.abspath(os.path.dirname(os.getcwd()))

userInput = input("Please type mmdd : ")

# Get file path
filePath = desktopPath + r'\Dairy Access Report\Match.xlsx'

# Read excel
ques = pd.read_excel(filePath, 'Question')
door = pd.read_excel(filePath, 'Door')
```

## 問券資料表
先將資料表的欄位名稱，再來因為問券資料表內的資料是一個區間的，所以我們用 ```userInput``` 來讓 User 選擇要抓取哪天的問券資料，之後將那天的資料存成 ```ques```

```python
# Update Date format from 2021-07-07  -> 0707
def removeYear(series):
    series = series.apply(str).apply(lambda x : x[5 : 10]).str.strip()
    series = series.apply(lambda x : x.replace('-', ''))
    return series

# Rename columns for dataframe ques
ques = ques.rename(columns= {'Which date you will go to the office' : 'Date', 'Employee ID #' : 'EID' })

### Select the date we need
# Set Y in column Fill Ques
ques.loc[removeYear(ques['Date']) == userInput, 'Fill Ques']  = 'Y'

# Selected Fill Ques == Y as df ques
ques = ques[ques['Fill Ques'] == 'Y']

ques = ques[['EID', 'Fill Ques']]

# Setting EID as index
ques.set_index(['EID'], inplace=True)

print(ques)
```

![95-02](../images/95-02.png)

上圖可以看到在 07/08 這天只有 3 個人填寫問券

## 門禁資料表
門禁系統因為沒有單獨的 ID 欄位，所以須用字串切割的方式來取得 ID <br />
且因為同一個人在門禁資料表中會有多次刷卡紀錄，因此要用 ```groupby()``` 來挑出每個人最早的刷卡時間

```python
# Get employee ID
door['ID'] = door['Person Name'].apply(lambda x:x[x.index("(")-7 : x.index("(")]).str.strip()

# Get the first clock record by ID
door = door.groupby(['ID']).agg({'Date' : 'min'})
print(door)
```

![95-03](../images/95-03.png)

門禁資料表(door)篩選出來會如上圖

## 合併資料表
二個資料表(ques & door) 都準備好後，就可以利用 ```merge``` 來將這二個表合併成一個（df）了 <br />

因為這二個表 index 都是 ID，因此我們可以用 ```left_index=True, right_index=True``` 作為合併的基準 <br />

::: tip
這裡要注意：我們是將 ques 資料表（有填問券的人）拿來比對門禁系統（有進辦公室的人） <br />
所以 door 要放在 ```ques``` 的前面（```pd.merge(door, ques, how='left', left_index=True, right_index=True)```）
:::

合併後， 門禁系統內的人若沒有填問券（也就是 ```door``` 有資料，而 ```ques``` 沒資料），則  ```Fill Ques``` 欄位會顯示 NAN（如下圖左）<br />

![95-04](../images/95-04.png)

所以我們可以用 ```df['Fill Ques'] = df['Fill Ques'].fillna('N')``` 將 NaN 改為 N（如上圖中）<br />

最後再將 ID 從 index 移出來，並 reset index （如上圖右）<br /> 

即可完成如同 Excel 的 Match 功能

```python
# Merge door & ques based on index
df = pd.merge(door, ques, how='left', left_index=True, right_index=True)
print(df)

df['Fill Ques'] = df['Fill Ques'].fillna('N')
print(df)

# Move index to a column
df['ID'] = df.index

# Reset index
df.reset_index(drop=True, inplace=True)
print(df)

with pd.ExcelWriter(desktopPath  + r'\Dairy Access Report\MatchDemo_' + userInput + '.xlsx') as writer:
    df.to_excel(writer, sheet_name = "df", index=False)

print("Saved to Excel")
```

## Source Code
```python
import pandas as pd
import os
from os import system
system('cls')


#####################################################
################# Read Excel Files ##################
#####################################################

desktopPath = os.path.abspath(os.path.dirname(os.getcwd()))
# get file
userInput = input("Please type mmdd : ")

filePath = desktopPath + r'\Dairy Access Report\Match.xlsx'

ques = pd.read_excel(filePath, 'Question')
door = pd.read_excel(filePath, 'Door')


# Update Date format from 2021-07-07  -> 0707
def removeYear(series):
    series = series.apply(str).apply(lambda x : x[5 : 10]).str.strip()
    series = series.apply(lambda x : x.replace('-', ''))
    return series

#####################################################
################### questionnaire ###################
#####################################################

# rename columns for dataframe ques
ques = ques.rename(columns= {'Which date you will go to the office' : 'Date', 'Employee ID #' : 'EID' })

### Select the date we need
# Set Y in column Fill Ques
ques.loc[removeYear(ques['Date']) == userInput, 'Fill Ques']  = 'Y'

# Selected Fill Ques == Y as df ques
ques = ques[ques['Fill Ques'] == 'Y']

ques = ques[['EID', 'Fill Ques']]

# Setting EID as index
ques.set_index(['EID'], inplace=True)


#####################################################
######################## Door #######################
#####################################################

# Get employee ID
door['ID'] = door['Person Name'].apply(lambda x:x[x.index("(")-7 : x.index("(")]).str.strip()

# Get the first clock record by ID
door = door.groupby(['ID']).agg({'Date' : 'min'})

#####################################################
####################### Merge #######################
#####################################################

# Merge door & ques based on index
df = pd.merge(door, ques, how='left', left_index=True, right_index=True)

df['Fill Ques'] = df['Fill Ques'].fillna('N')

# Move index to a column
df['ID'] = df.index

# Reset index
df.reset_index(drop=True, inplace=True)

#####################################################
############# Save dataframe to Excel ###############
#####################################################

with pd.ExcelWriter(desktopPath  + r'\Dairy Access Report\MatchDemo_' + userInput + '.xlsx') as writer:
    df.to_excel(writer, sheet_name = "df", index=False)

print("Saved to Excel")
```