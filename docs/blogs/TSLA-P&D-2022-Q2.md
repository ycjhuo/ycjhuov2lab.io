---
title: 特斯拉（TSLA）發布 2022 Q2 生產及交付量，Model 3/Y 大減 20%
description: '美股特斯拉|特斯拉 TSLA|特斯拉生產交車量 2022 Q2|特斯拉 Model 3 生產交車量 2022 Q2|特斯拉 Model Y 生產交車量 2022 Q2|特斯拉 拆股 2022'
date: 2022-07-05
tag: [Stock, TSLA]
category: Investment
---

特斯拉在 07/02（六）公佈了 2022 第二季的生產 & 交車數量

在上次財報發布 → 公佈 2022 Q2 交車數量期間（04/20 - 07/02），特斯拉股價大跌了 30.23%，而 S&P 500 指數也下跌了 14.22%

特斯拉的股價表現在本季結束了連續三季度超越大盤的成績，而是帶給投資者落後大盤 2 倍的報酬率

下面來看看特斯拉 2022 第二季的生產/交付量，是否能幫助特斯拉的股價止跌回升

## 特斯拉 2022 Q2 生產/交付量

<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2022 Q1</th>
    <th align="center">2022 Q2</th>
    <th align="center">2021 Q4 vs. 2021 Q3</th>
    <th align="center">2022 Q1 vs. 2021 Q4</th>
    <th align="center">2022 Q2 vs. 2022 Q1</th>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">14,218</td>
    <td align="center">16,411</td>
    <td align="center">46.62%</td>
    <td align="center">8.46%</td>
    <td align="center">15.42%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">291,189</td>
    <td align="center">232,169</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
    <td align="center">-20.27%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">14,724</td>
    <td align="center">16,162</td>
    <td align="center">26.49%</td>
    <td align="center">25.14%</td>
    <td align="center">9.77%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">295,324</td>
    <td align="center">238,533</td>
    <td align="center">27.9%</td>
    <td align="center">-0.53%</td>
    <td align="center">-19.23%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">305,407</td>
    <td align="center">248,580</td>
    <td align="center">28.6%</td>
    <td align="center">-0.14%</td>
    <td align="center">-18.61%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">310,048</td>
    <td align="center">254,695</td>
    <td align="center">27.84%</td>
    <td align="center">0.45%</td>
    <td align="center">-17.85%</td>
  </tr>
</table>

本季度的生產/交付量：

- Model S/X：生產量約 1.6 萬台，與比上季相比，成長了約 15.5%（上季的成長率約 8.5%）；交付量則較上季成長約 10%  <br/>
自 2021 Q2 改款完畢以來，生產量就逐步成長，季成長約 12%

- Model 3/Y：本季的生產/交付量受疫情＆供應鏈影響，下跌了約 20%，不像上季僅微幅下降 0.5%，本季是真正意義上的中斷了先前連續 6 個季度的雙位數成長率，首次的負成長（-20.27%）

::: tip
雖說本季的 Model 3/Y 生產/交付量下降了 20%，但實際數量仍與 2021 Q3 差不多
:::

## 特斯拉德國柏林廠＆美國德州廠
2022 Q1 財報提到：德國柏林廠＆美國德州廠均已進入生產狀態，且都以生產 Model Y 為主，讓投資者認為在 Model Y 生產量會在 Q2大幅增加，卻沒想到受到了疫情＆供應鏈的影響，生產量不增反減

馬斯克在接受訪問也表示，「由於電池短缺和中國大陸港口的問題，讓這些工廠難以增加產量」，點出了 Model 3/Y 本季生產量下滑 20% 的原因

綜觀特斯拉在前 6 季（2020 Q2 - 2021 Q4）創下的季產量成長率二位數成績，是處在疫情最嚴重的情況下，而現在各國皆慢慢控制住疫情影響，特斯拉才首次因外部影響而出現衰退，導致特斯拉在這二個新廠損失了數十億美元

## 後記
特斯拉的股價在這個季度出現了今年的最低點（$628），今年至今的報酬率為 -43%，除了是受到疫情＆供應鏈因素外，還加上聯準會打擊通膨而連連升息的影響，在這些不利條件下，特斯拉的股價能否止住跌勢？

除去這些壞消息外，我們仍可以期待即將到來的特斯拉 Q2 財報，亮點會有：
1. 毛利率上升：因 Model 3/Y 產量減少，高價車款 Model S/X 佔總生產量的比重從上季的 4.66% → 6.6%，以及最近的調漲車價下，毛利率可望一舉突破 30%（Q1 為 29.1%）
2. 隨著特斯拉最近的裁員＆停止招聘活動，顯示了特斯拉正設法將公司內的人員做最有效的運用，這有助於提升特斯拉的營業利益率，將本季的營業利益率上升到 20% 以上（Q1 為 19.2%）

換一方面看，雖然特斯拉受制於外部因素的影響，造成股價一路走跌，但公司本身仍在高速成長，且因應環境而迅速做出回應  <br/>

在股價遭受打壓的現在，是持續加碼特斯拉的好機會


::: warning
點擊觀看更多：  <br/>
- [特斯拉分析文](https://ycjhuo.gitlab.io/tag/TSLA/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
