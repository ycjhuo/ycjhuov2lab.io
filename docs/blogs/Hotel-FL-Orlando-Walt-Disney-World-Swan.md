---
title: 開箱！奧蘭多-華特迪士尼天鵝飯店（Walt Disney World Swan）
description: '佛羅里達 奧蘭多 華特迪士尼 飯店 介紹 推薦|Florida Orlando Walt Disney World 飯店|奧蘭多 Orlando 萬豪 迪士尼 飯店 推薦 CP 值|Walt Disney World Swan 華特迪士尼 天鵝 飯店 介紹 推薦 開箱'
date: 2023-06-30
tag: [Hotel, Disney, Orlando, Florida, Marriott]
category: Travel
---
::: tip
入住時間：2023/06/17 - 06/19 <br />
房型：2 Double Beds <br />
每晚價格（稅後）：$388（含每天 $45 的 resort fee） / 48,000 點 <br />
Google 評價：4.2 顆星（302 評價）
:::

接續上篇 [開箱！奧蘭多-華特迪士尼海豚飯店（Walt Disney World Dolphin）](https://ycjhuo.gitlab.io/blogs/Hotel-FL-Orlando-Walt-Disney-World-Dolphin.html)，這間就在海豚飯店的隔壁，一樣是由萬豪（Marriot）跟迪士尼合作的聯名飯店

跟海豚比起來較新，這點從 Google 地圖上僅有 300 個評價就看得出來（相比海豚的 13,000 個評價）

## 飯店外觀＆大廳（Hotel Building & Lobby）
不同於海豚飯店的金字塔造型&金魚元素，天鵝飯店（Swan）的外觀就是天鵝
![華特迪士尼天鵝飯店（Walt Disney World Swan）外觀](../images/223-01.jpg)
大廳的部分比較寬敞，但我覺得海豚飯店的大廳比較有設計感
![華特迪士尼天鵝飯店（Walt Disney World Swan）外觀](../images/223-02.jpg)
飯店櫃檯跟海豚一樣，有分給萬豪金卡會員以上的 Elite 櫃台，跟普通櫃檯 <br />
但不管是排哪個櫃台，通常 Check-in 都會花費半小時以上的時間
![華特迪士尼天鵝飯店（Walt Disney World Swan）外觀](../images/223-03.jpg)

## 房間內部（2 Double Beds）
這次入住的是 2 Double Beds 房型，房內有小冰箱及二瓶水，沒有微波爐 <br />
一樣是透過 Amex 的 THC（The Hotel Collection）訂房，但可能是長週末假期的關係，沒有被升等
![華特迪士尼天鵝飯店（Walt Disney World Swan）房間](../images/223-04.jpg)
![華特迪士尼天鵝飯店（Walt Disney World Swan）房間](../images/223-05.jpg)

房間看出去的景，如果有像入住海豚飯店那次一樣被升等景觀房，景色應該會更好，但這間在 21:10 看的到些微的 Epcot 煙火
![華特迪士尼天鵝飯店（Walt Disney World Swan）窗外景色](../images/223-11.jpg)

## 衛浴（Bathroom）
洗手台跟衛浴是獨立的，盥洗用具附的是 Gilchrist & Soames 這個牌子 <br />
另外，飯店不會主動提供牙刷、牙膏、拖鞋，若會用到的話，要主動跟飯店拿
![華特迪士尼天鵝飯店（Walt Disney World Swan）衛浴](../images/223-06.jpg)
![華特迪士尼天鵝飯店（Walt Disney World Swan）盥洗用具](../images/223-07.jpg)

## 餐廳（Restaurant & Bar）
若是萬豪的白金會員入住的話，一人可以得到 $10 的早餐額度，一間房最多 $20 <br />
在餐廳點一份餐都會超過這個額度，且因為有提早半小時入園的福利，所以比較推薦在飯店的 Grab & Go 買麵包帶在路上或是園區吃 <br />
菜單上的三明治都是會用烤箱幫我們加熱
![華特迪士尼天鵝飯店（Walt Disney World Swan）餐廳](../images/223-08.jpg)
![華特迪士尼天鵝飯店（Walt Disney World Swan）餐廳](../images/223-09.jpg)

## 飯店設施
設施跟海豚飯店一樣，有免費使用的游泳池、天鵝船以及健身房，有興趣可參考[上篇](https://ycjhuo.gitlab.io/blogs/Hotel-FL-Orlando-Walt-Disney-World-Dolphin.html) <br />

這篇介紹的則是接駁車，入住海豚或是天鵝的話，門口都會有接駁車可以搭乘，直接上車即可，不須出示房卡，除了迪士尼的四個園區外，也有到 Disney Springs（迪士尼的商店街）
![華特迪士尼天鵝飯店（Walt Disney World Swan）接駁車](../images/223-10.jpg)

## 後記
其實在奧蘭多，萬豪跟迪士尼聯名的飯店，除了這次介紹的天鵝（Walt Disney World Swan）跟上篇的海豚之外（Walt Disney World Dolphin），還有一個 Walt Disney World Swan Reserve，價格相比這二間就高了不少 <br />
 
天鵝跟海豚一晚的價格在 $300 - 400，而 Swan Reserve 則是在 $700 - 800 之間 <br />
而前面二間就我入住下來，感覺其實差不多，但因為入住海豚時，櫃台有幫我升級到景觀房，所以體驗比較好一點 <br />

接駁車的部分，其實三間飯店都是搭一樣的車，如果在天鵝上車的話，第二站就是海豚，再來是 Swan Reserve，最後就直接開到園區了 <br />

迪士尼飯店除了價格較高（迪士尼外圍的其他飯店價格約 $50 - 100），但該有的福利也不少，像是：提早半小時入園 & 到各個園區的免費接駁車 <br />

提早半小時入園的福利可讓我們在人潮進來前，先玩過一次最熱門的設施，玩完後再馬上排一次，大概一小時就能玩到二次 <br />
若是以 Lighting lane 的價格來算，玩二次價格約 $50，二個人的話就是 $100 <br />

再加上每天往返 飯店 - 園區的接駁車，若以  Lyft/Uber 來看，差不多是 $25-46，把這些都加上去後，也就不覺得迪士尼飯店貴了

::: tip
PS. 若是透過 美國運通 Amex The Hotel Collection（THC）或是 Fine Hotel + Resort（FHR）預定的話，則送的 $100 也可用在 reosrt fee <br />
但若是問櫃台的話，他們會說不行，不過在 Check-out 的那天，我們會收到 email 收據，上面的確是有用那 $100 扣掉 resort fee 的
:::

::: warning
點擊觀看更多： <br />
- [飯店開箱文](https://ycjhuo.gitlab.io/tag/Hotel/)
- [迪士尼相關](https://ycjhuo.gitlab.io/tag/Disney/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::
