---
title: 台灣駕照在紐澤西免考試換駕照 @ Bayonne MVC
description: '紐澤西換駕照 | 免考試換駕照 | 台灣駕照紐澤西 | 美國辦駕照 | MVC 預約 | 免試換照 | New Jersey 換駕照'
date: 2021-07-16
tag: [Drive Lincense, New Jersey, New York]
category: Travel
---

現在紐澤西（New Jersey）可以用台灣駕照「不用考試」直接更換成美國駕照了（本來台灣駕照僅能免除路考，還是需要筆試）

這篇來分享該如何在紐澤西用「台灣駕照」直接換成「美國駕照」

---

## 準備文件
要在紐澤西換駕照必須滿足以下條件：
- 居住在紐澤西（New Jersey）：能出示紐澤西地址的```銀行帳單（Bank Statement）```或是 ```水電費帳單（Utility Bill）```
- 具有 ```SSN (Social Security Number Card)``` 或能出示```無法辦理 SSN 的相關文件```
- 攜帶```台灣駕照（影本）```到紐約經文處辦理```駕照翻譯```
---

### 紐約經文處辦理駕照翻譯
要取得經文處的駕照翻譯，須填寫好```駕照英譯空白表格```（可參考下圖我的範本），附上```台灣駕照影本（正反面）```以及 $15 現金（郵寄的話要再多付貼好郵票的回郵信封） <br />
郵寄或親自送到經文處就可以了（約一個星期就會收到取件通知了）<br />

駕照英譯空白表格：
![紐澤西換駕照翻譯表格](../images/Taiwan-Driver-Translation-Form.png)

台灣駕照翻譯本如下圖：
![紐澤西換駕照經文處翻譯本](../images/Taiwan-Driver-Translaion.png)

紐約台灣經文處 [免筆試、免路考申換美國紐澤西州普通自用小客車駕照所需文件及注意事項](https://www.roc-taiwan.org/usnyc/post/9438.html)<br />

---

## 預約 MVC
NJ MVC（NJ Motor Vehicle Commission） [網址](https://telegov.njportal.com/njmvc/AppointmentWizard)

進入後，選擇 Initial Permit 就能選擇喜歡的地點來預約了（我是選擇距離我最近的 Bayonne MVC）

目前因為疫情關係，通常只能預約到二個月後的時間

::: tip
也有人分享預約時不選擇 Initial Permit，到 MVC 再說要辦 Drive lincense 也可以 <br />
但我的經驗是，到了 MVC 承辦人員會要求看預約截圖 <br />
所以若不是預約 Initial Permit 有可能會被拒絕
:::

![NJ MVC 預約](../images/NJ-MVC-Appointment.png)

---

## 前往 MVC
前往 MVC 前，先確認有攜帶:
1. 護照
2. 台灣駕照正本 + 經文處的翻譯本
3. 地址證明（Bank Statement or Utility Bill 擇一即可）
4. SSN 卡（Social Security Number and Card）
5. 美國的銀行所核發的提款卡（我是用 Citi Bank Debit Card）
---

## MVC 申辦流程
到了 MVC 總共有三關：
1. 排隊領表格（為了領取號碼牌）
2. 文件確認（ID Check），確認是否符合六點證明（取得 Permit）
3. 文件 Review，再確認一次六點證明，核發駕照＋拍照（取得駕照）

---

### 1. 排隊領表格
進入 MVC 前，門口的警衛會要你出示預約的手機截圖，確認後才可進入 <br />
進去後就要排隊領取下圖的表格（領取表格時也須出示預約的手機截圖），填寫完後要在排隊一次來領取號碼牌 <br />

![紐澤西換駕照填寫表格](../images/NJ-MVC-Form.png)

領取完號碼牌後，就依照指示去排「ID Check」的隊伍

::: tip
這張表格上寫的名字要跟護照上的一樣，例如我護照是 Yu-Ci，表格上就不能寫 Yuci 
:::

---

### 2. 文件確認（ID Check）
在這關，承辦人員會請你出示剛剛的號碼牌，並請你提供：
1. 護照
2. 台灣駕照正本 + 經文處的翻譯本
3. 地址證明（Bank Statement or Utility Bill 擇一即可）
4. SSN 卡（Social Security Number and Card）
5. 美國的銀行所核發的提款卡（我是用 Citi Bank Debit Card）
6. 上一關填寫的表格

這邊可跟承辦人員說要申請 Drive lincense Permit 就好，不須提到台灣駕照可以免試換照

::: tip
這邊要注意，上一關填寫的表格地址要跟地址證明上的一樣（因為製作好的駕照會寄送到這個地址）
:::

- 第一次去申請時，因為快要搬家了，因此第一關的表格地址寫的是公司地址（在紐約），直接被請回第一關重排 <br />
- 第二次去的時候，因為已經搬完家了，表格上的地址是新家的地址，但提供的地址證明是舊家的（都在紐澤西），一樣被拒絕，須要我提供跟表格上一樣的地址證明才可 <br />

---

### 3. 文件 Review
過了 ID Check 來到了最後一關，這邊也是要把上一關提供的文件拿出來再給他 review 一次，同時他也會順便將資料輸入電腦中建檔 <br />
也是在這邊我們要跟他說台灣駕照是可以直接換駕照而不須考試的 <br />
但我遇到的承辦人員好像已有經驗（或是他們程式有更新），在他看到了台灣駕照後，就直接請我提供經文處的翻譯本 <br />

等他資料都打完後，就會印出下圖這個文件給你簽名（以前的台灣駕照是沒有過期時間的，所以他一開始打的時候是 99/99/9999） <br />

但我先前回台灣時有到監理所將駕照更換為有過期時間的 (01/13/2064)，因此承辦人員再更正了日期後，又印了一次給我簽名（還問我說為什麼台灣的駕照效期這麼長） <br />

![紐澤西換駕照申請表格](../images/NJ-MVC-Appplication.png)


簽完文件後，接下來繳了 $34 （可刷信用卡，但不支援 Google / Apple Pay）以及拍照，就可以得到下圖這張臨時駕照了 <br />
同時他也會告知真正的駕照會在三個星期後寄到我們在第一關表格上填的地址 <br />
在這段時間，可以先用這張臨時駕照來開車 <br />

::: tip
但實際上，我在辦完駕照的第七天（含假日）就收到駕照囉
:::

![紐澤西換駕照通過](../images/NJ-MVC-Approved.png)

---

以上就是用台灣駕照在紐澤西換駕照的流程，在 MVC 所花的時間約為 2 個小時，希望對大家有幫助 <br />


若對辦理 IDNYC 有興趣的，可參考這篇： <br />
[IDNYC 申請攻略，紐約市民必備神卡](https://ycjhuo.gitlab.io/blogs/IDNYC.html)


::: warning
點擊觀看更多： <br />
- [旅遊相關文章](https://ycjhuo.gitlab.io/categories/Travel/)
- [財報分析文](https://ycjhuo.gitlab.io/tag/Stock/)
- [被動收入系列文](https://ycjhuo.gitlab.io/tag/Passive%20Income/)
:::