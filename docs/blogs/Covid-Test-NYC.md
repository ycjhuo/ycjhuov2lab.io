---
title: COVID-19 台灣秋冬防疫專案？紐約哪裡可取得核酸檢測報告 (PCR-Test)？ 更新 2021 檢測資料
description: '紐約 PCR Test|紐約 哪裡打 PCR|紐約 PCR 地點|New York PCR-Test|紐約 PCR 最快|紐約回台灣 PCR Test|疫情回台灣|回台灣 美國 紐約檢測'
date: 2020-12-11 02:17:56
tag: [Covid, New York]
category: Travel
---

台灣在 2020/12/01 宣布了秋冬防疫專案，規定在這段期間內 (2020/12/01 - 2021/02/28) 入境及轉機的旅客在登機前均要有 3 日內 COVID-19 核酸檢驗陰性報告 才能夠登機。影響到許多預計要在這段時間回去的人。

要順利取得檢驗報告，要注意的有二點：
1. 找到符合台灣規定的檢測報告（PCR、RT-PCR、NAA、NAT）的（且免費）檢測站？
2. 檢測報告多久可以出來？讓我們能推算說需要幾天的緩衝期才可在登機 3 天內拿到報告。

這篇分享一下我自己在曼哈頓檢測的經驗，因為怕報告太早或太晚出來，以及報告結果可能不準確(出現陽性反應？)，我總共做了二次檢測（連續二天，不同地方）。

---

## CityMD Fulton Urgent Care - NYC 檢測過程
第一家我選擇的是：CityMD Fulton Urgent Care - NYC
CityMD 是連鎖的醫療中心，之所以選擇這家純粹是因為距離公司最近。

我是星期一(12/07)去驗的，到的時間為 10:26，隊伍人不多，目測在我前面只有 10 人左右（可參見下圖），可能是當天氣溫只有 2 度，大家冷到不想出來排隊。

![42-01](../images/42-1.png)

等了 40 分鐘後，總算進來了室內，裡面等待人數約 6 人左右，須先在左邊的機器填完資料。
![42-02](../images/42-2.png)

在機器輸入個人基本資料時，發現國籍居然有台灣人的選項。
![42-03](../images/42-3.png)

填完資料後（也須填寫保險資料），櫃檯就會把你叫過去（如果沒在機器上填寫保險資料的話，這裡櫃檯會再問一次是否有保險，我在機器上選沒有保險，這裡直接拿保險卡給櫃檯看），詢問為什麼要來檢測以及是否有症狀（我回答有個同事剛被驗出陽性反應，因之前有跟他接觸過，所以就來驗看看，但目前並無症狀）。

之後櫃檯會請我們提供電子信箱，並幫我們建立帳號，報告出來後就可以直接在上面看（以及下載）。
網址是：[http://portal.citymd.com](http://portal.citymd.com/) ，也有 App 版，名字是 CITYMD。

接著進到診療室後，助手會拿一根很長的棉花棒伸進去鼻子裡（還好只要進去一邊），因為伸進去蠻深的，鼻子不舒服了一整天。
在助手取完檢體後，醫生就進來了。

醫生：你說的那位同事是多久前被測出有陽性反應的呢？
我：上週六
醫生：那你最近一次跟那位同事接觸是什麼時候？
我：二個月前。
醫生：距離二個月也太久了，幾乎不會被傳染。那你現在有什麼症狀嗎？
我：沒有。

問完後，醫生感覺很無言，但我總不能說因為我要搭飛機，需要檢測報告才能登機。（猜測如果這樣說的話，保險就不會負擔這次的費用）

最後就說報告大概 5 - 8 天就會出來（我是星期一去驗，星期六的班機），出來診療室後，不死心又問了一次櫃檯報告多久會出來，櫃檯一樣回答最快 5 天，不含假日。

整個過程約 1 小時就可結束，10:26 開始排隊，11:30 檢測完出來。


結果星期四中午就收到 Mail 及簡訊提醒報告已經出來了（驗完後的第三天收到報告）。可以進到 Web 或是 App 中看。
![42-04](../images/42-4.png)

---

## NYC Health + Hospitals / Bellevue 檢測過程
這家是紐約的公立醫院，一樣有提供免費的 PCR 檢測，符合台灣的檢測標準。位於聯合國總部附近（1 Ave. E 26th St.），這邊距離地鐵站（Park Ave. 23 St. 4 5 6 線會到）比較遠，不轉公車的話要走 15 分鐘左右。我是 11:06 到的，還沒進門就看到長長的隊伍。
![42-05](../images/42-5.png)

開始排的時候數了一下前面大概 55 人，前方有二列，一列是給老師，醫療人員的優先列，另一列則是一般檢測的隊伍。因此若優先列人多的話，就會拖慢一般檢測隊伍前進的速度。
下面圖片依序是：隊伍前段，中段，後段，我剛來時是站在第三張圖靠柱子的藍色外套那個女生的位置。
![42-06](../images/42-6.png)
![42-07](../images/42-7.png)
![42-08](../images/42-8.png)


等了二個半小時左右，總算輪到我填資料了。是由櫃檯詢問，我們回答的方式由她來輸入資料。這裡不會詢問檢測原因，但一樣會詢問是否有保險，我問了若提供保險的話，保險公司是否會另外跟我們收費？櫃檯回答不會，且檢測費用是由政府支出。

如果有 ID 或駕照的話可以省略很多輸入資料的時間，我是提供護照。

輸入完資料後，櫃檯一樣會請我們提供電子信箱，並請我們到她發送的連結註冊帳號，之後報告一樣是直接在這邊看。
網址是：[https://epicmychart.nychhc.org/MyChart](https://epicmychart.nychhc.org/MyChart/Authentication/Login?)，App 則是 MyChart。

填完資料後，進到左邊的診療室核對身份，取得檢測標籤。拿到標籤後，出了門走到外面的檢測站，助手就會開始檢測了。
這邊助手也是拿一根長長的棉花棒，但只會在鼻子裡面轉幾圈（二邊都要）就好了，並不會像 CityMD 裡面伸到很裡面。
![42-09](../images/42-9.png)
![42-10](../images/42-10.png)
![42-11](../images/42-11.png)

過程很快，從填完資料到完成檢測只花了不到 10 分鐘，但前面卻花了二個半小時在排隊。檢測完助手就會說 1-2 天就可以在 App 內看到檢測報告了。

結果在驗完的隔天，真的就收到通知信，說報告已經出爐了。
![42-12](../images/42-12.png)
![42-13](../images/42-13.png)

## 結論
如果只想檢測一次就好，推薦 NYC Health + Hospitals。雖然等待的時間較長（總時數約為 3 小時），但因為是在室內（不冷，還有 WIFI），可以悠閒地滑手機，且檢測過程也不會痛。大概檢測完，隔天下午就會收到報告了。

我有個同事也是驗完，隔天就收到報告，所以這個速度應該不是特例。她是 7:40 到，9:30 檢測完畢，整個過程約 2 個小時。

---

CityMD 的話，雖然整體只要花 1 小時，但因室內空間不夠，要在外面等，且檢測過程較不舒服（插的較深）。醫生跟櫃台都說報告要 5 - 8 天才會出來，讓人比較難估算時間。

雖然我在測完的第三天就收到報告了，我同事在紐澤西的 CityMD 測也是驗完三天後（含六日）拿到報告。

另外，紐澤西的 CityMD 是開門時就會照排隊順序先登記，登記完就各自回家等待，輪到你時會打電話給你，再過來檢測。

有次週六，CityMD 9 點開門，我 9:30 到，櫃檯就說今日人數已滿，請改天再來。相比之下，紐約只要在關門前排隊，就會一定會輪到你。相比之下，比較不怕白跑一趟。

如果大家有其他 DP 也歡迎在下面留言，希望大家都能順利在搭機的三天內取得檢驗報告。

## 2021 檢測資料 DP (Data Point)

<table style="width:100%">
  <tr>
    <th align="center">檢測時間</th>
    <th align="center">報告出來時間</th>
    <th align="center">檢測地點</th>
    <th align="center">地址</th>
    <th align="center">檢測流程</th>
  </tr>
  <tr>
    <td align="center">03/21/2021 (Sun.)16:30</td>
    <td align="center">03/27/2021 (Sat.) 12:20</td>
    <td align="center">CityMD Prospect Heights Urgent Care - Brooklyn</td>
    <td align="center">288 Flatbush Ave, Brooklyn, NY 11217</td>
    <td align="center">16:30 Walk in，16:58 測完</td>
  </tr>
  <tr>
    <td align="center">03/25/2021 (Thu.) 09:00</td>
    <td align="center">03/26/2021 (Fri.) 05:10</td>
    <td align="center">NYC Health + Hospitals/Gotham Health, Gouverneur</td>
    <td align="center">227 Madison St, Manhattan, NY 10002</td>
    <td align="center">7:30 排隊，8:30 開始營業，9:00 測完</td>
  </tr>
  <tr>
    <td align="center">03/29/2021 (Mon.) 10:00</td>
    <td align="center">03/29/2021 (Mon.) 17:00</td>
    <td align="center">CareCube</td>
    <td align="center">203 Court St, Brooklyn, NY 11201</td>
    <td align="center">10:00 Walk in，週一沒什麼人，馬上驗完</td>
  </tr>
  <tr>
    <td align="center">03/31/2021 (Wed.) 09:20</td>
    <td align="center">03/31/2021 (Wed.) 19:30</td>
    <td align="center">NYC Health + Hospitals/Gotham Health, Gouverneur</td>
    <td align="center">227 Madison St, Manhattan, NY 10002</td>
    <td align="center">09:20 Walk in，約 10 - 15 人在排隊，09:58 測完，隊伍約剩 5 人</td>
  </tr>
  <tr>
    <td align="center">04/05/2021 (Mon.) 09:00</td>
    <td align="center">04/06/2021 (Tue.) 00:58</td>
    <td align="center">NYC Health + Hospitals/Gotham Health, Gouverneur</td>
    <td align="center">227 Madison St, Manhattan, NY 10002</td>
    <td align="center">08:30 Walk in，09:00 測完</td>
  </tr>
    <tr>
    <td align="center">04/20/2021 (Tue.) 13:10</td>
    <td align="center">04/21/2021 (Wed.) 07:20</td>
    <td align="center">NYC Health + Hospitals/Gotham Health, Gouverneur</td>
    <td align="center">227 Madison St, Manhattan, NY 10002</td>
    <td align="center">13:03 Walk in，13:20 測完</td>
  </tr>
</table>

做完檢測，且報告出來後，就準備到機場回台灣囉。想了解出入境流程的可參考這篇：<br />
[開箱！疫情期間的長榮航空 紐約（JFK）飛 台北（TPE）豪華經濟艙](https://ycjhuo.gitlab.io/blogs/Eva-Air-JFK-To-TPE-Premium-Economy-Class.html)

新增 2022 年版本
[Covid-19 疫情期間，長榮航空經濟艙，紐約回台灣，出入境流程，2022 年版](https://ycjhuo.gitlab.io/blogs/Covid-New-York-Taiwan.html)