---
title: 特斯拉（TSLA）公佈 2021 Q4 財報，股價暴跌 11%
description: '特斯拉財報|TSLA 財報|特斯拉 2021 Q4 第四季財報|TSLA 2021 Q4 第四季財報'
date: 2022-02-06
tag: [Stock, TSLA]
category: Investment
---

還記得特斯拉在 Q3 財報繳出了亮麗的成績單，股價大幅領先大盤嗎？（期間，特斯拉股價上漲 31.7%； S&P 500 僅為 +2.58%） <br/>

而在 Q3 - Q4 這段期間：股價上升了 8.27%；同期 S&P 500 指數為 -4.11%  <br/>


但特斯拉在 01/26 公佈了 2021 Q4 財報後，隔日股價即下跌了 11.17%，是財報中隱藏了什麼秘密導致股價大幅下滑嗎？ <br/>
就成長幅度來說，特斯拉 Q4 財報更優於 Q3，下面就來看看特斯拉 Q4 財報的表現如何

::: warning
特斯拉上季（Q3）財報回顧：[特斯拉（TSLA）公佈 2021 Q3 季報，股價寫下新紀錄](https://ycjhuo.gitlab.io/blogs/TSLA-Earnings-2021-Q3.html)
:::

## 特斯拉 2021 Q4 財報
[來源](https://ir.tesla.com/#tab-quarterly-disclosure)：
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <th align="center">2021 Q4</th>
    <th align="center">Q3 vs. Q2</th>
    <th align="center">Q4 vs. Q3</th>
  </tr>
  <tr>
    <td align="center">電動車營收（百萬）</td>
    <td align="center">10,206</td>
    <td align="center">12,057</td>
    <td align="center">15,967</td>
    <td align="center">15.35%</td>
    <td align="center">24.49%</td>
  </tr>
  <tr>
    <td align="center">電動車利潤（百萬）</td>
    <td align="center">2,899</td>
    <td align="center">3,673</td>
    <td align="center">4,882</td>
    <td align="center">21.07%</td>
    <td align="center">24.76%</td>
  </tr>
  <tr>
    <td align="center">電動車毛利率</td>
    <td align="center">28.4%</td>
    <td align="center">30.5%</td>
    <td align="center">30.6%</td>
    <td align="center">2.1%</td>
    <td align="center">0.1%</td>
  </tr>
  <tr>
    <td align="center">營業利益率</td>
    <td align="center">11%</td>
    <td align="center">14.6%</td>
    <td align="center">14.7%</td>
    <td align="center">3.6%</td>
    <td align="center">0.1%</td>
  </tr>
    <tr>
    <td align="center">EPS（Diluted）</td>
    <td align="center">1.02</td>
    <td align="center">1.44</td>
    <td align="center">2.05</td>
    <td align="center">29.17%</td>
    <td align="center">29.76%</td>
  </tr>
</table>

- 電動車營收：與 Q3 相比，上漲了 24.49%，且已連續三季成長有著雙位數的成長 11.8% → 15.35% → 24.49%  <br/>
在此同時，總營收成長幅度為 22.36%，表示電動車的成長動能更高於其它收入
- 電動車利潤：成長幅度（24.76%） > 營收（24.49%），表示特斯拉在造車毛利上有了更近一步的提升
- 毛利率：連續二季保持在 30% 以上（30.5% → 30.6%）；傳統車廠的龍頭豐田，毛利率僅 19.9%，而同為電動車的蔚來汽車則是 19.1%
- 營業利益率：雖本季（14.6%）僅比 Q3 上升 0.1%，但這已扣除了給 CEO 的 2.45 億的股權協議（本季總營業利潤約 26 億）所得出的營業利益率，若把這 2.45 億排除的話，則本季的營業利益率會從 14.7% → 16%
- EPS：本季 EPS 為 2.05，相比上季成長了 29.76%，僅一季的 EPS 就達到了 Q1 - Q3 總和的 70%

本季的財報相比上季來說，表現更好，營收/利潤均有約 25% 以上的成長性，毛利率/營益率皆在同產業位居第一  <br/>

::: tip
目前市值最高的傳統車廠—豐田與電動車—蔚來的毛利率 / 營益率各為 19.9% ＆ 11% 以及 19.1% ＆ -9.1%，皆遜於特斯拉的 30.6% ＆ 14.7%
:::

![特斯拉與其他同業的營業利益率比較](../images/156-01.png)

## 特斯拉 2021 Q4 生產/交付量
<table style="width:100%">
  <tr>
    <th align="center"></th>
    <th align="center">2021 Q2</th>
    <th align="center">2021 Q3</th>
    <td align="center">2021 Q4</td>
    <th align="center">Q2 vs. Q1</th>
    <th align="center">Q3 vs. Q2</th>
    <td align="center">Q4 vs. Q3</td>
  </tr>
  <tr>
    <td align="center">Model S/X 生產量</td>
    <td align="center">2,340</td>
    <td align="center">8,941</td>
    <td align="center">13,109</td>
    <td align="center">N/A</td>
    <td align="center">282.09%</td>
    <td align="center">46.62%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 生產量</td>
    <td align="center">204,081</td>
    <td align="center">228,882</td>
    <td align="center">292,731</td>
    <td align="center">13.17%</td>
    <td align="center">12.15%</td>
    <td align="center">27.9%</td>
  </tr>
  <tr>
    <td align="center">Model S/X 交付量</td>
    <td align="center">1,895</td>
    <td align="center">9,289</td>
    <td align="center">11,766</td>
    <td align="center">-6.65%</td>
    <td align="center">390.18%</td>
    <td align="center">26.67%</td>
  </tr>
  <tr>
    <td align="center">Model 3/Y 交付量</td>
    <td align="center">199,409</td>
    <td align="center">232,102</td>
    <td align="center">296,884</td>
    <td align="center">9.06%</td>
    <td align="center">16.39%</td>
    <td align="center">27.91%</td>
  </tr>
  <tr>
    <td align="center">總生產量</td>
    <td align="center">206,421</td>
    <td align="center">237,823</td>
    <td align="center">305,840</td>
    <td align="center">14.46%</td>
    <td align="center">15.21%</td>
    <td align="center">28.6%</td>
  </tr>
  <tr>
    <td align="center">總交付量</td>
    <td align="center">201,304</td>
    <td align="center">241,391</td>
    <td align="center">308,650</td>
    <td align="center">8.89%</td>
    <td align="center">19.91%</td>
    <td align="center">27.86%</td>
  </tr>
</table>

- Model S/X 生產量：身為車價＆利潤最高的車種，季產能約 1.3 萬台，成長率與 Q3 相比後約 47%
- Model 3/Y 生產量：主力銷售車種，季產能為 29 萬台，成長率與 Q3 相比約為 28%，增速甚至是 Q3 的二倍

目前 Model S/X 生產量的成長速度約是 3/Y 的 1.75 倍，若這個趨勢沒有減緩的話，則特斯拉的毛利率仍能繼續提升

財報中提到：
1. 德州＆柏林工廠的 Model Y 將在取得政府許可後，開始交付到消費者手上
2. 加州的 Fremont 工廠，在今年的 Model 3/Y 生產量為 50 萬，明年將可實現年產 60 萬的目標

特斯拉預估，未來的生產量將以年增 50% 的幅度成長，下圖為特斯拉與同業的交付量比較：
特斯拉在 2021 的成長率皆在 50% 以上，而其它車廠仍處在生產地獄，皆在 25% 以下

::: tip
2021 Model 3/Y 生產量（90.6 萬）約為 2020（45.5 萬）的二倍
:::

![特斯拉與同業的交付量比較](../images/156-02.png)

## 後記
財報中提到，軟體營收 Full Self-Driving (FSD) 將會成為未來增速最快的營收來源，同時也是改善汽車安全性的重要因素  <br/>
而目前在美國約有 6 萬使用者，相比現在年產量近百萬的 Model 3/Y，的確是仍有廣大的增速空間

最近由於美股正處在升息前的動盪階段，各家財報公布後，不是大漲就是大跌，例如： <br/>
Google 財報發布後，大漲近 10%，而 Facebook 則是大跌 26%  <br/>

與其將資金投入前景不明的 FB 或是為了要搶進拆股的股價紅利，我個人偏向買入財報好，前景也很明確的特斯拉，也希望在升息的壓力下，能夠讓特斯拉的股價繼續下跌，讓我們可用更低的價格買進更多的特斯拉

