import { sidebar } from "vuepress-theme-hope";

export const twSidebar = sidebar({
  "/": [
    "",
    {
      text: "美股投資",
      icon: "arrow-trend-up",
      link: "https://ycjhuo.gitlab.io/tag/Stock//",
    },
    {
      text: "行程旅遊",
      icon: "location-pin",
      link: "https://ycjhuo.gitlab.io/categories/Travel/"
    },
    {
      text: "飯店開箱",
      icon: "hotel",
      link: "https://ycjhuo.gitlab.io/tag/Hotel/"
    },
    {
      text: "被動收入",
      icon: "hand-holding-dollar",
      link: "https://ycjhuo.gitlab.io/tag/Passive%20Income/"
    },
  ],
});
