import { defineUserConfig } from "vuepress";
import theme from "./theme.js";
import { searchPlugin } from "@vuepress/plugin-search";
import { googleAnalyticsPlugin } from '@vuepress/plugin-google-analytics'
import googleAdSensePlugin from "vuepress-plugin-google-adsense2";


export default defineUserConfig({
  base: "/ycjhuov2/",
  
  plugins: [
    searchPlugin({ }),
    googleAnalyticsPlugin({ id: 'G-9N82TZGCXL'}),
    googleAdSensePlugin ({ id: 'ca-pub-1016984133618195'}),
  ],

  /*
  locales: {
    "/": {
      lang: "zh-TW",
      title: "YC's Weekly Journal",
      description: "Code, Invest, Travel",
    },
    "/en/": {
      lang: "en-US",
      title: "YC's Weekly Journal",
      description: "Code, Invest, Travel",
    },
  },
  */

  theme,

  // Enable it with pwa
  // shouldPrefetch: false,
});
